<?php

/**
 * Description of user_auth
 *
 * @author TOHIR <otcleantech@gmail.com>
 * @example  description
 */
class User_auth {

    public function __construct() {
        if (!class_exists('IpaddressLocator')) {
            require_once 'utils/IpaddressLocator.php';
        }

        require_once 'bin/constant.php';
        require_once 'bin/make_safe.php';
        require_once 'utils/methods.php';
    }

    public function login(array $params = []) {
        if (empty($params)) {
            return FALSE;
        }

        $con = new Connection();
        $con->ConnectioManager();

        $iplocator = \utils\IpaddressLocator::fetchIpaddressCityCountry(getRealIp());
        $ip = json_decode($iplocator);

        if ($ip->status == 'success') {
            $city = $ip->city;
            $country = $ip->country;
        } else {
            $city = '';
            $country = '';
        }

        $qry = "SELECT a.*, s.logo FROM tbl_account a "
            . "LEFT JOIN tbl_settings s ON a.account_id = s.school_id "
            . " WHERE a.school_id = '" . $params['school_id'] . "' AND a.username='" . $params['username'] . "' AND  a.password='" . $params['password'] . "' ";
        $rst = mysql_query($qry) or die(mysql_error());

        if (mysql_num_rows($rst) > 0) {
            $_SESSION['ereport'] = mysql_fetch_assoc($rst);
            $_SESSION['school_id'] = $_SESSION['ereport']['school_id'];

            $message = $_SESSION['ereport']['school_name'] . " log in successfully";
            $this->log_user_action($message);

            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function logout() {
        $this->log_user_action($_SESSION['ereport']['school_name'] . " logout successfully");
        session_unset('ereport');
        session_destroy();
        
        return TRUE;
    }

    public function log_user_action($message, $level = 0) {

        $iplocator = \utils\IpaddressLocator::fetchIpaddressCityCountry(getRealIp());
        $ip = json_decode($iplocator);

        if ($ip->status == 'success') {
            $city = $ip->city;
            $country = $ip->country;
        } else {
            $city = '';
            $country = '';
        }
        $acount_id = $_SESSION['ereport']['account_id'];
        $ip_address = ip2long(getRealIp());
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $datetime = date('Y-m-d H:i:s');

        $sql = "INSERT INTO tbl_log VALUES (null, '$acount_id', '$ip_address', '$user_agent', '$message', '$datetime', '$level', '$city', '$country', 1)";

        ExecuteSQLQuery($sql);

        return;
    }

}

// end class: user_auth
// end file: user_auth.php
