<?php

/**
 * Description of settings
 *
 * @author TOHIR
 */


class Settings {
    
     public function __construct() {
        if (!class_exists('IpaddressLocator')) {
            require_once 'utils/IpaddressLocator.php';
        }

        
        require_once 'utils/methods.php';
    }
    
    public function save(array $data = []){
        
        if (empty($data)){
            return FALSE;
        }
        
        $school_id = $_SESSION['ereport']['account_id'];
        $school_name = $data['school_name'];
        $address = $data['address'];
        $phone = $data['phone'];
        $email = $data['email'];
        $date_updated = date('Y-m-d h:i:s');
        
         $sql = "INSERT INTO tbl_settings VALUES (null, '$school_id', '$school_name', '$address', '$phone', '$email', null, '$date_updated')";

        ExecuteSQLQuery($sql);
        
        return TRUE;
    }
}
