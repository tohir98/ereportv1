<?php session_start();
set_time_limit(0);
include("connect.php");
include("fns.php");

$_SESSION['user'] = $_SESSION['cspa']['email'];

if( !isset($_SESSION['user']))
{
	include("index.php");
	exit;
}
?><head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Typography | BlueWhale Admin</title>
    <link rel="stylesheet" type="text/css" href="css_main/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css_main/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
			setSidebarHeight();


        });
    </script>
</head>
<?php
	$select = "SELECT * FROM customer_reg WHERE ctype='Customer' ";
	$result= mysql_query($select);
?>

<div class="box round first grid">
                <h2>
                   Client Lists <font style="font-size:15px; color:red">You have (<?php  echo mysql_num_rows($result);?>) Companies. </font></h2>
                <div class="block">
                    
                    
                    
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th></th>
							<th>Sector</th>
							<th>Company</th>
							<th>Contact Person</th>
							<th>Mobile</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
					<?php
					
					$i = 0;
					while($row = mysql_fetch_array($result)) 
					{
					
					?>
						<tr class="odd gradeX">
						  <?php 
						  $sn=$row[sn];
						  echo"<td><input type=checkbox name=sn[] value=$sn></td>";
						  ?>
					
						  <td><?php echo $row[sector] ?></td>
						  <td><?php echo $row[company] ?></td>
						  <td><?php echo $row[fname]." ".$row[lname] ?></td>
						  <td><?php echo $row[mobile] ?></td>
						   <?php 
						  echo "<td><a href='home.php?page=edit-client&sn=$sn'>Edit</a></td>";
							?>
						</tr>
						<?php
					  }
					  ?>
					
						
					</tbody>
				</table>
                    
                    
                    
                </div>
            </div>