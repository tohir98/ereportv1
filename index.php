<?php
@session_start();
$msg = (string) "Welcome...Pls Login";

if (!get_cfg_var('safe_mode')) {
    set_time_limit(0);
}

//include("bin/make_safe.php");
include("bin/Connection.php");

if (!class_exists('User_auth')) {
    require_once 'libraries/classes/user_auth.php';
}


if (isset($_POST['btn'])) {

    $user_auth = new User_auth();
    if ($user_auth->login(array(
            'school_id' => strip_tags(stripslashes($_POST['school_id'])),
            'username' => strip_tags(stripslashes($_POST['username'])),
            'password' => strip_tags(stripslashes($_POST['password']))
        ))
    ) {
      
        
        header("location:home.php");
    }else{
        $msg = "Invalid Login Details";
    }
   
}
?>

<?php
if (filter_input(INPUT_GET, $_GET['page']) == "logout") {
    $dat = date("Y-m-d");
    $tim = date("H:i:s");
    $action = "LOGOUT (USER : " . $_SESSION['school_id'] . ")";
    logit($_SESSION['school_id'], $ip_address, $action);
    session_destroy();
    @header("location: index.php");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--webfonts-->
        <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700|Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
        <!--//webfonts-->
        <script>
            function myFunction()
            {
                alert("Thanks for login");
            }
        </script>
    </head>

    <body>
        <div class="main">
            <div class="user">
                <img src="images/user.png" alt="">
            </div>
            <div class="login">
                <div class="inset">
                    <!-----start-main---->
                    <form action="" method="post">
                        <div>
                            <span style="color: #FF0000"><?php echo $msg; ?></span>
                        </div>
                        <div>
                            <span><label>School ID</label></span>
                            <span><input required name="school_id" id="school_id" type="text" class="textbox" value="100"></span>
                        </div>
                        <div>
                            <span><label>Username</label></span>
                            <span><input required name="username" id="username" type="text" class="textbox" value="demo"></span>
                        </div>
                        <div>
                            <span><label>Password</label></span>
                            <span><input required type="password" name="password" id="password" class="password" value="password"></span>
                        </div>
                        <div class="sign">
                            <div class="submit">
                                <input name="btn" type="submit" value="LOGIN" >
                            </div>
                            <span class="forget-pass">
                                <a href="#">Forgot Password?</a>
                            </span>
                            <div class="clear"> </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-----//end-main---->
        </div>
    </body>
</html>