﻿<?php
@session_start();
set_time_limit(0);
ob_start();
//error_reporting(0);


require_once 'bin/constant.php';
//require_once 'bin/make_safe.php';
require_once 'libraries/classes/user_auth.php';
$user_auth = new User_auth();

if (!isset($_SESSION['ereport'])) {
    header("Location: http://eduportalng.com/demo/index.php");
    exit();
}



//if logout then destroy the session and redirect the user
if (isset($_GET['logout'])) {
    if ($user_auth->logout()) {
        header('location: index.php');
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>School Manager</title>
        <link rel="stylesheet" type="text/css" href="css_main/reset.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css_main/text.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css_main/grid.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css_main/layout.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css_main/nav.css" media="screen" />
        <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
        <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
        <link href="css_main/table/demo_page.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN: load jquery -->
        <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
        <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
        <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
        <!-- END: load jquery -->
        <script type="text/javascript" src="js/table/table.js"></script>
        <script src="js/setup.js" type="text/javascript"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                setupLeftMenu();

                $('.datatable').dataTable();
                setSidebarHeight();


            });
        </script>
    </head>
    <body>
        <div class="container_12">
            <div class="grid_12 header-repeat">
                <div id="branding">
                    <div class="floatleft">
<?php
if ($_SESSION['ereport']['logo'] && $_SESSION['ereport']['logo'] !== ''):
    $schLogo = 'upload/' . $_SESSION['ereport']['logo'];
else:
    $schLogo = "images/logo.png";
endif;
?>
                        <img src="<?= $schLogo ?>" alt="<?= $_SESSION['ereport']['school_name'] ?>" style="max-height: 75px" />
                    </div>
                    <div class="floatright">
                        <div class="floatleft">
                            <img src="img/img-profile.jpg" alt="Profile Pic" /></div>
                        <div class="floatleft marginleft10">
                            <ul class="inline-ul floatleft">
                                <li>Hello Admin</li>
                                <li><a href="settings/settings.php" target="mainFrame">Config</a></li>
                                <li><a href="home.php?logout">Logout</a></li>
                            </ul>
                            <br />
                            <span class="small grey">Last Login: 3 hours ago</span>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="grid_12">
                <ul class="nav main">
                    <li class="ic-dashboard"><a href="#"><span>Dashboard</span></a> </li>
                    <li class="ic-form-style"><a href="javascript:"><span>Account</span></a>
                        <ul>
                            <li><a href="staffinfo/session_grid.php" target="mainFrame">Session</a> </li>
                            <li><a href="staffinfo/term_grid.php" target="mainFrame">Term</a> </li>
                            <li><a href="staffinfo/subject_grid.php" target="mainFrame">Subjects</a> </li>
                            <li><a href="staffinfo/class_grid.php" target="mainFrame">Class</a> </li>
                            <li><a href="calendrlist.php" target="mainFrame">School Calender</a> </li>
                            <li><a href="settings/settings.php" target="mainFrame">Settings</a> </li>
                        </ul>
                    </li>
                    <li class="ic-charts"><a href="javascript:"><span>Communication</span></a>
                        <ul>
                            <li><a href="communication/send_sms.php" target="mainFrame">SMS</a> </li>
                            <li><a href="communication/email.php" target="mainFrame">Email</a> </li>
                        </ul>
                    </li>
                    <li class="ic-notifications"><a href="#"><span>Notifications</span></a></li>
                </ul>
            </div>
            <div class="clear">
            </div>
            <div class="grid_2">
                <div class="box sidemenu">
                    <div class="block" id="section-menu">
                        <ul class="section menu">
                            <li><a class="menuitem">Manage Student</a>
                                <ul class="submenu">
                                    <li><a href="addstudent.php" target="mainFrame">New Student</a> </li>
                                    <li><a href="studentlist.php" target="mainFrame">All Students</a> </li>
                                </ul>
                            </li>
                            <li><a class="menuitem">Manage Fee</a>
                                <ul class="submenu">
                                    <li><a href="fee/fees.php" target="mainFrame">Fees</a> </li>
                                    <li><a href="fee/addfee.php" target="mainFrame">Setup Fee</a> </li>
                                    <li><a href="fee/payment.php" target="mainFrame">Fee Payment</a> </li>
                                    <li><a href="fee/allpayment.php" target="mainFrame">All Payments</a></li>
                                    <li><a href="fee/total_income.php" target="mainFrame">Total Income</a></li>
                                </ul>
                            </li>
                            <li><a class="menuitem">Manage Staff</a>
                                <ul class="submenu">
                                    <li><a href="staffinfo/position_grid.php" target="mainFrame">Setup Level</a> </li>
                                    <li><a href="staffinfo/employee.php" target="mainFrame">Setup Staff</a> </li>
                                    <li><a href="staffinfo/employee_grid.php" target="mainFrame">All Staff(s)</a> </li>
                                </ul>
                            </li>
                            <li><a class="menuitem">Payrol</a>
                                <ul class="submenu">
                                    <li><a  href="staffinfo/earnings.php" target="mainFrame">Earnings</a> </li>
                                    <li><a  href="staffinfo/earnings_grid.php" target="mainFrame">Earnings List</a> </li>
                                    <li><a  href="staffinfo/deductions2.php" target="mainFrame">Deductions</a> </li>
                                    <li><a  href="staffinfo/deduction_grid.php" target="mainFrame">Deduction List</a> </li>
                                    <li><a  href="staffinfo/search.php" target="mainFrame">Monthly Report</a> </li>
                                </ul>
                            </li>
                            <li><a class="menuitem">Psychomotor</a>
                                <ul class="submenu">
                                    <li><a  href="staffinfo/psychomotor_list.php" target="mainFrame">Psychomotor List</a> </li>
                                    <li><a  href="psychomotor.php" target="mainFrame">Add Psychomotor</a> </li>
                                </ul>
                            </li>
                            <li><a class="menuitem">Result</a>
                                <ul class="submenu">
                                    <li><a  href="resultlist.php" target="mainFrame">Result List</a> </li>
                                    <li><a  href="upload.php" target="mainFrame">Upload Result</a> </li>
                                    <li><a  href="check.php" target="mainFrame">Check Result</a> </li>
                                </ul>
                            </li>
                            <li><a class="menuitem">Manage User</a>
                                <ul class="submenu">
                                    <li><a  href="staffinfo/user_grid.php" target="mainFrame">User List</a> </li>
                                    <li><a  href="staffinfo/user.php" target="mainFrame">New User</a> </li>
                                    <li><a  href="staffinfo/usertype_grid.php" target="mainFrame">New Type</a> </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="grid_10">
                <div class="box round first grid">
                    <h2>
                        Welcome: <?= strtoupper($_SESSION['ereport']['school_name']) ?> &nbsp;|&nbsp;School ID:<?= $_SESSION['school_id'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date('d-M-Y'); ?></h2>

                    <iframe name="mainFrame" id="mainFrame" width="100%" height="500px" frameborder="0" src="studentlist.php"> </iframe>
                </div>

            </div>
            <div class="clear">
            </div>
        </div>
        <div class="clear">
        </div>
        <div id="site_info">
            <p>
                Copyright &copy;<?php echo date('Y'); ?> &nbsp; | &nbsp;<a href="#"><?= SITE_NAME ?></a>. All Rights Reserved.
            </p>
        </div>
    </body>
</html>
