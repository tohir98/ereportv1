<?php
 	include '../bin/Connection.php';
	 include "../bin/make_safe.php";
    $con = new Connection();
	$con->ConnectioManager();
	
	$payment_id=$_GET[id];
	$select = "SELECT p.payment_id as ID, a.firstname Firstname,a.lastname Lastname, p.amount Amount, c.class_name Class, t.term, s.session_name, p.datetime,  p.purpose, p.amount2,p.session_id,p.term_id, p.class_id,p.admission_id
FROM tbl_payment p INNER JOIN tbl_class c ON p.class_id=c.class_id
INNER JOIN tbl_term t ON p.term_id=t.term_id
INNER JOIN tbl_session s ON p.session_id=s.session_id
INNER JOIN tbl_admission a ON p.admission_id = a.admission_id WHERE payment_id = '$payment_id' ";

	$result= mysql_query($select);
	
	while($row = mysql_fetch_array($result)) 
	{
		$name=strtoupper($row[1]." ".$row[2]);
		$amount=number_format($row[3],2);
		$date=$row[7];
		$date=date("d-M-Y",strtotime($date));
		$receipt_no="A".str_pad($row[0],6,"0",STR_PAD_LEFT);
		$purpose=$row[8];
		$amount2=$row[9];
		$amount=$row[3];
		$class=$row[4];
		$term=$row[5];
		//echo getAllPaidFee($row[10],$row[11],$row[12],$row[13]);
		$total_paid=getAllPaidFee($row[10],$row[11],$row[12],$row[13]);
		$balance=getFee($row[10],$row[11],$row[12])-$total_paid;
	}
		
	  	

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>-</title>
  <link rel="stylesheet" href="calendar.css">
<script language="JavaScript" src="calendar_us.js"></script>
<link rel="stylesheet" type="text/css" href="css/default.css">


<style type="text/css">
<!--
.style103 {font-size: 10pt; font-family: Tahoma; font-weight: bold; color: #000000; }
.style29 {font-size: 10px; color: #333333; font-family: Tahoma; font-weight: bold; }
.style5 {font-size: 10px}
.style9 {font-size: 10pt;
	font-family: Tahoma;
	font-weight: bold;
	color: #FFFFFF;
}

.style99 {color: #003399;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.style104 {color: #FF0000}
-->
</style>
</head>

<body>
<div style="width:50%; border:solid 3px #990000; height:auto; margin-top:20px; border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px;  margin-left:280px; background-color:#FFFFFF">
<table width="100%" align="" class="contentpaneopen">
  <tbody>
    <tr>
      <td colspan="2" valign="top"><form method="post" enctype="multipart/form-data"  name="frmpayment" id="frmpayment" >
        <table width="98%" border="0" cellpadding="5" cellspacing="5">
          <tbody>
            
            
            <tr>
              <td colspan="3" align="left" style="font-size:16px; font-family:arial; font-weight:bold; margin-left:40px;">Receipt No: <?=$receipt_no?></td>
              <td align="left" style="font-size:16px; font-family:arial; font-weight:bold; margin-left:40px;"><?php echo date('d/M/Y'); ?></td>
            </tr>
            <tr>
              <td colspan="4" align="right"><hr color="#FF0000" /></td>
              </tr>
            <tr>
              <td align="left"><span class="label"><b><i>Payment Date :</span></td>
              <td width="32%" align="left"><?php echo $date; ?></td>
              <td align="left"><span class="label"><b><i>Class :</span></td>
              <td align="left"><?php echo $class; ?></td>
            </tr>
            <tr>
              <td align="left"><span class="label"><b><i>Received From  :</span></td>
              <td align="left"><?php echo $name; ?></td>
              <td align="left"><span class="label"><b><i>Term :</span></td>
              <td align="left"><?php echo $term; ?></td>
            </tr>
            <tr>
              <td align="left"><span class="label"><b><i>Amount Paid  :</span></td>
              <td colspan="3" align="left"><?php echo ucfirst($amount2); ?></td>
              </tr>
            <tr>
              <td width="32%" align="left" valign="top"><span class="label"><b><i>Being Payment For  :</span></td>
              <td colspan="3" align="left"><?php echo ucfirst($purpose); ?></td>
              </tr>
            <tr>
              <td align="left"><span class="label"><b><i>Currently Paid:</i></b></span></td>
              <td align="left">N<?php echo number_format($amount,2); ?></td>
              <td align="left">&nbsp;</td>
              <td align="left">&nbsp;</td>
            </tr>
            <tr>
              <td align="left"><span class="label"><b><i>Total Amount Paid:</i></b></span></td>
              <td align="left">N<?php echo number_format($total_paid,2); ?></td>
              <td align="left"><span class="label"><b><i>Balance:</i></b></span></td>
              <td align="left">N<?php echo number_format($balance,2); ?></td>
            </tr>
            
            <tr>
              <td colspan="4" align="right"><hr color="#FF0000"/></td>
              </tr>
            
            <tr>
              <td align="right">&nbsp;</td>
              <td align="right"><span class="style5"><div id="dstat" align="center"></div></span></td>
              <td align="left"><label></label></td>
              <td align="left"><a href="javascript:window.print()"><img src="../images/bt_print.jpg" alt="Click Here to Print" width="96" height="19" /></a></td>
            </tr>
          </tbody>
        </table>
      </form></td>
    </tr>
  </tbody>
</table>
</div>
</body>
</html>
