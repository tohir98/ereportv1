<?php include '_fee_header_script.php'; ?>
<script type="text/javascript">

    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();


    });
</script>
</head>
<?php
$select = "SELECT p.payment_id, a.lastname, a.firstname, p.amount, c.class_name, t.term, s.session_name
					FROM tbl_payment p INNER JOIN tbl_class c ON p.class_id=c.class_id
					INNER JOIN tbl_term t ON p.term_id=t.term_id
					INNER JOIN tbl_session s ON p.session_id=s.session_id
					INNER JOIN tbl_admission a ON p.admission_id = a.admission_id ORDER BY p.payment_id DESC";
$result = mysql_query($select);
?>

<div class="box round first grid">
    <h2>
        Student Payment History</h2>
    <div class="block">



        <table class="data display datatable" id="example">
            <thead>
                <tr>

                    <th>ID</th>
                    <th>Receipt No</th>
                    <th>Lastname</th>
                    <th>Firstname</th>
                    <th>Amount</th>
                    <th>Class</th>
                    <th>Term</th>
                    <th>Session</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                while ($row = mysql_fetch_array($result)) {
                    ?>
                    <tr class="odd gradeX">

                        <td><?php echo $row[0] ?></td>
                        <td><?php echo "A" . str_pad($row[0], 6, "0", STR_PAD_LEFT) ?></td>
                        <td><?php echo $row[1] ?></td>
                        <td><?php echo $row[2] ?></td>
                        <td><?php echo number_format($row[3], 2) ?></td>
                        <td><?php echo $row[4] ?></td>
                        <td><?php echo $row[5] ?></td>
                        <td><?php echo $row[6] ?></td>
    <?php
    echo "<td><a href='receipt.php?id=$row[0]' target='_blank'>Receipt</a></td>";
    ?>
                    </tr>
                        <?php
                    }
                    ?>


            </tbody>
        </table>



    </div>
</div>