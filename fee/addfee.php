<?php include '_fee_header_script.php'; ?>
<?php

if ($_POST['Submit'] === "Save") {
    $term_id = $_POST['term_id'];
    $session_id = $_POST['session_id'];
    $class_id = $_POST['class_id'];
    $admission_form = $_POST['admission_form'];
    $tuition_fee = $_POST['tuition_fee'];
    $textbook = $_POST['textbook'];
    $uniform = $_POST['uniform'];
    $jur_waec = $_POST['jur_waec'];
    $games = $_POST['games'];
    $development = $_POST['development'];
    $library = $_POST['library'];
    $exam = $_POST['exam'];
    $practical = $_POST['practical'];
    $pta = $_POST['pta'];
    $project = $_POST['project'];
    $transport = $_POST['transport'];
    $excursion = $_POST['excursion'];
    $lesson = $_POST['lesson'];
    $sport_wear = $_POST['sport_wear'];
    $computer = $_POST['computer'];
    $meal = $_POST['meal'];
    $graduation = $_POST['graduation'];


    if (!$term_id || !$session_id || !$class_id || !$tuition_fee) {
        $error = "<font color='red'>Please Enter Necessary Information!!!</font>";
    } elseif (recordexist2($term_id, $session_id, $class_id)) {
        $error = "Record exist!!!";
        //exit();	
    } else {
        if (!$result = mysql_query("INSERT INTO tbl_fee VALUES(null,
				'$admission_form',				'$tuition_fee',				'$textbook',				'$uniform',				'$jur_waec',
				'$games',	'$development',	'$library',	 '$exam', '$practical',	'$pta',		'$project',		'$transport',	'$excursion',	'$lesson',				'$sport_wear','$computer', null, '$meal',	'$graduation', '$term_id', '$session_id', '$class_id')"))
            echo mysql_error();
        else {
            echo "Operation successful";
            ?>
            <script type="text/javascript"> alert("Operation was successsful");</script>
            <?php
        }
    }
}
?>
    <script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setupProgressbar('progress-bar');
        setDatePicker('date-picker');
        setupDialogBox('dialog', 'opener');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
    </script>
    <!-- /TinyMCE -->
    <style type="text/css">
        #progress-bar
        {
            width: 400px;
        }
    </style>
</head>

<div class="box round first grid">
    <h2>
        SetUp Fee</h2>
    <div class="block ">


        <form method="post" enctype="multipart/form-data"  name="frmreg" id="frmreg" >
            <table class="form">
                <tbody>
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="98%" border="0" cellpadding="5" cellspacing="5">
                                <tbody>
                                    <tr>
                                        <td colspan="4" align="center" style="font-size:16px; font-family:arial; font-weight:bold; margin-left:40px; color: #EE0000"><?php echo isset($error) ? $error : ''; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="left" style="font-size:16px; font-family:arial; font-weight:bold; margin-left:40px;">School Fee Setup</td>
                                        <td align="left" style="font-size:16px; font-family:arial; font-weight:bold; margin-left:40px;"><?php echo date('d/M/Y'); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="right"><hr color="#FF0000" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><label>Term :</label></td>
                                        <td align="left"><?php
if (isset($term_id)) {
    $Qterm2 = "SELECT * FROM tbl_term WHERE term_id != '$term_id'";
    $Rterm2 = mysql_query($Qterm2);
    $Qterm1 = "SELECT * FROM tbl_term WHERE term_id = '$term_id'";
    $Rterm1 = mysql_query($Qterm1);
    $rowterm1 = mysql_fetch_array($Rterm1);
} else {
    $Qterm2 = "SELECT * FROM tbl_term ORDER BY term ASC";
    $Rterm2 = mysql_query($Qterm2);
}
?>
                                            <select name="term_id" class="" id="term_id">
                                                <option value="">Select term</option>
                                            <?php if (isset($term_id)) { ?>
                                                    <option  selected="selected" value="<?php echo $rowterm1['term_id']; ?>"><?php echo $rowterm1['term']; ?></option>
                                            <?php } ?>
                                            <?php while ($Rowsterm2 = mysql_fetch_array($Rterm2)) { ?>
                                                    <option value="<?php echo $Rowsterm2['term_id']; ?>"><?php echo $Rowsterm2['term']; ?></option>
                                            <?php } ?>
                                            </select></td>
                                        <td width="14%" align="left"><label>Session :</label></td>
                                        <td align="left"><?php
                                            if (isset($session_id)) {
                                                $Qsession2 = "SELECT * FROM tbl_session WHERE session_id != '$session_id'";
                                                $Rsession2 = mysql_query($Qsession2);
                                                $Qsession1 = "SELECT * FROM tbl_session WHERE session_id = '$session_id'";
                                                $Rsession1 = mysql_query($Qsession1);
                                                $rowsession1 = mysql_fetch_array($Rsession1);
                                            } else {
                                                $Qsession2 = "SELECT * FROM tbl_session ORDER BY session_name ASC";
                                                $Rsession2 = mysql_query($Qsession2);
                                            }
                                            ?>
                                            <select name="session_id" class="" id="session_id">
                                                <option value="">Select session</option>
                                            <?php if (isset($session_id)) { ?>
                                                    <option  selected="selected" value="<?php echo $rowsession1['session_id']; ?>"><?php echo $rowsession1['session_name']; ?></option>
                                            <?php } ?>
                                            <?php while ($Rowssession2 = mysql_fetch_array($Rsession2)) { ?>
                                                    <option value="<?php echo $Rowssession2['session_id']; ?>"><?php echo $Rowssession2['session_name']; ?></option>
                                            <?php } ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td width="18%" align="right">&nbsp;</td>
                                        <td width="28%" align="left">&nbsp;</td>
                                        <td><label>Class :</label></td>
                                        <td width="40%" align="left"><?php
                                            if (isset($class_id)) {
                                                $Qclass2 = "SELECT * FROM tbl_class WHERE class_id != '$class_id'";
                                                $Rclass2 = mysql_query($Qclass2);
                                                $Qclass1 = "SELECT * FROM tbl_class WHERE class_id = '$class_id'";
                                                $Rclass1 = mysql_query($Qclass1);
                                                $rowclass1 = mysql_fetch_array($Rclass1);
                                            } else {
                                                $Qclass2 = "SELECT * FROM tbl_class ORDER BY class_name ASC";
                                                $Rclass2 = mysql_query($Qclass2);
                                            }
                                            ?>
                                            <select name="class_id" class="" id="class_id">
                                                <option value="">Select class</option>
                                            <?php if (isset($class_id)) { ?>
                                                    <option  selected="selected" value="<?php echo $rowclass1['class_id']; ?>"><?php echo $rowclass1['class_name']; ?></option>
                                            <?php } ?>
                                            <?php while ($Rowsclass2 = mysql_fetch_array($Rclass2)) { ?>
                                                    <option value="<?php echo $Rowsclass2['class_id']; ?>"><?php echo $Rowsclass2['class_name']; ?></option>
                                            <?php } ?>
                                            </select></td>
                                    </tr>



                                    <tr>
                                        <td colspan="3" align="left" style="font-size:16px; font-family:arial; font-weight:bold; margin-left:40px;">Fee Items </td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="right"><hr color="#FF0000"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="label">
                                                <label for="student_city">Admission Form:</label>
                                            </span></td>
                                        <td align="left"><span class="style29"><span class="text-input-bg">
                                                    <input id="admission_form" name="admission_form" size="30" type="text" />
                                                </span></span></td>
                                        <td align="left"><span class="label">
                                                <label for="student_city">Tuition Fee:</label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="tuition_fee" name="tuition_fee" size="30" type="text" />
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="label">
                                                <label for="student_city">Textbook:</label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="textbook" name="textbook" size="30" type="text" />
                                            </span></td>
                                        <td align="left"><span class="label">
                                                <label for="student_state">Uniform</label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="uniform" name="uniform" size="30" type="text" />
                                            </span></td>
                                    </tr>



                                    <tr>
                                        <td align="left"><span class="label">
                                                <label for="student_phone1">Jnr. WAEC </label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="jur_waec" name="jur_waec" size="30" type="text" />
                                            </span></td>
                                        <td align="left"><span class="label">
                                                <label for="label">Games</label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="games" name="games" size="30" type="text" />
                                            </span></td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="label">
                                                <label for="student_email">Development</label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="development" name="development" size="30" type="text" />
                                            </span></td>
                                        <td align="left">
                                            <label for="student_email">Library</label>
                                        </td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="library" name="library" size="30" type="text" />
                                            </span></td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="label">
                                                <label for="student_email">Exam</label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="exam" name="exam" size="30" type="text" />
                                            </span></td>
                                        <td><label>Practical</label></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="practical" name="practical" size="30" type="text" />
                                            </span></td>
                                    </tr>

                                    <tr>
                                        <td><label>PTA</label></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="pta" name="pta" size="30" type="text" />
                                            </span></td>
                                        <td><label>Project</label></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="project" name="project" size="30" type="text" />
                                            </span></td>
                                    </tr>

                                    <tr>
                                        <td><label>Transport</label></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="transport" name="transport" size="30" type="text" />
                                            </span></td>
                                        <td><label>Excursion</label></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="excursion" name="excursion" size="30" type="text" />
                                            </span></td>
                                    </tr>

                                    <tr>
                                        <td><label>Lesson</label></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="lesson" name="lesson" size="30" type="text" />
                                            </span></td>
                                        <td><label>Sport Wear </label></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="sport_wear" name="sport_wear" size="30" type="text" />
                                            </span></td>
                                    </tr>

                                    <tr>
                                        <td><label>Computer</label></td>
                                        <td ><span class="text-input-bg">
                                                <input id="computer" name="computer" size="30" type="text" />
                                            </span></td>
                                        <td><label>Meal</label></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="meal" name="meal" size="30" type="text" />
                                            </span></td>
                                    </tr>

                                    <tr>
                                        <td><label>Graduation</label></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="graduation" name="graduation" size="30" type="text" />
                                            </span></td>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="right">&nbsp;</td>
                                        <td align="right"><span class="style5"><div id="dstat" align="center"></div></span></td>
                                        <td align="left"><label>
                                                <input name="Submit" type="submit" class="btn btn-red" value="Save" />
                                            </label></td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>