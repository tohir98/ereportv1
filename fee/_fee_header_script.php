<?php session_start();
// error_reporting(0);
set_time_limit(0);
include("../connect.php");
include("../fns.php");
include("../bin/make_safe.php");
?><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="../css_main/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../css_main/text.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../css_main/grid.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../css_main/layout.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../css_main/nav.css" media="screen" />
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
<link href="../css_main/table/demo_page.css" rel="stylesheet" type="text/css" />
<!-- BEGIN: load jquery -->
<script src="../js/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery-ui/jquery.ui.core.min.js"></script>
<script src="../js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
<script src="../js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
<script src="../js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
<script src="../js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
<script src="../js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="../js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
<script src="../js/table/jquery.dataTables.min.js" type="text/javascript"></script>
<!-- END: load jquery -->
<script type="text/javascript" src="../js/table/table.js"></script>
<script src="../js/setup.js" type="text/javascript"></script>