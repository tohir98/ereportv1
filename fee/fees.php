<?php session_start();
set_time_limit(0);
error_reporting(0);
include("../connect.php");
include("../fns.php");
?><head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Typography | BlueWhale Admin</title>
    <link rel="stylesheet" type="text/css" href="../css_main/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../css_main/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../css_main/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../css_main/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../css_main/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="../css_main/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="../js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="../js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="../js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="../js/table/table.js"></script>
    <script src="../js/setup.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
			setSidebarHeight();


        });
    </script>
</head>
<?php
		$select = "SELECT f.fee_id, s.session_name, c.class_name, t.term, f.admission_form+f.computer+f.development+f.exam+f.excursion+f.games+f.graduation+f.jur_waec+f.lesson+f.library+f.meal+f.practical+f.project+f.pta+f.sport_wear+f.textbook+f.transport+f.tuition_fee+f.uniform Fee
					FROM tbl_fee f INNER JOIN tbl_class c ON f.class_id=c.class_id
					INNER JOIN tbl_term t ON f.term_id=t.term_id
					INNER JOIN tbl_session s ON f.session_id=s.session_id ";
	$result= mysql_query($select);
?>

<div class="box round first grid">
                <h2>
                    School Fees Schedule &nbsp;</h2>
                <div class="block">
                    
                    
                    
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							
							<th>ID</th>
							<th>Session</th>
							<th>Class</th>
							<th>Term</th>
							<th>AmountPayable(N)</th>
						</tr>
					</thead>
					<tbody>
					<?php
					
					$i = 0;
					while($row = mysql_fetch_array($result)) 
					{
					
					?>
						<tr class="odd gradeX">
						 
					
						  <td><?php echo $row[0] ?></td>
						  <td><?php echo $row[1] ?></td>
						  <td><?php echo $row[2] ?></td>
						  <td><?php echo $row[3] ?></td>
						  <td><?php echo number_format($row[4],2) ?></td>
						</tr>
						<?php
					  }
					  ?>
					
						
					</tbody>
				</table>
                    
                    
                    
                </div>
            </div>