<?php

/*
 * Company Name: EduPortal ltd
 * ControllerName
 * 
 * @category   Function
 * @package    Users
 * @subpackage Users
 * @author     Tohir O. <otcleantech@gmail.com>
 * @copyright  Copyright � 2014
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

function opendatabase() {
    $db1 = mysqli_connect(MYSQLHOST, MYSQLUSER, MYSQLPASS, MYSQLDB);
    try {
        if (!$db1) {
            $exceptionstring = "Error connecting to database: <br />";
            $exceptionstring .= mysql_errno() . ": " . mysql_error();
            throw new exception($exceptionstring);
        }

        return $db1;
    } catch (exception $e) {
        echo $e->getmessage();
        die();
    }
}

function delete($table, $where) {
    $result = ExecuteSQLQuery("DELETE FROM  $table WHERE $where ");
    if ($result)
        return true;
    else
        return false;
}

//end function delete($table,$where)

/*
 *
 *
 */

function check($table, $col, $value) {
    $result = ExecuteSQLQuery("SELECT * FROM  $table WHERE $col = '$value' ");
    if (mysqli_fetch_row($result) > 0) {
        return true;
    } else {
        return false;
    }
}

//End function check($table, $col, $value)



/*
 * Get a col from a table
 *
 */

//function getField($tablename, $keyfield, $column, $value) {   // table, keyfield, column, value
//    $Query = "SELECT $column FROM $tablename WHERE $keyfield = '$value' ";
//    $Rtu = mysqli_query($dbhandle, $Query);
//    while ($row_intt = mysql_fetch_array($Rtu)) {
//        $field = $row_intt[0];
//    }
//    return $field;
//}

/*
 * Get last ID
 *
 */

function getLastId($tablename, $id_col) {   // table, keyfield, column, value
    $Rno = ExecuteSQLQuery("SELECT max($id_col) FROM $tablename ");
    if (mysqli_num_rows($Rno) > 0) {
        $row = mysqli_fetch_array($Rno);
        return $row[0];
    } else {
        return 0;
    }
}

//end func getLastId

/*
 * Get base_url
 *
 */

function base_url() {   // base_url
    return "home.php?";
}

//end func base_url

function ExecuteSQLQuery($Query, $dbName = '') {
    $db = opendatabase();
    if ($dbName == "")
        $dbName = $db;
    $re2 = mysqli_query($dbName, $Query);
    mysqli_close($db);
    return $re2;
}

function Redirect($url, $permanent = false) {
    header('Location: ' . $url, true, $permanent ? 301 : 302);

    exit();
}

function updateSettings(array $data = [], $file = []) {

    if (empty($data)) {
        return FALSE;
    }

    $msg = 'Settings updated successfully';
    $school_id = $_SESSION['ereport']['account_id'];
    $school_name = $data['school_name'];
    $address = $data['address'];
    $phone = $data['phone'];
    $email = $data['email'];
    $date_updated = date('Y-m-d h:i:s');

    $logo = '';
    if (!empty($file)) {
        $fileType = $file['type'];
        $logo = $file['name'];
        $target_path = "../upload/";
        if ($fileType === "image/jpeg" || $fileType === "image/png" || $fileType === "image/gif") {
            move_uploaded_file($file['tmp_name'], $target_path . $logo);
        } else {
            $msg = "Invalid picture";
        }
    }


    if (check('tbl_settings', 'school_id', $school_id)) {
        $sql = "UPDATE tbl_settings SET school_name = '$school_name', address = '$address', phone = '$phone', email = '$email', date_updated = '$date_updated' ";
        if ($logo && $logo !== '') {
            $sql .= " ,logo = '$logo' ";
        }
        $sql .= " WHERE school_id = '$school_id' ";
        ExecuteSQLQuery("UPDATE tbl_account SET school_name = '$school_name' WHERE account_id = '$school_id' ");
    } else {
        $sql = "INSERT INTO tbl_settings VALUES (null, '$school_id', '$school_name', '$address', '$phone', '$email', $logo, '$date_updated')";
    }

    ExecuteSQLQuery($sql);

    return $msg;
}
