<?php session_start();
error_reporting(E_ERROR);
  
include ("../datagrid2.class.php");
include ("../conit.php");

$objGrid = new datagrid;
$objGrid -> friendlyHTML();
$objGrid -> pathtoimages("../images/");
$objGrid -> closeTags(true);  
$objGrid -> form('Deduction', true);
$objGrid -> methodForm("post"); 
$objGrid -> total("loan_repay,welfare,coop,guarantee,tax");
$objGrid -> searchby("employee_id");
#$objGrid -> linkparam("sess=".$_REQUEST["sess"]."&username=".$_REQUEST["username"]);	 
$objGrid -> decimalDigits(2);
$objGrid -> decimalPoint(",");
#$objGrid -> conectadb("localhost", "root", "p3ac3dr1v3db", "emis");
$objGrid -> conectadb($servername, $username, $password, $databasename);
$objGrid -> tabla ("tbl_deductions");
$objGrid -> lgpage ("apDiv","http://$wwwroot/staffinfo/deductions2.php","180","400");
$objGrid -> buttons(true,true,true,true);
$objGrid -> keyfield("deductions_id");
#$objGrid -> salt("Some Code4Stronger(Protection)");
$objGrid -> TituloGrid("Deduction List");
#$objGrid -> FooterGrid("<div style='float:left'></div>");
$objGrid -> datarows(10);
$objGrid -> paginationmode('mixed');
#$objGrid -> orderby("month", "ASC");
#$objGrid -> groupby("year", "ASC");
$objGrid -> noorderarrows();
$objGrid -> FormatColumn("employee_id", "Employee", 5, 5, 0, "60", "center", "select:select employee_id, concat(firstname,' ',othernames) from tbl_employee");
$objGrid -> FormatColumn("loan_repay", "Loan Repay ", 50, 100, 0, "100", "left","","","","","","");
$objGrid -> FormatColumn("welfare", "Welfare",30, 50, 0, "70", "left","","","","","","");
$objGrid -> FormatColumn("coop", "Coop",30, 50, 0, "70", "left","","","","","","");
$objGrid -> FormatColumn("guarantee", "Guarantee",30, 50, 0, "70", "left","","","","","","");
$objGrid -> FormatColumn("tax", "Tax",30, 50, 0, "70", "left","","","","","","");
$objGrid -> FormatColumn("month", "Month",30, 50, 0, "70", "left","","","","","","");
$objGrid -> FormatColumn("year", "Year",30, 50, 0, "70", "left","","","","","","");
/*$objGrid -> where ("active = '1'");*/

$objGrid -> setHeader();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" media="all" href="../niceforms-default.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<style type="text/css">
#apDiv {
	position:absolute;
	width:600px;
	z-index:1;
	left: 264px;
	top: 105px;
	visibility: hidden;
}
-->
</style>
<script language="javascript" type="text/javascript" src="../JSFiles/dgscripts.js"></script>
<script language="javascript" type="text/javascript" src="../niceforms.js"></script>
</head>

<body>
   <div id="apDiv">
      <div id="container">

     <form action="" method="" enctype="multipart/form-data" name="frmchk" class="niceform" id="frmchk">  
<fieldset>
    	
  
      <table width="99%" border="0" cellpadding="0" cellspacing="0" bgcolor="f2f2e6">
                 <tr><td height="25" align="right" bgcolor="f2f2e6">
                  <?php echo  "<a href='javascript:CloseForm(\"apDiv\",\"http://$wwwroot/staffinfo/deduction_grid.php\");' class=\"next\">"."<img border='0' src='../images/close.png' width='16' height='16' />"."</a>";?>&nbsp;</td>
        </tr>
 <tr>
          <td valign="top">
          <div id="mydiv" style="border:none"></div>
          </td>
        </tr>  
       
    
</table>

</fieldset></form></div></div>

</div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><?php 
	/* AJAX inline edition comes in two flawors */
	/*	silent: To save record, just enter or double click */
	/*	default: To save record, must click icon */
	/* try yourself and see which one likes more (My preferred is silent) */
	$objGrid -> ajax("silent");

	$objGrid -> grid();
	
	$objGrid -> desconectar();
?></td>
  </tr>
</table>
</body>
</html>
