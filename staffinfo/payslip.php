<?php
session_start();
if(!session_is_registered('userid') && !session_is_registered('user') )
 {
  header("Location: ../index.php");
  }

include("../conit.php");
include("../make_safe.php");
if (isset($_GET['id']) and isset($_GET['month'])){
$id = $_GET['id']; 
$month = $_GET["month"]; 
$year = $_GET["year"];

$Qchker = "select employee_id, loan_repay, welfare, coop, guarantee, tax, loan_repay+welfare+coop+guarantee+tax total from tbl_deductions where employee_id = '$id' and month = '$month' ";
$Rchker = mysql_query($Qchker);
if (mysql_num_rows($Rchker) > 0){
$RowRchker = mysql_fetch_array($Rchker);
$loan_repay = $RowRchker['loan_repay'];
$welfare = $RowRchker['welfare'];
$coop = $RowRchker['coop'];
$guarantee = $RowRchker['guarantee'];
$tax = $RowRchker['tax'];
$employee_id = $RowRchker['employee_id'];
$total_deduction = $RowRchker['total'];

$earnings = getTotalE($id, $month, $year);


$edit = "1";
}}
$Qemp_id = "select firstname, othernames from tbl_employee where employee_id = '$employee_id' ";
		$Remp_id = mysql_query($Qemp_id);
		while($row_rsResult = mysql_fetch_array($Remp_id)) 
		{ 
			$emp_name=$row_rsResult['firstname']." ".$row_rsResult['othernames'];
			//$level_id =$row_rsResult['level_id'];
		}
	/*	
	$Qlevel_id = "select salary, arrears+sundries+salary-tax earnings from tbl_level where level_id = '$level_id' ";
		$Rlevel_id = mysql_query($Qlevel_id);
		while($row_rsResult2 = mysql_fetch_array($Rlevel_id)) 
		{ 
			$salary=$row_rsResult2['salary'];
			$earnings=$row_rsResult2['earnings'];
		}*/
	$Qall= "select * from tbl_earnings where employee_id = '$id' and month = '$month'  ";
		$Rall = mysql_query($Qall);
		while($row_intt = mysql_fetch_array($Rall)) 
		{ 
			$basic=$row_intt["basic"];
			$housing=$row_intt["housing"];
			$transport=$row_intt["transport"];
			$utility=$row_intt["utility"];
			$loan=$row_intt["loan"];
			$advance=$row_intt["advances"];
					
		}
?>





<html>

<head>
<link rel="shortcut icon" href="../icons/Checked.ico">
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<title>Payment Slip: Living Spring College of Technology</title>
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.25in 1.0in 1.25in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
.style11 {font-family: "Trebuchet MS"; font-size: 12pt; color: #FF0000; font-weight: bold; }
.style25 {font-size: 11pt; font-family: "Trebuchet MS";}
.style26 {
	color: #0000FF;
	font-family: "Trebuchet MS";
	font-size: 24px;
}
-->
</style>


</head>

<body lang=EN-US style='tab-interval:.5in'>

<table width="650" align="center">
	<tr> 
		<td align="center"> <h1 class="style26">PAYMENT SLIP </h1></td>
	</tr>
		<tr> 
		<td> Staff ID<span class="style11"> :  <span class="style11"><?php echo $employee_id; ?></span></span></td>
	</tr>
		<tr>
		  <td>Staff Name ; &nbsp;<span class="style11"><?php echo $emp_name; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date: 
		    <?php 
	$d=date('d-M-Y');
	echo $d;
?>		  </td>
  </tr>
		<tr>
		  <td>Paying slip for the month of: <span class="style11"><?php echo $month." ".$year; ?></span></td>
  </tr>
		
		
		<tr>
		  <td>&nbsp;</td>
  </tr>
		

		<tr>
		  <td><table width="80%" border="0" align="center">
            <tr>
              <td width="47%">Basic</td>
              <td width="53%">&nbsp;<?php echo number_format($basic,2); ?></td>
              <td width="53%">&nbsp;</td>
            </tr>
            <tr>
              <td>Housing</td>
              <td>&nbsp;<?php echo number_format($housing,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Transport</td>
              <td>&nbsp;<?php echo number_format($transport,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Utility</td>
              <td>&nbsp;<?php echo number_format($utility,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Loan</td>
              <td>&nbsp;<?php echo number_format($loan,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Advance</td>
              <td>&nbsp;<?php echo number_format($advance,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Grosspay</td>
              <td><span class="style11">
                <?php	echo "N".number_format($earnings,2);  ?>
              </span></td>
              <td><span class="style11">
                <?php	echo "N".number_format($earnings,2);  ?>
              </span></td>
            </tr>
            <tr>
              <td>Less</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Loan Repayment</td>
              <td>&nbsp;<?php echo number_format($loan_repay,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Welfare Contr</td>
              <td>&nbsp;<?php echo number_format($welfare,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Coop Contr</td>
              <td>&nbsp;<?php echo number_format($coop,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Guarantee</td>
              <td>&nbsp;<?php echo number_format($guarantee,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Tax payable</td>
              <td>&nbsp;<?php echo number_format($tax,2); ?></td>
              <td>&nbsp;</td>
            </tr>
            
            <tr>
              <td><span class="style25">Total deductions: </span></td>
              <td><span class="style11"><?php echo "N".number_format($total_deduction,2); ?></span></td>
              <td>&nbsp;</td>
            </tr>
            
            <tr>
              <td class="style25"><strong>PAYABLE AMOUNT:</strong></td>
              <td>&nbsp;</td>
              <td><span class="style11">
                <?php
  		echo "N".number_format(($earnings-$total_deduction),2);
   ?>
              </span></td>
            </tr>
            <tr>
              <td align="right" class="style25">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            
            <tr>
              <td align="right" class="style25">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table></td>
  </tr>
		<tr>
		  <td>&nbsp;</td>
  </tr>
		
		
		<tr>
		  <td align="right">&nbsp;<a href="javascript:window.print()"><img src="../images/bt_print.jpg" alt="print" width="96" height="19" border="0" /></a></td>
  </tr>
</table>
</body>

</html>
