<?php
session_start();

include("../conit.php");
include("../bin/make_safe.php");

$mon = $_REQUEST['mon2'];
$year = trim($_REQUEST['cboyear2']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Monthly Report</title>
<script language="JavaScript" src="tigra_calendar/calendar_us.js"></script>
<link rel="stylesheet" href="tigra_calendar/calendar.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript" type="text/javascript" src="../niceforms.js"></script>

<script language="javascript" src="jst.js">
</script>
<style type="text/css">
<!--
.style3 {
	font-family: "Trebuchet MS";
	font-size: 22px;
}
.style6 {
	color: #000000;
	font-family: "Trebuchet MS";
	font-weight: bold;
}
.style7 {
	font-family: "Trebuchet MS";
	color: #000000;
}
.style9 {
	color: #000000;
	font-weight: bold;
}
.style10 {color: #000000}
-->
</style>
</head>
<body>
<div id="container">
  
    	<form  method="post" name="frmdeduction" class="niceform" id="frmdeduction">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
        
        <tr>
          <td><fieldset class="action">
          <div align="center" class="formheader style3" id="div"><?php echo strtoupper($_SESSION['ereport']['school_name']); ?><BR /> STAFF PAYROL FOR THE MONTH OF <?php echo strtoupper($mon)." ".$year; ?></div>
          </fieldset>
          
          <fieldset>
          <table width="100%" border="1" align="center" style="border:thin">
  <tr bgcolor="#ccc">
    <td align="center" valign="middle" bgcolor="#CCCCCC" style="color: #666666">&nbsp;</td>
    <td valign="middle" style="color:#666666">&nbsp;</td>
    <td valign="middle" style="color:#666666">&nbsp;</td>
    <td colspan="4" align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;SALARY</span></td>
    <td colspan="2" align="center" valign="middle" style="color:#666666"><span class="style6">ADVANCES</span></td>
    <td align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;GROSS</span></td>
    <td colspan="5" valign="middle" style="color:#666666" align="center"><span class="style6">&nbsp;DEDUCTIONS</span></td>
    <td valign="middle" style="color:#666666" align="center"><span class="style6">&nbsp;TOTAL</span></td>
    <td align="center" valign="middle" class="style6" style="color:#000000">NET</td>
  </tr>
  <tr bgcolor="#ccc">
    <td width="5%" align="center" valign="middle" bgcolor="#CCCCCC" style="color: #666666"><span class="style7 style1"><strong>S/N </strong></span></td>
    <td width="20%" valign="middle" style="color:#666666"><span class="style6">&nbsp;EMPLOYEE</span> </td>
    <td width="40" valign="middle" style="color:#666666"><span class="style6">&nbsp;POSITION</span></td>
	<td width="80" align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;Basic</span></td>
	<td width="40" align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;Housing</span></td>
<td width="75" align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;Transport</span></td>
<td width="75" align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;Utility</span></td>
<td width="75" align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;Loan</span></td>
<td width="75" align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;Advance</span></td>
<td width="40" align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;PAY</span></td>
	<td width="80" valign="middle" style="color:#666666"><span class="style6">&nbsp;Loan rpyt</span></td>
	<td width="40" valign="middle" style="color:#666666"><span class="style6">&nbsp;Welfare</span></td>
<td width="75" valign="middle" style="color:#666666"><span class="style6">&nbsp;Coop</span></td>
<td width="75" valign="middle" style="color:#666666"><span class="style6">&nbsp;Guartee</span></td>
<td width="75" valign="middle" style="color:#666666"><span class="style6">&nbsp;Tax</span></td>
<td width="40" valign="middle" style="color:#666666"><span class="style6">&nbsp;DEDUCTIONS</span></td>
<td width="40" align="center" valign="middle" style="color:#666666"><span class="style6">&nbsp;PAY</span></td>
  </tr>
  
  <?php
  
	
	$today = date("Y-m-d");			
$qry = "SELECT *
FROM tbl_earnings WHERE month = '$mon' and year = '$year' ";
$Rqry = mysql_query($qry);
   if (mysql_num_rows($Rqry)>0){
  $counter = 1;
  $bgcolor = '#e9eff2';
  while ($rowRqry = mysql_fetch_array($Rqry)){
  
  
  if ($bgcolor =='#e9eff2'){$bgcolor ='#Fff';
  }else{
  $bgcolor ='#e9eff2';}

  $employee_id = $rowRqry["employee_id"];
  $housing = number_format($rowRqry["housing"], 0);
  $transport = number_format($rowRqry["transport"], 0);
  $utility = number_format($rowRqry["utility"], 0);
  $loan = number_format($rowRqry["loan"], 0);
  $advances = number_format($rowRqry["advances"], 0);
  $basic = number_format($rowRqry["basic"], 0);
  $tot = $rowRqry["basic"] + $rowRqry["advances"] + $rowRqry["loan"] + $rowRqry["utility"] + $rowRqry["transport"] + $rowRqry["housing"
];

$dd = number_format(getTotalD($employee_id, $mon, $year),2);
$tax = number_format(getTax($employee_id, $mon, $year),2);
$Guartee = number_format(getGuartee($employee_id, $mon, $year),2);
$coop = number_format(getCoop($employee_id, $mon, $year),2);
$welfare = number_format(getWalfare($employee_id, $mon, $year),2);
$loan_repay = number_format(getLoan_rpyt($employee_id, $mon, $year),2);

$net = number_format($tot-getTotalD($employee_id, $mon, $year),2);
//$net = number_format($net,2);
$tot = number_format($tot,2);
  $month = date('M');
  $year = $rowRqry["year"];
 // $year = date('Y');
  
 $Qacct = "select firstname, othernames, level_id from tbl_employee where employee_id = '$employee_id' ";
		$Racct = mysql_query($Qacct);
		while($row_rsResult = mysql_fetch_array($Racct)) 
		{ 
			$emp_name=$row_rsResult['firstname']." ".$row_rsResult['othernames'];
			$level_id=$row_rsResult['level_id'];
		}
		$position = getPosition($level_id);

echo "  
  <tr bgcolor=\"$bgcolor\">
    <td align='center'>&nbsp;$counter</td>
	<td>&nbsp;<a href=\"payslip.php?id=$employee_id&month=$mon&year=$year\">$emp_name</a></td>
	<td>&nbsp;$position</td>
	<td>&nbsp;$basic</td>
	<td>&nbsp;$housing</td>
	<td>&nbsp;$transport</td>
	<td>&nbsp;$utility</td>
	<td>&nbsp;$loan</td>
	<td>&nbsp;$advances</td>
	<td>&nbsp;$tot</td>
	<td>&nbsp;$loan_repay</td>
	<td>&nbsp;$welfare</td>
	<td>&nbsp;$coop</td>
	<td>&nbsp;$Guartee</td>
	<td>&nbsp;$tax</td>
	<td>&nbsp;$dd</td>
	<td>&nbsp;$net</td>
 
  </tr>";
  $counter++; 
  //echo $count;
  }
  
  
  
  }
  
  
  ?>
  <tr bgcolor="#ccc">
    <td align="center" valign="middle" bgcolor="#CCCCCC" style="color: #666666">&nbsp;</td>
    <td valign="middle" style="color:#666666"><span class="style9">GRAND TOTAL </span></td>
    <td valign="middle" style="color:#666666"><span class="style10"></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_earnings", "basic", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_earnings", "housing", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_earnings", "transport", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_earnings", "utility", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_earnings", "loan", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_earnings", "advances", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrandEarning($mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_deductions", "loan_repay", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_deductions", "welfare", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_deductions", "coop", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_deductions", "guarantee", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrand("tbl_deductions", "tax", $mon, $year),2); ?></strong></span></td>
    <td align="left" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format(getGrandDeduction($mon, $year),2); ?></strong></span></td>
    <td align="center" valign="middle" style="color:#666666"><span class="style10"><strong><?php echo number_format((getGrandEarning($mon, $year)-getGrandDeduction($mon, $year)),2);  ?></strong></span></td>
  </tr>
</table>
          </fieldset><br /></td>
        </tr>
        <tr>
          <td align="right">
          </td>
        </tr>
      </table>
  </form>
</div>
</body>
</html>
