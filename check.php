<?php
	include 'bin/Connection.php';
	include "bin/make_safe.php";
	$con = new Connection();
	$con->ConnectioManager();
?><head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" type="text/css" href="css_main/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css_main/fancy-button/fancy-button.css" rel="stylesheet" type="text/css" />
    <!--Jquery UI CSS-->
    <link href="css_main/themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <!--jQuery Date Picker-->
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.datepicker.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.progressbar.min.js" type="text/javascript"></script>
    <!-- jQuery dialog related-->
    <script src="js/jquery-ui/external/jquery.bgiframe-2.1.2.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.draggable.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.position.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.resizable.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.dialog.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.blind.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.explode.min.js" type="text/javascript"></script>
    <!-- jQuery dialog end here-->
    <!--Fancy Button-->
    <script src="js/fancy-button/fancy-button.js" type="text/javascript"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <!-- Load TinyMCE -->
    <script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setupTinyMCE();
            setupProgressbar('progress-bar');
            setDatePicker('date-picker');
            setupDialogBox('dialog', 'opener');
            $('input[type="checkbox"]').fancybutton();
            $('input[type="radio"]').fancybutton();
        });
    </script>
    <!-- /TinyMCE -->
    <style type="text/css">
        #progress-bar
        {
            width: 400px;
        }
    </style>
</head>




<div class="box round first grid">
                <h2>
                    Check Result</h2>
                <div class="block ">
<form  method="post"  name="frmupload" id="frmupload" action="reportSheet.php" target="_blank">
        <table class="form">
          <tbody>
            
            <tr>
              <td width="14%"><label>Class:*</label></td>
              <td width="30%" align="left"><?php 
			if (isset($class_id )){
			$Qclass2 = "SELECT * FROM tbl_class WHERE class_id != '$class_id'";
$Rclass2 = mysql_query($Qclass2);
			$Qclass1 = "SELECT * FROM tbl_class WHERE class_id = '$class_id'";
$Rclass1 = mysql_query($Qclass1);
$rowclass1 = mysql_fetch_array($Rclass1);}else{
$Qclass2 = "SELECT * FROM tbl_class ORDER BY class_name ASC";
$Rclass2 = mysql_query($Qclass2);
}
?>
                <select required name="class_id" class="" id="class_id" onChange="getStudent(this.value)">
                  <option value="">Select class</option>
                  <?php if (isset($class_id )){?>
                  <option  selected="selected" value="<?php echo $rowclass1['class_id']; ?>"><?php echo $rowclass1['class_name'];?></option>
                  <?php }?>
                  <?php while($Rowsclass2=mysql_fetch_array($Rclass2)){ ?>
                  <option value="<?php echo $Rowsclass2['class_id']; ?>"><?php echo $Rowsclass2['class_name'];?></option>
                  <?php } ?>
              </select></td>
              <td width="56%" align="left">&nbsp;</td>
            </tr>
            
            <tr>
              <td><label>Student:</label></td>
              <td align="left"><div id="studentdiv">
                <select required name="student_id" id="student_id">
                  <option value="">Select Class First</option>
                </select>
              </div></td>
              <td align="left"><input type="hidden" name="pid" id="pid" value="<?php if (isset($id)){
			  echo $id;
			}?>" /></td>
            </tr>
            <tr>
              <td><label>Term:</label></td>
              <td align="left"><?php 
			if (isset($term_id )){
			$Qterm2 = "SELECT * FROM tbl_term WHERE term_id != '$term_id'";
$Rterm2 = mysql_query($Qterm2);
			$Qterm1 = "SELECT * FROM tbl_term WHERE term_id = '$term_id'";
$Rterm1 = mysql_query($Qterm1);
$rowterm1 = mysql_fetch_array($Rterm1);}else{
$Qterm2 = "SELECT * FROM tbl_term ORDER BY term ASC";
$Rterm2 = mysql_query($Qterm2);
}
?>
                <select required name="term_id" class="" id="term_id">
                  <option value="">Select term</option>
                  <?php if (isset($term_id )){?>
                  <option  selected="selected" value="<?php echo $rowterm1['term_id']; ?>"><?php echo $rowterm1['term'];?></option>
                  <?php }?>
                  <?php while($Rowsterm2=mysql_fetch_array($Rterm2)){ ?>
                  <option value="<?php echo $Rowsterm2['term_id']; ?>"><?php echo $Rowsterm2['term'];?></option>
                  <?php } ?>
                </select></td>
              <td align="left">&nbsp;</td>
            </tr>
            <tr>
              <td><label>Session:</label></td>
              <td align="left"><?php 
			if (isset($session_id )){
			$Qsession2 = "SELECT * FROM tbl_session WHERE session_id != '$session_id'";
$Rsession2 = mysql_query($Qsession2);
			$Qsession1 = "SELECT * FROM tbl_session WHERE session_id = '$session_id'";
$Rsession1 = mysql_query($Qsession1);
$rowsession1 = mysql_fetch_array($Rsession1);}else{
$Qsession2 = "SELECT * FROM tbl_session ORDER BY session_name ASC";
$Rsession2 = mysql_query($Qsession2);
}
?>
                <select required name="session_id" class="" id="session_id">
                  <option value="">Select session</option>
                  <?php if (isset($session_id )){?>
                  <option  selected="selected" value="<?php echo $rowsession1['session_id']; ?>"><?php echo $rowsession1['session_name'];?></option>
                  <?php }?>
                  <?php while($Rowssession2=mysql_fetch_array($Rsession2)){ ?>
                  <option value="<?php echo $Rowssession2['session_id']; ?>"><?php echo $Rowssession2['session_name'];?></option>
                  <?php } ?>
                </select></td>
              <td align="left">&nbsp;</td>
            </tr>
            
            
            <tr>
              <td align="right">&nbsp;</td>
              <td align="left"><label>
                <input name="btnCheck" type="submit" class="btn btn-blue" id="btnCheck" value="Submit" />
                <input name="Reset" type="reset" class="btn btn-blue" value="Reset" />
              </label></td>
              <td align="left">&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </form>
</div>
</div>
<script language="javascript" type="text/javascript">
function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
	function getStudent(classId) {		
		
		var strURL="findStudent.php?class_id="+classId;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('studentdiv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}		
	}
</script>