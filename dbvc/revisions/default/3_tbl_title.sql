CREATE TABLE IF NOT EXISTS `tbl_title` (
  `title_id` int(25) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_title
-- ----------------------------
INSERT INTO `tbl_title` VALUES ('1', 'Mr.');
INSERT INTO `tbl_title` VALUES ('2', 'Mrs.');
INSERT INTO `tbl_title` VALUES ('3', 'Miss.');
