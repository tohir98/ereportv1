CREATE TABLE IF NOT EXISTS `tbl_log` (
  `log_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(25) unsigned NOT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `machine_name` varchar(150) DEFAULT NULL,
  `operation` varchar(200) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `fname` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `FK_tbl_log_1` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;