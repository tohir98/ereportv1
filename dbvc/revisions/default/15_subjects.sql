CREATE TABLE IF NOT EXISTS `tbl_subject` (
  `subject_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(150) DEFAULT NULL,
  `subject_desc` varchar(150) DEFAULT NULL,
  `sch_id` int(11) NOT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;