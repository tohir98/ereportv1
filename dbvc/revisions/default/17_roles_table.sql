CREATE TABLE  IF NOT EXISTS `tbl_role` (
  `role_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `Firstname` varchar(100) DEFAULT NULL,
  `Lastname` varchar(100) DEFAULT NULL,
  `user_type_id` int(25) unsigned DEFAULT NULL,
  `islocked` varchar(5) DEFAULT 'no',
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  KEY `FK_tbl_role_1` (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;