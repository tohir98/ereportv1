CREATE TABLE IF NOT EXISTS `tbl_states` (
  `states_id` int(10) NOT NULL,
  `StateName` varchar(200) NOT NULL,
  `StateCapital` varchar(50) DEFAULT NULL,
  `StateCode` char(10) DEFAULT NULL,
  `Deleted` varchar(50) DEFAULT NULL,
  `DeletedBy` varchar(30) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`states_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_states
-- ----------------------------
INSERT INTO `tbl_states` VALUES ('1', 'Abia', '', 'AB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('2', 'Adamawa', '', 'AD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('3', 'Akwa Ibom', '', 'AK', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('4', 'Anambra', '', 'AN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('5', 'Bauchi', '', 'BA', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('6', 'Bayelsa', '', 'BY', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('7', 'Benue', '', 'BN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('8', 'Borno', '', 'BO', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('9', 'Cross Rivers', '', 'CR', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('10', 'Delta', '', 'DT', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('11', 'Ebonyi', '', 'EB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('12', 'Edo', '', 'ED', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('13', 'Ekiti', '', 'EK', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('14', 'Enugu', '', 'EN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('15', 'FCT', '', 'FC', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('16', 'Gombe', '', 'GM', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('17', 'Imo', '', 'IM', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('18', 'Jigawa', '', 'JG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('19', 'Kaduna', '', 'KD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('20', 'Kano', '', 'KN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('21', 'Katsina', '', 'KT', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('22', 'Kebbi', '', 'KB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('23', 'Kogi', '', 'KG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('24', 'Kwara', '', 'KW', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('25', 'Lagos', '', 'LA', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('26', 'Nassarawa', '', 'NS', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('27', 'Niger', '', 'NG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('28', 'Ogun', '', 'OG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('29', 'Ondo', '', 'OD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('30', 'Osun', '', 'OS', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('31', 'Oyo', '', 'OY', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('32', 'Plateau', '', 'PL', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('33', 'Rivers', '', 'RV', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('34', 'Sokoto', '', 'SO', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('35', 'Taraba', '', 'TR', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('36', 'Yobe', '', 'YB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('37', 'Zamfara', '', 'ZM', '', '', '2008-08-02 18:55:22');
