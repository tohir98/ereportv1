CREATE TABLE  IF NOT EXISTS `ereport`.`tbl_scorebank` (
  `scoreBank_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(25) unsigned DEFAULT NULL,
  `subject_id` int(25) unsigned DEFAULT NULL,
  `student_code` varchar(150) DEFAULT NULL,
  `ca` double DEFAULT NULL,
  `exam` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `remark` varchar(150) DEFAULT NULL,
  `term_id` int(25) unsigned DEFAULT NULL,
  `session_id` int(25) unsigned DEFAULT NULL,
  `sch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`scoreBank_id`),
  KEY `FK_tbl_scorebank_class` (`class_id`),
  KEY `FK_tbl_scorebank_subject` (`subject_id`),
  KEY `FK_tbl_scorebank_term` (`term_id`),
  KEY `FK_tbl_scorebank_session` (`session_id`),
  CONSTRAINT `FK_tbl_scorebank_classID` FOREIGN KEY (`class_id`) REFERENCES `tbl_class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_scorebank_session` FOREIGN KEY (`session_id`) REFERENCES `tbl_session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_scorebank_subjectID` FOREIGN KEY (`subject_id`) REFERENCES `tbl_subject` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_scorebank_term` FOREIGN KEY (`term_id`) REFERENCES `tbl_term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;