CREATE TABLE IF NOT EXISTS `tbl_deductions` (
  `deductions_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(25) unsigned DEFAULT NULL,
  `loan_repay` double DEFAULT NULL,
  `welfare` double DEFAULT NULL,
  `coop` double DEFAULT NULL,
  `guarantee` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `month` varchar(45) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`deductions_id`),
  KEY `FK_tbl_deductions_1` (`employee_id`),
  CONSTRAINT `tbl_deductions_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;