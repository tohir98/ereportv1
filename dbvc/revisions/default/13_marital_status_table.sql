CREATE TABLE IF NOT EXISTS `tbl_marital_status` (
  `marital_status_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `marital_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`marital_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_marital_status
-- ----------------------------
INSERT INTO `tbl_marital_status` VALUES ('1', 'Single');
INSERT INTO `tbl_marital_status` VALUES ('2', 'Married');
INSERT INTO `tbl_marital_status` VALUES ('3', 'Divorced');
INSERT INTO `tbl_marital_status` VALUES ('4', 'Separated');
