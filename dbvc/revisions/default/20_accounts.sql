CREATE TABLE IF NOT EXISTS `tbl_account` (
  `account_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `school_id` int(25) unsigned DEFAULT NULL,
  `school_name` varchar(150) DEFAULT NULL,
  `school_shortname` varchar(150) DEFAULT NULL,
  `school_address` varchar(350) DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;