CREATE TABLE IF NOT EXISTS `tbl_psychomotor` (
  `psychomotor_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `attentiveness` int(25) unsigned DEFAULT NULL,
  `punctuality` int(25) unsigned DEFAULT NULL,
  `honest` int(25) unsigned DEFAULT NULL,
  `dedication` int(25) unsigned DEFAULT NULL,
  `neatness` int(25) unsigned DEFAULT NULL,
  `relationship` int(25) unsigned DEFAULT NULL,
  `commitment` int(25) unsigned DEFAULT NULL,
  `politeness` int(25) unsigned DEFAULT NULL,
  `responsibility` int(25) unsigned DEFAULT NULL,
  `obedience` int(25) unsigned DEFAULT NULL,
  `admission_id` int(25) unsigned DEFAULT NULL,
  `teacher_comment` varchar(250) DEFAULT NULL,
  `principal_comment` varchar(250) DEFAULT NULL,
  `session_id` int(25) unsigned DEFAULT NULL,
  `term_id` int(25) unsigned DEFAULT NULL,
  `no_in_classs` int(25) unsigned DEFAULT NULL,
  `times_present` int(25) unsigned DEFAULT NULL,
  `times_absent` int(25) unsigned DEFAULT NULL,
  `next_term_begins` varchar(45) DEFAULT NULL,
  `next_term_ends` varchar(45) DEFAULT NULL,
  `sch_id` int(25) unsigned DEFAULT NULL,
  `class_id` int(25) NOT NULL,
  PRIMARY KEY (`psychomotor_id`),
  KEY `FK_tbl_psychomotor_1` (`admission_id`),
  KEY `FK_tbl_psychomotor_2` (`session_id`),
  KEY `FK_tbl_psychomotor_3` (`term_id`),
  CONSTRAINT `FK_tbl_psychomotor_1` FOREIGN KEY (`admission_id`) REFERENCES `tbl_admission` (`admission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_psychomotor_2` FOREIGN KEY (`session_id`) REFERENCES `tbl_session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_psychomotor_3` FOREIGN KEY (`term_id`) REFERENCES `tbl_term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;