CREATE TABLE IF NOT EXISTS `tbl_religion` (
  `religion_id` int(25) NOT NULL AUTO_INCREMENT,
  `religion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`religion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_religion
-- ----------------------------
INSERT INTO `tbl_religion` VALUES ('1', 'Islam');
INSERT INTO `tbl_religion` VALUES ('2', 'Christianity');
