CREATE TABLE  IF NOT EXISTS `tbl_earnings` (
  `earnings_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(25) unsigned DEFAULT NULL,
  `housing` double DEFAULT NULL,
  `transport` double DEFAULT NULL,
  `utility` double DEFAULT NULL,
  `loan` double DEFAULT NULL,
  `advances` double DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `basic` double DEFAULT NULL,
  PRIMARY KEY (`earnings_id`),
  KEY `FK_tbl_earnings_1` (`employee_id`),
  CONSTRAINT `tbl_earnings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;