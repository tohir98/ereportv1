CREATE TABLE IF NOT EXISTS `tbl_level` (
  `level_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(45) DEFAULT NULL,
  `arrears` double DEFAULT NULL,
  `sundries` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `salary` double DEFAULT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;