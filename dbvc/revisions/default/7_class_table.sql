CREATE TABLE IF NOT EXISTS `tbl_class` (
  `class_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(150) DEFAULT NULL,
  `class_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;