CREATE TABLE IF NOT EXISTS `tbl_term` (
  `term_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(150) DEFAULT NULL,
  `term_desc` varchar(150) DEFAULT NULL,
  `sch_id` int(11) NOT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;