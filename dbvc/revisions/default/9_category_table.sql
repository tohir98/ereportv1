CREATE TABLE IF NOT EXISTS `tbl_category` (
  `category_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;