/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : ereport

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2015-09-21 21:18:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_account`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_account`;
CREATE TABLE `tbl_account` (
  `account_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `school_id` int(25) unsigned DEFAULT NULL,
  `school_name` varchar(150) DEFAULT NULL,
  `school_shortname` varchar(150) DEFAULT NULL,
  `school_address` varchar(350) DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_account
-- ----------------------------
INSERT INTO `tbl_account` VALUES ('1', '100', 'ADVENTIST HIGH SCHOOL', 'ads', null, 'demo', 'password', '2013-04-21 20:23:25');

-- ----------------------------
-- Table structure for `tbl_admission`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_admission`;
CREATE TABLE `tbl_admission` (
  `admission_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(150) DEFAULT NULL,
  `middlename` varchar(150) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `class_id` int(25) unsigned DEFAULT NULL,
  `dateofbirth` varchar(50) DEFAULT NULL,
  `gender_id` int(25) unsigned DEFAULT NULL,
  `bloodgroup` varchar(50) DEFAULT NULL,
  `birthplace` varchar(150) DEFAULT NULL,
  `nationality` varchar(150) DEFAULT NULL,
  `mothertongue` varchar(150) DEFAULT NULL,
  `disability` varchar(50) DEFAULT NULL,
  `religion_id` int(25) unsigned DEFAULT NULL,
  `country_id` int(25) DEFAULT NULL,
  `state_id` int(25) unsigned DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `address1` varchar(150) DEFAULT NULL,
  `address2` varchar(150) DEFAULT NULL,
  `pincode` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `photo` varchar(150) DEFAULT NULL,
  `admission_no` varchar(150) DEFAULT NULL,
  `amission_date` varchar(150) DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `sch_id` int(25) DEFAULT NULL,
  PRIMARY KEY (`admission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_admission
-- ----------------------------
INSERT INTO `tbl_admission` VALUES ('1', 'Ismail', 'OLAIDE', 'AFOLABI', '1', '2006-10-01', '1', null, 'LAGOS', '130', null, 'Physically Fit', '1', '0', '25', 'EBUTE METTA', 'JEBBA ROAD', 'HOUSE 31', null, '08165212457', '08165212457', 'info@aol.com', 'Mr folayemi.JPG', null, null, null, '100');
INSERT INTO `tbl_admission` VALUES ('2', 'OMOPARI', 'O', 'LANDER', '9', '2013-10-14', '2', '', 'ILESHA', '', null, 'Physically Fit', '2', '0', '25', 'IKOYI', '12 PLOT, OSBRNE ROAD', '', null, '(8654) - (679) - (8149)', '', 'me@you.iom', 'Mr Rasak.JPG', null, null, null, '100');
INSERT INTO `tbl_admission` VALUES ('3', 'Adebayo', 'S', 'Salami', '1', '1930-03-19', '1', null, 'Isolo', '130', null, 'Physically Fit', '1', '0', '25', 'Lagos', 'Suite 75-78, Terrace Wing, TBS Complex, Onika Lago', 'Suite 75-78, Terrace Wing, TBS Complex, Onika Lagos', null, '08023443744', '', '', 'ball.jpg', null, null, null, '100');
INSERT INTO `tbl_admission` VALUES ('4', 'Jamiu', 'Bolade', 'Oyewo', '1', '2013-12-23', '1', null, 'Isolo', '130', null, 'Physically Fit', '1', '0', '18', '', 'TBS Complex, Onika Lago', '', null, '08073076401', '', '', '', null, null, null, '100');
INSERT INTO `tbl_admission` VALUES ('5', 'Rukayat', 'A', 'Akindele', '1', '2014-12-23', '2', null, 'Lgos', '130', null, '', '1', '0', '25', 'Ikeja', '11, Fadeyi street, Aguda B/S, Ogba', '', null, '(0803) - (781) - (6549)', '', 'rr@ymail.com', 'eid-mubarak6.gif', null, null, null, '100');
INSERT INTO `tbl_admission` VALUES ('6', 'Simiat', 'F', 'Ilyas', '1', '2014-12-23', '2', '', 'Lgos', '130', null, '', '1', '0', '0', 'Ikeja', '11, Fadeyi street, Aguda B/S, Ogba', '', null, '(0803) - (781) - (6549)', '', 'rrom@ymail.com', 'coat of arm.jpg', null, null, null, '100');
INSERT INTO `tbl_admission` VALUES ('7', 'Harmony', '', 'Joel', '1', '2015-09-17', '1', null, 'Lagos', '130', null, 'Physically Fit', '2', '0', '25', 'Surulere', '10 Bola Shadipe Street, Off Adelabu, Surulere', '', null, '+2348037816587', '+2348037816587', 'otcleantech@gmail.com', 'ball.jpg', null, null, null, '100');

-- ----------------------------
-- Table structure for `tbl_calender`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_calender`;
CREATE TABLE `tbl_calender` (
  `calender_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(25) unsigned DEFAULT NULL,
  `term_id` int(25) unsigned DEFAULT NULL,
  `begin` varchar(45) DEFAULT NULL,
  `end` varchar(45) DEFAULT NULL,
  `midterm_start` varchar(45) DEFAULT NULL,
  `midterm_end` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`calender_id`),
  KEY `FK_tbl_calender_1` (`session_id`),
  KEY `FK_tbl_calender_2` (`term_id`),
  CONSTRAINT `FK_tbl_calender_1` FOREIGN KEY (`session_id`) REFERENCES `tbl_session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_calender_2` FOREIGN KEY (`term_id`) REFERENCES `tbl_term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_calender
-- ----------------------------
INSERT INTO `tbl_calender` VALUES ('1', '1', '1', '10/01/2013', '10/31/2013', '10/15/2013', '10/16/2013');
INSERT INTO `tbl_calender` VALUES ('2', '1', '1', '12/16/2014', '11/27/2014', '12/17/2014', '12/27/2014');
INSERT INTO `tbl_calender` VALUES ('3', '1', '2', '12/01/2014', '03/13/2015', '01/14/2015', '01/15/2015');

-- ----------------------------
-- Table structure for `tbl_card`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_card`;
CREATE TABLE `tbl_card` (
  `card_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `serial` varchar(150) DEFAULT NULL,
  `pin` varchar(150) DEFAULT NULL,
  `attempt` int(10) unsigned DEFAULT '0',
  `used` varchar(45) DEFAULT 'no',
  PRIMARY KEY (`card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_card
-- ----------------------------
INSERT INTO `tbl_card` VALUES ('1', '00000001', '595837723842', '4', 'no');
INSERT INTO `tbl_card` VALUES ('2', '00000001', '824375645894', '3', 'no');
INSERT INTO `tbl_card` VALUES ('3', '00000003', '898934582935', '1', 'no');
INSERT INTO `tbl_card` VALUES ('4', '00000004', '536627592279', '0', 'no');
INSERT INTO `tbl_card` VALUES ('5', '00000005', '633937835293', '0', 'no');
INSERT INTO `tbl_card` VALUES ('6', '00000006', '773564273243', '0', 'no');
INSERT INTO `tbl_card` VALUES ('7', '00000007', '287826427325', '0', 'no');
INSERT INTO `tbl_card` VALUES ('8', '00000008', '267983488258', '0', 'no');
INSERT INTO `tbl_card` VALUES ('9', '00000009', '327548355384', '0', 'no');
INSERT INTO `tbl_card` VALUES ('10', '00000010', '382878669232', '0', 'no');
INSERT INTO `tbl_card` VALUES ('11', '00000011', '953728735757', '0', 'no');
INSERT INTO `tbl_card` VALUES ('12', '00000012', '576567753356', '0', 'no');
INSERT INTO `tbl_card` VALUES ('13', '00000013', '593933649862', '0', 'no');
INSERT INTO `tbl_card` VALUES ('14', '00000014', '952326854834', '0', 'no');
INSERT INTO `tbl_card` VALUES ('15', '00000015', '357635656996', '0', 'no');
INSERT INTO `tbl_card` VALUES ('16', '00000016', '834578336299', '0', 'no');
INSERT INTO `tbl_card` VALUES ('17', '00000017', '599567646429', '0', 'no');
INSERT INTO `tbl_card` VALUES ('18', '00000018', '525745864343', '0', 'no');
INSERT INTO `tbl_card` VALUES ('19', '00000019', '844425276974', '0', 'no');
INSERT INTO `tbl_card` VALUES ('20', '00000020', '423335745946', '0', 'no');
INSERT INTO `tbl_card` VALUES ('21', '00000021', '565934699843', '0', 'no');
INSERT INTO `tbl_card` VALUES ('22', '00000022', '574373424724', '0', 'no');
INSERT INTO `tbl_card` VALUES ('23', '00000023', '468689226884', '0', 'no');
INSERT INTO `tbl_card` VALUES ('24', '00000024', '869263868499', '0', 'no');
INSERT INTO `tbl_card` VALUES ('25', '00000025', '958742494432', '0', 'no');
INSERT INTO `tbl_card` VALUES ('26', '00000026', '343834636257', '0', 'no');
INSERT INTO `tbl_card` VALUES ('27', '00000027', '462893658399', '0', 'no');
INSERT INTO `tbl_card` VALUES ('28', '00000028', '764398746972', '0', 'no');
INSERT INTO `tbl_card` VALUES ('29', '00000029', '969978257733', '0', 'no');
INSERT INTO `tbl_card` VALUES ('30', '00000030', '496853443756', '0', 'no');
INSERT INTO `tbl_card` VALUES ('31', '00000031', '858974896334', '0', 'no');
INSERT INTO `tbl_card` VALUES ('32', '00000032', '789764467746', '0', 'no');
INSERT INTO `tbl_card` VALUES ('33', '00000033', '982633954954', '0', 'no');
INSERT INTO `tbl_card` VALUES ('34', '00000034', '523276736459', '0', 'no');
INSERT INTO `tbl_card` VALUES ('35', '00000035', '644729825465', '0', 'no');
INSERT INTO `tbl_card` VALUES ('36', '00000036', '899326586299', '0', 'no');
INSERT INTO `tbl_card` VALUES ('37', '00000037', '733227885988', '0', 'no');
INSERT INTO `tbl_card` VALUES ('38', '00000038', '547273266686', '0', 'no');
INSERT INTO `tbl_card` VALUES ('39', '00000039', '772666984982', '0', 'no');
INSERT INTO `tbl_card` VALUES ('40', '00000040', '987335663264', '0', 'no');
INSERT INTO `tbl_card` VALUES ('41', '00000041', '469733629942', '0', 'no');
INSERT INTO `tbl_card` VALUES ('42', '00000042', '735478878758', '0', 'no');
INSERT INTO `tbl_card` VALUES ('43', '00000043', '335452492368', '0', 'no');
INSERT INTO `tbl_card` VALUES ('44', '00000044', '649948677732', '0', 'no');
INSERT INTO `tbl_card` VALUES ('45', '00000045', '269524795438', '0', 'no');
INSERT INTO `tbl_card` VALUES ('46', '00000046', '857588662385', '0', 'no');
INSERT INTO `tbl_card` VALUES ('47', '00000047', '829352893232', '0', 'no');
INSERT INTO `tbl_card` VALUES ('48', '00000048', '742298547877', '0', 'no');
INSERT INTO `tbl_card` VALUES ('49', '00000049', '947863678723', '0', 'no');
INSERT INTO `tbl_card` VALUES ('50', '00000050', '539686392784', '0', 'no');
INSERT INTO `tbl_card` VALUES ('51', '00000051', '679557256735', '0', 'no');
INSERT INTO `tbl_card` VALUES ('52', '00000052', '357357663573', '0', 'no');
INSERT INTO `tbl_card` VALUES ('53', '00000053', '579227435828', '0', 'no');
INSERT INTO `tbl_card` VALUES ('54', '00000054', '373467956738', '0', 'no');
INSERT INTO `tbl_card` VALUES ('55', '00000055', '983836334952', '0', 'no');
INSERT INTO `tbl_card` VALUES ('56', '00000056', '626253472545', '0', 'no');
INSERT INTO `tbl_card` VALUES ('57', '00000057', '337482583535', '0', 'no');
INSERT INTO `tbl_card` VALUES ('58', '00000058', '462496629848', '0', 'no');
INSERT INTO `tbl_card` VALUES ('59', '00000059', '584444679499', '0', 'no');
INSERT INTO `tbl_card` VALUES ('60', '00000060', '266676838232', '0', 'no');
INSERT INTO `tbl_card` VALUES ('61', '00000061', '225963622224', '0', 'no');
INSERT INTO `tbl_card` VALUES ('62', '00000062', '656524368564', '0', 'no');
INSERT INTO `tbl_card` VALUES ('63', '00000063', '245259539654', '0', 'no');
INSERT INTO `tbl_card` VALUES ('64', '00000064', '998642336985', '0', 'no');
INSERT INTO `tbl_card` VALUES ('65', '00000065', '765965729867', '0', 'no');
INSERT INTO `tbl_card` VALUES ('66', '00000066', '494292593892', '0', 'no');
INSERT INTO `tbl_card` VALUES ('67', '00000067', '474737247563', '0', 'no');
INSERT INTO `tbl_card` VALUES ('68', '00000068', '574925865976', '0', 'no');
INSERT INTO `tbl_card` VALUES ('69', '00000069', '898794247428', '0', 'no');
INSERT INTO `tbl_card` VALUES ('70', '00000070', '742825396763', '0', 'no');
INSERT INTO `tbl_card` VALUES ('71', '00000071', '843273539355', '0', 'no');
INSERT INTO `tbl_card` VALUES ('72', '00000072', '354233448489', '0', 'no');
INSERT INTO `tbl_card` VALUES ('73', '00000073', '942585637854', '0', 'no');
INSERT INTO `tbl_card` VALUES ('74', '00000074', '358458688986', '0', 'no');
INSERT INTO `tbl_card` VALUES ('75', '00000075', '272688833799', '0', 'no');
INSERT INTO `tbl_card` VALUES ('76', '00000076', '735726689627', '0', 'no');
INSERT INTO `tbl_card` VALUES ('77', '00000077', '285282298586', '0', 'no');
INSERT INTO `tbl_card` VALUES ('78', '00000078', '476582439396', '0', 'no');
INSERT INTO `tbl_card` VALUES ('79', '00000079', '322377975233', '0', 'no');
INSERT INTO `tbl_card` VALUES ('80', '00000080', '524674638643', '0', 'no');
INSERT INTO `tbl_card` VALUES ('81', '00000081', '688533879382', '0', 'no');
INSERT INTO `tbl_card` VALUES ('82', '00000082', '633826854545', '0', 'no');
INSERT INTO `tbl_card` VALUES ('83', '00000083', '944232592688', '0', 'no');
INSERT INTO `tbl_card` VALUES ('84', '00000084', '569528937826', '0', 'no');
INSERT INTO `tbl_card` VALUES ('85', '00000085', '283866225452', '0', 'no');
INSERT INTO `tbl_card` VALUES ('86', '00000086', '434863673375', '0', 'no');
INSERT INTO `tbl_card` VALUES ('87', '00000087', '968683937445', '0', 'no');
INSERT INTO `tbl_card` VALUES ('88', '00000088', '298978598823', '0', 'no');
INSERT INTO `tbl_card` VALUES ('89', '00000089', '699265477777', '0', 'no');
INSERT INTO `tbl_card` VALUES ('90', '00000090', '325347774976', '0', 'no');
INSERT INTO `tbl_card` VALUES ('91', '00000091', '789377928792', '0', 'no');
INSERT INTO `tbl_card` VALUES ('92', '00000092', '652964965229', '0', 'no');
INSERT INTO `tbl_card` VALUES ('93', '00000093', '923967392922', '0', 'no');
INSERT INTO `tbl_card` VALUES ('94', '00000094', '626897968857', '0', 'no');
INSERT INTO `tbl_card` VALUES ('95', '00000095', '942766942344', '0', 'no');
INSERT INTO `tbl_card` VALUES ('96', '00000096', '396354854927', '0', 'no');
INSERT INTO `tbl_card` VALUES ('97', '00000097', '657653279352', '0', 'no');
INSERT INTO `tbl_card` VALUES ('98', '00000098', '886284484874', '0', 'no');
INSERT INTO `tbl_card` VALUES ('99', '00000099', '426448665726', '0', 'no');
INSERT INTO `tbl_card` VALUES ('100', '00000100', '284777845945', '0', 'no');
INSERT INTO `tbl_card` VALUES ('101', '00000101', '444852793259', '0', 'no');
INSERT INTO `tbl_card` VALUES ('102', '00000102', '484796735263', '0', 'no');
INSERT INTO `tbl_card` VALUES ('103', '00000103', '323246792369', '0', 'no');
INSERT INTO `tbl_card` VALUES ('104', '00000104', '573738726487', '0', 'no');
INSERT INTO `tbl_card` VALUES ('105', '00000105', '943554372768', '0', 'no');
INSERT INTO `tbl_card` VALUES ('106', '00000106', '392496683567', '0', 'no');
INSERT INTO `tbl_card` VALUES ('107', '00000107', '888434394949', '0', 'no');
INSERT INTO `tbl_card` VALUES ('108', '00000108', '636735829732', '0', 'no');
INSERT INTO `tbl_card` VALUES ('109', '00000109', '252884638378', '0', 'no');
INSERT INTO `tbl_card` VALUES ('110', '00000110', '753359756656', '0', 'no');
INSERT INTO `tbl_card` VALUES ('111', '00000111', '259844989469', '0', 'no');
INSERT INTO `tbl_card` VALUES ('112', '00000112', '954574423289', '0', 'no');
INSERT INTO `tbl_card` VALUES ('113', '00000113', '265757335579', '0', 'no');
INSERT INTO `tbl_card` VALUES ('114', '00000114', '644498394324', '0', 'no');
INSERT INTO `tbl_card` VALUES ('115', '00000115', '779487729662', '0', 'no');
INSERT INTO `tbl_card` VALUES ('116', '00000116', '476744744464', '0', 'no');
INSERT INTO `tbl_card` VALUES ('117', '00000117', '524864957766', '0', 'no');
INSERT INTO `tbl_card` VALUES ('118', '00000118', '595679237486', '0', 'no');
INSERT INTO `tbl_card` VALUES ('119', '00000119', '522929697832', '0', 'no');
INSERT INTO `tbl_card` VALUES ('120', '00000120', '388728286587', '0', 'no');
INSERT INTO `tbl_card` VALUES ('121', '00000121', '697464832985', '0', 'no');
INSERT INTO `tbl_card` VALUES ('122', '00000122', '382652597753', '0', 'no');
INSERT INTO `tbl_card` VALUES ('123', '00000123', '874649644546', '0', 'no');
INSERT INTO `tbl_card` VALUES ('124', '00000124', '626869734938', '0', 'no');
INSERT INTO `tbl_card` VALUES ('125', '00000125', '489729962326', '0', 'no');
INSERT INTO `tbl_card` VALUES ('126', '00000126', '789588798835', '0', 'no');
INSERT INTO `tbl_card` VALUES ('127', '00000127', '422834348276', '0', 'no');
INSERT INTO `tbl_card` VALUES ('128', '00000128', '429556692262', '0', 'no');
INSERT INTO `tbl_card` VALUES ('129', '00000129', '884459635688', '0', 'no');
INSERT INTO `tbl_card` VALUES ('130', '00000130', '257567422865', '0', 'no');
INSERT INTO `tbl_card` VALUES ('131', '00000131', '245859298698', '0', 'no');
INSERT INTO `tbl_card` VALUES ('132', '00000132', '929296227936', '0', 'no');
INSERT INTO `tbl_card` VALUES ('133', '00000133', '783795444499', '0', 'no');
INSERT INTO `tbl_card` VALUES ('134', '00000134', '464384477958', '0', 'no');
INSERT INTO `tbl_card` VALUES ('135', '00000135', '646833666489', '0', 'no');
INSERT INTO `tbl_card` VALUES ('136', '00000136', '885685228897', '0', 'no');
INSERT INTO `tbl_card` VALUES ('137', '00000137', '847527792336', '0', 'no');
INSERT INTO `tbl_card` VALUES ('138', '00000138', '834658636369', '0', 'no');
INSERT INTO `tbl_card` VALUES ('139', '00000139', '878795544824', '0', 'no');
INSERT INTO `tbl_card` VALUES ('140', '00000140', '377247588293', '0', 'no');
INSERT INTO `tbl_card` VALUES ('141', '00000141', '682535644989', '0', 'no');
INSERT INTO `tbl_card` VALUES ('142', '00000142', '475659958288', '0', 'no');
INSERT INTO `tbl_card` VALUES ('143', '00000143', '446949399749', '0', 'no');
INSERT INTO `tbl_card` VALUES ('144', '00000144', '467625468588', '0', 'no');
INSERT INTO `tbl_card` VALUES ('145', '00000145', '966565649385', '0', 'no');
INSERT INTO `tbl_card` VALUES ('146', '00000146', '334588844958', '0', 'no');
INSERT INTO `tbl_card` VALUES ('147', '00000147', '295793564579', '0', 'no');
INSERT INTO `tbl_card` VALUES ('148', '00000148', '367838826823', '0', 'no');
INSERT INTO `tbl_card` VALUES ('149', '00000149', '964467765432', '0', 'no');
INSERT INTO `tbl_card` VALUES ('150', '00000150', '974537926974', '0', 'no');
INSERT INTO `tbl_card` VALUES ('151', '00000151', '426584757795', '0', 'no');
INSERT INTO `tbl_card` VALUES ('152', '00000152', '658287842276', '0', 'no');
INSERT INTO `tbl_card` VALUES ('153', '00000153', '788639279673', '0', 'no');
INSERT INTO `tbl_card` VALUES ('154', '00000154', '422465469485', '0', 'no');
INSERT INTO `tbl_card` VALUES ('155', '00000155', '733452943962', '0', 'no');
INSERT INTO `tbl_card` VALUES ('156', '00000156', '435393873383', '0', 'no');
INSERT INTO `tbl_card` VALUES ('157', '00000157', '639662992257', '0', 'no');
INSERT INTO `tbl_card` VALUES ('158', '00000158', '753243747669', '0', 'no');
INSERT INTO `tbl_card` VALUES ('159', '00000159', '447584888655', '0', 'no');
INSERT INTO `tbl_card` VALUES ('160', '00000160', '258629549763', '0', 'no');
INSERT INTO `tbl_card` VALUES ('161', '00000161', '422482459487', '0', 'no');
INSERT INTO `tbl_card` VALUES ('162', '00000162', '227438592936', '0', 'no');
INSERT INTO `tbl_card` VALUES ('163', '00000163', '862398533582', '0', 'no');
INSERT INTO `tbl_card` VALUES ('164', '00000164', '425958932557', '0', 'no');
INSERT INTO `tbl_card` VALUES ('165', '00000165', '683259967893', '0', 'no');
INSERT INTO `tbl_card` VALUES ('166', '00000166', '263898226985', '0', 'no');
INSERT INTO `tbl_card` VALUES ('167', '00000167', '845985922448', '0', 'no');
INSERT INTO `tbl_card` VALUES ('168', '00000168', '762877886282', '0', 'no');
INSERT INTO `tbl_card` VALUES ('169', '00000169', '968478889524', '0', 'no');
INSERT INTO `tbl_card` VALUES ('170', '00000170', '448645445336', '0', 'no');
INSERT INTO `tbl_card` VALUES ('171', '00000171', '486688877863', '0', 'no');
INSERT INTO `tbl_card` VALUES ('172', '00000172', '768975663562', '0', 'no');
INSERT INTO `tbl_card` VALUES ('173', '00000173', '446672844734', '0', 'no');
INSERT INTO `tbl_card` VALUES ('174', '00000174', '633477243497', '0', 'no');
INSERT INTO `tbl_card` VALUES ('175', '00000175', '794397265859', '0', 'no');
INSERT INTO `tbl_card` VALUES ('176', '00000176', '923788555829', '0', 'no');
INSERT INTO `tbl_card` VALUES ('177', '00000177', '455789897536', '0', 'no');
INSERT INTO `tbl_card` VALUES ('178', '00000178', '459827838955', '0', 'no');
INSERT INTO `tbl_card` VALUES ('179', '00000179', '557796536543', '0', 'no');
INSERT INTO `tbl_card` VALUES ('180', '00000180', '822854786599', '0', 'no');
INSERT INTO `tbl_card` VALUES ('181', '00000181', '828967264863', '0', 'no');
INSERT INTO `tbl_card` VALUES ('182', '00000182', '564495247886', '0', 'no');
INSERT INTO `tbl_card` VALUES ('183', '00000183', '664624392773', '0', 'no');
INSERT INTO `tbl_card` VALUES ('184', '00000184', '353738689672', '0', 'no');
INSERT INTO `tbl_card` VALUES ('185', '00000185', '369862356799', '0', 'no');
INSERT INTO `tbl_card` VALUES ('186', '00000186', '293262277497', '0', 'no');
INSERT INTO `tbl_card` VALUES ('187', '00000187', '258366949595', '0', 'no');
INSERT INTO `tbl_card` VALUES ('188', '00000188', '754372495964', '0', 'no');
INSERT INTO `tbl_card` VALUES ('189', '00000189', '639849853677', '0', 'no');
INSERT INTO `tbl_card` VALUES ('190', '00000190', '448848686366', '0', 'no');
INSERT INTO `tbl_card` VALUES ('191', '00000191', '258293376768', '0', 'no');
INSERT INTO `tbl_card` VALUES ('192', '00000192', '249232978523', '0', 'no');
INSERT INTO `tbl_card` VALUES ('193', '00000193', '226848975692', '0', 'no');
INSERT INTO `tbl_card` VALUES ('194', '00000194', '666967832242', '0', 'no');
INSERT INTO `tbl_card` VALUES ('195', '00000195', '929692848889', '0', 'no');
INSERT INTO `tbl_card` VALUES ('196', '00000196', '784392853893', '0', 'no');
INSERT INTO `tbl_card` VALUES ('197', '00000197', '273978637942', '0', 'no');
INSERT INTO `tbl_card` VALUES ('198', '00000198', '979459558962', '0', 'no');
INSERT INTO `tbl_card` VALUES ('199', '00000199', '784583577547', '0', 'no');
INSERT INTO `tbl_card` VALUES ('200', '00000200', '769658967883', '0', 'no');

-- ----------------------------
-- Table structure for `tbl_category`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE `tbl_category` (
  `category_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_category
-- ----------------------------
INSERT INTO `tbl_category` VALUES ('1', ' Teaching Staff');
INSERT INTO `tbl_category` VALUES ('2', ' Non Teaching');
INSERT INTO `tbl_category` VALUES ('3', ' Cleaners');

-- ----------------------------
-- Table structure for `tbl_class`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_class`;
CREATE TABLE `tbl_class` (
  `class_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(150) DEFAULT NULL,
  `class_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_class
-- ----------------------------
INSERT INTO `tbl_class` VALUES ('1', 'JSS 1A', 'Junior class 1A');
INSERT INTO `tbl_class` VALUES ('2', 'JSS 1B', 'Junior Class 1B');
INSERT INTO `tbl_class` VALUES ('3', 'JSS 1C', 'Junior class 1C');
INSERT INTO `tbl_class` VALUES ('4', 'JSS 2A', 'Junior Class 2A');
INSERT INTO `tbl_class` VALUES ('5', 'JSS 2B', 'Junior Class 2B');
INSERT INTO `tbl_class` VALUES ('6', 'JSS 2C', 'Junior Class 2C');
INSERT INTO `tbl_class` VALUES ('7', 'JSS 3A', 'Junior Class 3');
INSERT INTO `tbl_class` VALUES ('8', 'JSS 3B', 'Junior Class 3B');
INSERT INTO `tbl_class` VALUES ('9', 'JSS 3C', 'Junior Class 3C');

-- ----------------------------
-- Table structure for `tbl_country`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_country`;
CREATE TABLE `tbl_country` (
  `country_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_country
-- ----------------------------
INSERT INTO `tbl_country` VALUES ('1', 'Afghanistan');
INSERT INTO `tbl_country` VALUES ('2', 'Algeria');
INSERT INTO `tbl_country` VALUES ('3', 'Andorra');
INSERT INTO `tbl_country` VALUES ('4', 'Angola');
INSERT INTO `tbl_country` VALUES ('5', 'Antigua &amp; Deps');
INSERT INTO `tbl_country` VALUES ('6', 'Argentina');
INSERT INTO `tbl_country` VALUES ('7', 'Armenia');
INSERT INTO `tbl_country` VALUES ('8', 'Australia');
INSERT INTO `tbl_country` VALUES ('9', 'Austria');
INSERT INTO `tbl_country` VALUES ('10', 'Azerbaijan');
INSERT INTO `tbl_country` VALUES ('11', 'Bahamas');
INSERT INTO `tbl_country` VALUES ('12', 'Bahrain');
INSERT INTO `tbl_country` VALUES ('13', 'Bangladesh');
INSERT INTO `tbl_country` VALUES ('14', 'Barbados');
INSERT INTO `tbl_country` VALUES ('15', 'Belarus');
INSERT INTO `tbl_country` VALUES ('16', 'Belgium');
INSERT INTO `tbl_country` VALUES ('17', 'Belize');
INSERT INTO `tbl_country` VALUES ('18', 'Benin');
INSERT INTO `tbl_country` VALUES ('19', 'Bhutan');
INSERT INTO `tbl_country` VALUES ('20', 'Bolivia');
INSERT INTO `tbl_country` VALUES ('21', 'Bosnia Herzegovina');
INSERT INTO `tbl_country` VALUES ('22', 'Botswana');
INSERT INTO `tbl_country` VALUES ('23', 'Brazil');
INSERT INTO `tbl_country` VALUES ('24', 'Brunei');
INSERT INTO `tbl_country` VALUES ('25', 'Bulgaria');
INSERT INTO `tbl_country` VALUES ('26', 'Burkina');
INSERT INTO `tbl_country` VALUES ('27', 'Burundi');
INSERT INTO `tbl_country` VALUES ('28', 'Cambodia');
INSERT INTO `tbl_country` VALUES ('29', 'Cameroon');
INSERT INTO `tbl_country` VALUES ('30', 'Canada');
INSERT INTO `tbl_country` VALUES ('31', 'Cape Verde');
INSERT INTO `tbl_country` VALUES ('32', 'Central African Rep');
INSERT INTO `tbl_country` VALUES ('33', 'Chad');
INSERT INTO `tbl_country` VALUES ('34', 'Chile');
INSERT INTO `tbl_country` VALUES ('35', 'China');
INSERT INTO `tbl_country` VALUES ('36', 'Colombia');
INSERT INTO `tbl_country` VALUES ('37', 'Comoros');
INSERT INTO `tbl_country` VALUES ('38', 'Congo');
INSERT INTO `tbl_country` VALUES ('39', 'Congo {Democratic Rep}');
INSERT INTO `tbl_country` VALUES ('40', 'Costa Rica');
INSERT INTO `tbl_country` VALUES ('41', 'Croatia');
INSERT INTO `tbl_country` VALUES ('42', 'Cuba');
INSERT INTO `tbl_country` VALUES ('43', 'Cyprus');
INSERT INTO `tbl_country` VALUES ('44', 'Czech Republic');
INSERT INTO `tbl_country` VALUES ('45', 'Denmark');
INSERT INTO `tbl_country` VALUES ('46', 'Djibouti');
INSERT INTO `tbl_country` VALUES ('47', 'Dominica');
INSERT INTO `tbl_country` VALUES ('48', 'Dominican Republic');
INSERT INTO `tbl_country` VALUES ('49', 'East Timor');
INSERT INTO `tbl_country` VALUES ('50', 'Ecuador');
INSERT INTO `tbl_country` VALUES ('51', 'Egypt');
INSERT INTO `tbl_country` VALUES ('52', 'El Salvador');
INSERT INTO `tbl_country` VALUES ('53', 'Equatorial Guinea');
INSERT INTO `tbl_country` VALUES ('54', 'Eritrea');
INSERT INTO `tbl_country` VALUES ('55', 'Estonia');
INSERT INTO `tbl_country` VALUES ('56', 'Ethiopia');
INSERT INTO `tbl_country` VALUES ('57', 'Fiji');
INSERT INTO `tbl_country` VALUES ('58', 'Finland');
INSERT INTO `tbl_country` VALUES ('59', 'France');
INSERT INTO `tbl_country` VALUES ('60', 'Gabon');
INSERT INTO `tbl_country` VALUES ('61', 'Gambia');
INSERT INTO `tbl_country` VALUES ('62', 'Georgia');
INSERT INTO `tbl_country` VALUES ('63', 'Germany');
INSERT INTO `tbl_country` VALUES ('64', 'Ghana');
INSERT INTO `tbl_country` VALUES ('65', 'Greece');
INSERT INTO `tbl_country` VALUES ('66', 'Grenada');
INSERT INTO `tbl_country` VALUES ('67', 'Guatemala');
INSERT INTO `tbl_country` VALUES ('68', 'Guinea');
INSERT INTO `tbl_country` VALUES ('69', 'Guinea-Bissau');
INSERT INTO `tbl_country` VALUES ('70', 'Guyana');
INSERT INTO `tbl_country` VALUES ('71', 'Haiti');
INSERT INTO `tbl_country` VALUES ('72', 'Honduras');
INSERT INTO `tbl_country` VALUES ('73', 'Hungary');
INSERT INTO `tbl_country` VALUES ('74', 'Iceland');
INSERT INTO `tbl_country` VALUES ('75', 'India');
INSERT INTO `tbl_country` VALUES ('76', 'Indonesia');
INSERT INTO `tbl_country` VALUES ('77', 'Iran');
INSERT INTO `tbl_country` VALUES ('78', 'Iraq');
INSERT INTO `tbl_country` VALUES ('79', 'Ireland {Republic}');
INSERT INTO `tbl_country` VALUES ('80', 'Israel');
INSERT INTO `tbl_country` VALUES ('81', 'Italy');
INSERT INTO `tbl_country` VALUES ('82', 'Ivory Coast');
INSERT INTO `tbl_country` VALUES ('83', 'Jamaica');
INSERT INTO `tbl_country` VALUES ('84', 'Japan');
INSERT INTO `tbl_country` VALUES ('85', 'Jordan');
INSERT INTO `tbl_country` VALUES ('86', 'Kazakhstan');
INSERT INTO `tbl_country` VALUES ('87', 'Kenya');
INSERT INTO `tbl_country` VALUES ('88', 'Kiribati');
INSERT INTO `tbl_country` VALUES ('89', 'Korea North');
INSERT INTO `tbl_country` VALUES ('90', 'Korea South');
INSERT INTO `tbl_country` VALUES ('91', 'Kosovo');
INSERT INTO `tbl_country` VALUES ('92', 'Kuwait');
INSERT INTO `tbl_country` VALUES ('93', 'Kyrgyzstan');
INSERT INTO `tbl_country` VALUES ('94', 'Laos');
INSERT INTO `tbl_country` VALUES ('95', 'Latvia');
INSERT INTO `tbl_country` VALUES ('96', 'Lebanon');
INSERT INTO `tbl_country` VALUES ('97', 'Lesotho');
INSERT INTO `tbl_country` VALUES ('98', 'Liberia');
INSERT INTO `tbl_country` VALUES ('99', 'Libya');
INSERT INTO `tbl_country` VALUES ('100', 'Liechtenstein');
INSERT INTO `tbl_country` VALUES ('101', 'Lithuania');
INSERT INTO `tbl_country` VALUES ('102', 'Luxembourg');
INSERT INTO `tbl_country` VALUES ('103', 'Macedonia');
INSERT INTO `tbl_country` VALUES ('104', 'Madagascar');
INSERT INTO `tbl_country` VALUES ('105', 'Malawi');
INSERT INTO `tbl_country` VALUES ('106', 'Malaysia');
INSERT INTO `tbl_country` VALUES ('107', 'Maldives');
INSERT INTO `tbl_country` VALUES ('108', 'Mali');
INSERT INTO `tbl_country` VALUES ('109', 'Malta');
INSERT INTO `tbl_country` VALUES ('110', 'Marshall Islands');
INSERT INTO `tbl_country` VALUES ('111', 'Mauritania');
INSERT INTO `tbl_country` VALUES ('112', 'Mauritius');
INSERT INTO `tbl_country` VALUES ('113', 'Mexico');
INSERT INTO `tbl_country` VALUES ('114', 'Micronesia');
INSERT INTO `tbl_country` VALUES ('115', 'Moldova');
INSERT INTO `tbl_country` VALUES ('116', 'Monaco');
INSERT INTO `tbl_country` VALUES ('117', 'Mongolia');
INSERT INTO `tbl_country` VALUES ('118', 'Montenegro');
INSERT INTO `tbl_country` VALUES ('119', 'Morocco');
INSERT INTO `tbl_country` VALUES ('120', 'Mozambique');
INSERT INTO `tbl_country` VALUES ('121', 'Myanmar, {Burma}');
INSERT INTO `tbl_country` VALUES ('122', 'Namibia');
INSERT INTO `tbl_country` VALUES ('123', 'Nauru');
INSERT INTO `tbl_country` VALUES ('124', 'Nepal');
INSERT INTO `tbl_country` VALUES ('125', 'Netherlands');
INSERT INTO `tbl_country` VALUES ('126', 'New Zealand');
INSERT INTO `tbl_country` VALUES ('127', 'Nicaragua');
INSERT INTO `tbl_country` VALUES ('128', 'Niger');
INSERT INTO `tbl_country` VALUES ('129', 'Nigeria');
INSERT INTO `tbl_country` VALUES ('130', 'Norway');
INSERT INTO `tbl_country` VALUES ('131', 'Oman');
INSERT INTO `tbl_country` VALUES ('132', 'Pakistan');
INSERT INTO `tbl_country` VALUES ('133', 'Palau');
INSERT INTO `tbl_country` VALUES ('134', 'Palestine');
INSERT INTO `tbl_country` VALUES ('135', 'Panama');
INSERT INTO `tbl_country` VALUES ('136', 'Papua New Guinea');
INSERT INTO `tbl_country` VALUES ('137', 'Paraguay');
INSERT INTO `tbl_country` VALUES ('138', 'Peru');
INSERT INTO `tbl_country` VALUES ('139', 'Philippines');
INSERT INTO `tbl_country` VALUES ('140', 'Poland');
INSERT INTO `tbl_country` VALUES ('141', 'Portugal');
INSERT INTO `tbl_country` VALUES ('142', 'Qatar');
INSERT INTO `tbl_country` VALUES ('143', 'Romania');
INSERT INTO `tbl_country` VALUES ('144', 'Russian Federation');
INSERT INTO `tbl_country` VALUES ('145', 'Rwanda');
INSERT INTO `tbl_country` VALUES ('146', 'Saint Vincent &amp; the Grenadines');
INSERT INTO `tbl_country` VALUES ('147', 'Samoa');
INSERT INTO `tbl_country` VALUES ('148', 'San Marino');
INSERT INTO `tbl_country` VALUES ('149', 'Sao Tome &amp; Principe');
INSERT INTO `tbl_country` VALUES ('150', 'Saudi Arabia');
INSERT INTO `tbl_country` VALUES ('151', 'Senegal');
INSERT INTO `tbl_country` VALUES ('152', 'Serbia');
INSERT INTO `tbl_country` VALUES ('153', 'Seychelles');
INSERT INTO `tbl_country` VALUES ('154', 'Sierra Leone');
INSERT INTO `tbl_country` VALUES ('155', 'Singapore');
INSERT INTO `tbl_country` VALUES ('156', 'Slovakia');
INSERT INTO `tbl_country` VALUES ('157', 'Slovenia');
INSERT INTO `tbl_country` VALUES ('158', 'Solomon Islands');
INSERT INTO `tbl_country` VALUES ('159', 'Somalia');
INSERT INTO `tbl_country` VALUES ('160', 'South Africa');
INSERT INTO `tbl_country` VALUES ('161', 'Spain');
INSERT INTO `tbl_country` VALUES ('162', 'Sri Lanka');
INSERT INTO `tbl_country` VALUES ('163', 'St Kitts &amp; Nevis');
INSERT INTO `tbl_country` VALUES ('164', 'St Lucia');
INSERT INTO `tbl_country` VALUES ('165', 'Sudan');
INSERT INTO `tbl_country` VALUES ('166', 'Suriname');
INSERT INTO `tbl_country` VALUES ('167', 'Swaziland');
INSERT INTO `tbl_country` VALUES ('168', 'Sweden');
INSERT INTO `tbl_country` VALUES ('169', 'Switzerland');
INSERT INTO `tbl_country` VALUES ('170', 'Syria');
INSERT INTO `tbl_country` VALUES ('171', 'Taiwan');
INSERT INTO `tbl_country` VALUES ('172', 'Tajikistan');
INSERT INTO `tbl_country` VALUES ('173', 'Tanzania');
INSERT INTO `tbl_country` VALUES ('174', 'Thailand');
INSERT INTO `tbl_country` VALUES ('175', 'Togo');
INSERT INTO `tbl_country` VALUES ('176', 'Tonga');
INSERT INTO `tbl_country` VALUES ('177', 'Trinidad &amp; Tobago');
INSERT INTO `tbl_country` VALUES ('178', 'Tunisia');
INSERT INTO `tbl_country` VALUES ('179', 'Turkey');
INSERT INTO `tbl_country` VALUES ('180', 'Turkmenistan');
INSERT INTO `tbl_country` VALUES ('181', 'Tuvalu');
INSERT INTO `tbl_country` VALUES ('182', 'Uganda');
INSERT INTO `tbl_country` VALUES ('183', 'Ukraine');
INSERT INTO `tbl_country` VALUES ('184', 'United Arab Emirates');
INSERT INTO `tbl_country` VALUES ('185', 'United Kingdom');
INSERT INTO `tbl_country` VALUES ('186', 'United States');
INSERT INTO `tbl_country` VALUES ('187', 'Uruguay');
INSERT INTO `tbl_country` VALUES ('188', 'Uzbekistan');
INSERT INTO `tbl_country` VALUES ('189', 'Vanuatu');
INSERT INTO `tbl_country` VALUES ('190', 'Vatican City');
INSERT INTO `tbl_country` VALUES ('191', 'Venezuea');
INSERT INTO `tbl_country` VALUES ('192', 'Vietnam');
INSERT INTO `tbl_country` VALUES ('193', 'Yemen');
INSERT INTO `tbl_country` VALUES ('194', 'Zambia');
INSERT INTO `tbl_country` VALUES ('195', 'Zimbabwe');

-- ----------------------------
-- Table structure for `tbl_country1`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_country1`;
CREATE TABLE `tbl_country1` (
  `country_id` int(25) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_country1
-- ----------------------------
INSERT INTO `tbl_country1` VALUES ('1', 'Nigeria');
INSERT INTO `tbl_country1` VALUES ('2', 'Others');

-- ----------------------------
-- Table structure for `tbl_deductions`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_deductions`;
CREATE TABLE `tbl_deductions` (
  `deductions_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(25) unsigned DEFAULT NULL,
  `loan_repay` double DEFAULT NULL,
  `welfare` double DEFAULT NULL,
  `coop` double DEFAULT NULL,
  `guarantee` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `month` varchar(45) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`deductions_id`),
  KEY `FK_tbl_deductions_1` (`employee_id`),
  CONSTRAINT `tbl_deductions_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_deductions
-- ----------------------------
INSERT INTO `tbl_deductions` VALUES ('1', '1', '250', '150', '0', '0', '1450', 'Apr', '2014');

-- ----------------------------
-- Table structure for `tbl_earnings`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_earnings`;
CREATE TABLE `tbl_earnings` (
  `earnings_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(25) unsigned DEFAULT NULL,
  `housing` double DEFAULT NULL,
  `transport` double DEFAULT NULL,
  `utility` double DEFAULT NULL,
  `loan` double DEFAULT NULL,
  `advances` double DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `basic` double DEFAULT NULL,
  PRIMARY KEY (`earnings_id`),
  KEY `FK_tbl_earnings_1` (`employee_id`),
  CONSTRAINT `tbl_earnings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_earnings
-- ----------------------------
INSERT INTO `tbl_earnings` VALUES ('1', '1', '58000', '43500', '14500', '4500', '244', 'Apr', '2014', '29000');

-- ----------------------------
-- Table structure for `tbl_employee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_employee`;
CREATE TABLE `tbl_employee` (
  `employee_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `title_id` int(25) unsigned DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `othernames` varchar(45) DEFAULT NULL,
  `level_id` int(25) unsigned DEFAULT NULL,
  `gender_id` int(25) unsigned DEFAULT NULL,
  `marital_status_id` int(25) unsigned DEFAULT NULL,
  `dateof_birth` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `postal_address` varchar(150) DEFAULT NULL,
  `home_address` varchar(150) DEFAULT NULL,
  `g_name` varchar(150) DEFAULT NULL,
  `g_occupation` varchar(150) DEFAULT NULL,
  `g_address` varchar(500) DEFAULT NULL,
  `g_phone` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `country_id` int(25) unsigned DEFAULT NULL,
  `regional_state_id` int(25) unsigned DEFAULT NULL,
  `local_govt_id` int(25) unsigned DEFAULT NULL,
  `home_town` varchar(150) DEFAULT NULL,
  `religion_id` int(25) unsigned DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `FK_tbl_employee_1` (`level_id`),
  KEY `FK_tbl_employee_genda` (`gender_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_employee
-- ----------------------------
INSERT INTO `tbl_employee` VALUES ('1', '3', 'HASSAN', 'KUTIGI', '1', '2', '3', '04/08/2014', '08073076401', '', '', 'KUTG', '', '', '08058575', '', '0', '0', '0', '', '0');
INSERT INTO `tbl_employee` VALUES ('2', '1', 'ADENIYI', 'O.E', '1', '1', '1', '04/08/2014', '08808080808', 'P.O.BOX 1107', 'OSOGBO', 'LWAL', 'TEACHING', 'OSOGBO', '0808080808', '', '1', '0', '614', 'OSOGBO', '2');

-- ----------------------------
-- Table structure for `tbl_fee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_fee`;
CREATE TABLE `tbl_fee` (
  `fee_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `admission_form` double DEFAULT NULL,
  `tuition_fee` double DEFAULT NULL,
  `textbook` double DEFAULT NULL,
  `uniform` double DEFAULT NULL,
  `jur_waec` double DEFAULT NULL,
  `games` double DEFAULT NULL,
  `development` double DEFAULT NULL,
  `library` double DEFAULT NULL,
  `exam` double DEFAULT NULL,
  `practical` double DEFAULT NULL,
  `pta` double DEFAULT NULL,
  `project` double DEFAULT NULL,
  `transport` double DEFAULT NULL,
  `excursion` double DEFAULT NULL,
  `lesson` double DEFAULT NULL,
  `sport_wear` double DEFAULT NULL,
  `computer` double DEFAULT NULL,
  `arrears` double DEFAULT NULL,
  `meal` double DEFAULT NULL,
  `graduation` double DEFAULT NULL,
  `term_id` int(25) unsigned DEFAULT NULL,
  `session_id` int(25) unsigned DEFAULT NULL,
  `class_id` int(25) unsigned DEFAULT NULL,
  PRIMARY KEY (`fee_id`),
  KEY `FK_tbl_fee_2` (`term_id`),
  KEY `FK_tbl_fee_3` (`session_id`),
  KEY `FK_tbl_fee_4` (`class_id`),
  CONSTRAINT `FK_tbl_fee_2` FOREIGN KEY (`term_id`) REFERENCES `tbl_term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_fee_3` FOREIGN KEY (`session_id`) REFERENCES `tbl_session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_fee_4` FOREIGN KEY (`class_id`) REFERENCES `tbl_class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_fee
-- ----------------------------
INSERT INTO `tbl_fee` VALUES ('1', '1500', '4500', '5000', '3500', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1500', null, '0', '0', '1', '1', '1');
INSERT INTO `tbl_fee` VALUES ('2', '0', '5000', '0', '0', '0', '0', '0', '0', '0', '0', '1000', '0', '0', '0', '0', '2000', '0', null, '0', '0', '2', '1', '2');
INSERT INTO `tbl_fee` VALUES ('3', '500', '25000', '200', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '1', '1', '7');
INSERT INTO `tbl_fee` VALUES ('4', '500', '25000', '200', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '1', '1', '8');
INSERT INTO `tbl_fee` VALUES ('5', '500', '25000', '200', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '1', '1', '9');
INSERT INTO `tbl_fee` VALUES ('6', '0', '14500', '200', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '2', '1', '1');
INSERT INTO `tbl_fee` VALUES ('7', '434', '4343', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '3', '1', '1');
INSERT INTO `tbl_fee` VALUES ('8', '452', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '3', '1', '2');
INSERT INTO `tbl_fee` VALUES ('9', '1000', '4850', '2000', '500', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '1', '2', '1');

-- ----------------------------
-- Table structure for `tbl_gender`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_gender`;
CREATE TABLE `tbl_gender` (
  `gender_id` int(25) NOT NULL AUTO_INCREMENT,
  `gender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_gender
-- ----------------------------
INSERT INTO `tbl_gender` VALUES ('1', 'male');
INSERT INTO `tbl_gender` VALUES ('2', 'female');

-- ----------------------------
-- Table structure for `tbl_level`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_level`;
CREATE TABLE `tbl_level` (
  `level_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(45) DEFAULT NULL,
  `arrears` double DEFAULT NULL,
  `sundries` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `salary` double DEFAULT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_level
-- ----------------------------
INSERT INTO `tbl_level` VALUES ('1', 'Level 7', '4500', '1500', '2500', '29000');

-- ----------------------------
-- Table structure for `tbl_lga`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_lga`;
CREATE TABLE `tbl_lga` (
  `ID` int(11) NOT NULL,
  `STATECODE` varchar(4) NOT NULL,
  `LG` varchar(80) NOT NULL,
  UNIQUE KEY `ID_2` (`ID`),
  KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_lga
-- ----------------------------
INSERT INTO `tbl_lga` VALUES ('189', 'CR', 'Etung');
INSERT INTO `tbl_lga` VALUES ('190', 'CR', 'Bekwara');
INSERT INTO `tbl_lga` VALUES ('191', 'CR', 'Bakassi');
INSERT INTO `tbl_lga` VALUES ('192', 'CR', 'Calabar Municipality');
INSERT INTO `tbl_lga` VALUES ('193', 'DT', 'Oshimili');
INSERT INTO `tbl_lga` VALUES ('194', 'DT', 'Aniocha');
INSERT INTO `tbl_lga` VALUES ('195', 'DT', 'Aniocha South');
INSERT INTO `tbl_lga` VALUES ('196', 'DT', 'Ika South');
INSERT INTO `tbl_lga` VALUES ('197', 'DT', 'Ika North-East');
INSERT INTO `tbl_lga` VALUES ('198', 'DT', 'Ndokwa West');
INSERT INTO `tbl_lga` VALUES ('199', 'DT', 'Ndokwa East');
INSERT INTO `tbl_lga` VALUES ('200', 'DT', 'Isoko south');
INSERT INTO `tbl_lga` VALUES ('201', 'DT', 'Isoko North');
INSERT INTO `tbl_lga` VALUES ('202', 'DT', 'Bomadi');
INSERT INTO `tbl_lga` VALUES ('203', 'DT', 'Burutu');
INSERT INTO `tbl_lga` VALUES ('204', 'DT', 'Ughelli South');
INSERT INTO `tbl_lga` VALUES ('205', 'DT', 'Ughelli North');
INSERT INTO `tbl_lga` VALUES ('206', 'DT', 'Ethiope West');
INSERT INTO `tbl_lga` VALUES ('207', 'DT', 'Ethiope East');
INSERT INTO `tbl_lga` VALUES ('208', 'DT', 'Sapele');
INSERT INTO `tbl_lga` VALUES ('209', 'DT', 'Okpe');
INSERT INTO `tbl_lga` VALUES ('210', 'DT', 'Warri North');
INSERT INTO `tbl_lga` VALUES ('211', 'DT', 'Warri South');
INSERT INTO `tbl_lga` VALUES ('212', 'DT', 'Uvwie');
INSERT INTO `tbl_lga` VALUES ('213', 'DT', 'Udu');
INSERT INTO `tbl_lga` VALUES ('214', 'DT', 'Warri Central');
INSERT INTO `tbl_lga` VALUES ('215', 'DT', 'Ukwani');
INSERT INTO `tbl_lga` VALUES ('216', 'DT', 'Oshimili North');
INSERT INTO `tbl_lga` VALUES ('217', 'DT', 'Patani');
INSERT INTO `tbl_lga` VALUES ('218', 'EB', 'Afikpo North');
INSERT INTO `tbl_lga` VALUES ('219', 'EB', 'Afikpo South');
INSERT INTO `tbl_lga` VALUES ('220', 'EB', 'Onicha');
INSERT INTO `tbl_lga` VALUES ('221', 'EB', 'Ohaozara');
INSERT INTO `tbl_lga` VALUES ('222', 'EB', 'Abakaliki');
INSERT INTO `tbl_lga` VALUES ('223', 'EB', 'Ishielu');
INSERT INTO `tbl_lga` VALUES ('224', 'EB', 'lkwo');
INSERT INTO `tbl_lga` VALUES ('225', 'EB', 'Ezza');
INSERT INTO `tbl_lga` VALUES ('226', 'EB', 'Ezza South');
INSERT INTO `tbl_lga` VALUES ('227', 'EB', 'Ohaukwu');
INSERT INTO `tbl_lga` VALUES ('228', 'EB', 'Ebonyi');
INSERT INTO `tbl_lga` VALUES ('229', 'EB', 'Ivo ');
INSERT INTO `tbl_lga` VALUES ('230', 'ED', 'Esan North-East');
INSERT INTO `tbl_lga` VALUES ('231', 'ED', 'Esan Central');
INSERT INTO `tbl_lga` VALUES ('232', 'ED', 'Esan West');
INSERT INTO `tbl_lga` VALUES ('233', 'ED', 'Egor');
INSERT INTO `tbl_lga` VALUES ('234', 'ED', 'Ukpoba');
INSERT INTO `tbl_lga` VALUES ('235', 'ED', 'Central');
INSERT INTO `tbl_lga` VALUES ('236', 'ED', 'Etsako Central');
INSERT INTO `tbl_lga` VALUES ('237', 'ED', 'Igueben');
INSERT INTO `tbl_lga` VALUES ('238', 'ED', 'Oredo');
INSERT INTO `tbl_lga` VALUES ('239', 'ED', 'Ovia SouthWest');
INSERT INTO `tbl_lga` VALUES ('240', 'ED', 'Ovia South-East');
INSERT INTO `tbl_lga` VALUES ('241', 'ED', 'Orhionwon');
INSERT INTO `tbl_lga` VALUES ('242', 'ED', 'Uhunmwonde');
INSERT INTO `tbl_lga` VALUES ('243', 'ED', 'Etsako East ');
INSERT INTO `tbl_lga` VALUES ('244', 'ED', 'Esan South-East ');
INSERT INTO `tbl_lga` VALUES ('245', 'EK', 'Ado');
INSERT INTO `tbl_lga` VALUES ('246', 'EK', 'Ekiti-East');
INSERT INTO `tbl_lga` VALUES ('247', 'EK', 'Ekiti-West ');
INSERT INTO `tbl_lga` VALUES ('248', 'EK', 'Emure/Ise/Orun');
INSERT INTO `tbl_lga` VALUES ('249', 'EK', 'Ekiti South-West');
INSERT INTO `tbl_lga` VALUES ('250', 'EK', 'Ikare');
INSERT INTO `tbl_lga` VALUES ('251', 'EK', 'Irepodun');
INSERT INTO `tbl_lga` VALUES ('252', 'EK', 'Ijero, ');
INSERT INTO `tbl_lga` VALUES ('253', 'EK', 'Ido/Osi');
INSERT INTO `tbl_lga` VALUES ('254', 'EK', 'Oye');
INSERT INTO `tbl_lga` VALUES ('255', 'EK', 'Ikole');
INSERT INTO `tbl_lga` VALUES ('256', 'EK', 'Moba');
INSERT INTO `tbl_lga` VALUES ('257', 'EK', 'Gbonyin');
INSERT INTO `tbl_lga` VALUES ('258', 'EK', 'Efon');
INSERT INTO `tbl_lga` VALUES ('259', 'EK', 'Ise/Orun ');
INSERT INTO `tbl_lga` VALUES ('260', 'EK', 'Ilejemeje.');
INSERT INTO `tbl_lga` VALUES ('261', 'EN', 'Enugu South');
INSERT INTO `tbl_lga` VALUES ('262', 'EN', 'Igbo-Eze South');
INSERT INTO `tbl_lga` VALUES ('263', 'EN', 'Enugu North');
INSERT INTO `tbl_lga` VALUES ('264', 'EN', 'Nkanu');
INSERT INTO `tbl_lga` VALUES ('265', 'EN', 'Udi Agwu');
INSERT INTO `tbl_lga` VALUES ('266', 'EN', 'Oji-River');
INSERT INTO `tbl_lga` VALUES ('267', 'EN', 'Ezeagu');
INSERT INTO `tbl_lga` VALUES ('268', 'EN', 'IgboEze North');
INSERT INTO `tbl_lga` VALUES ('269', 'EN', 'Isi-Uzo');
INSERT INTO `tbl_lga` VALUES ('270', 'EN', 'Nsukka');
INSERT INTO `tbl_lga` VALUES ('271', 'EN', 'Igbo-Ekiti');
INSERT INTO `tbl_lga` VALUES ('272', 'EN', 'Uzo-Uwani');
INSERT INTO `tbl_lga` VALUES ('273', 'EN', 'Enugu Eas');
INSERT INTO `tbl_lga` VALUES ('274', 'EN', 'Aninri');
INSERT INTO `tbl_lga` VALUES ('275', 'EN', 'Nkanu East');
INSERT INTO `tbl_lga` VALUES ('276', 'EN', 'Udenu');
INSERT INTO `tbl_lga` VALUES ('277', 'GM', 'Akko');
INSERT INTO `tbl_lga` VALUES ('278', 'GM', 'Balanga');
INSERT INTO `tbl_lga` VALUES ('279', 'GM', 'Billiri');
INSERT INTO `tbl_lga` VALUES ('280', 'GM', 'Dukku');
INSERT INTO `tbl_lga` VALUES ('281', 'GM', 'Kaltungo');
INSERT INTO `tbl_lga` VALUES ('282', 'GM', 'Kwami');
INSERT INTO `tbl_lga` VALUES ('283', 'GM', 'Shomgom');
INSERT INTO `tbl_lga` VALUES ('284', 'GM', 'Funakaye');
INSERT INTO `tbl_lga` VALUES ('285', 'GM', 'Gombe');
INSERT INTO `tbl_lga` VALUES ('286', 'GM', 'Nafada/Bajoga ');
INSERT INTO `tbl_lga` VALUES ('287', 'GM', 'Yamaltu/Delta');
INSERT INTO `tbl_lga` VALUES ('288', 'IM', 'Aboh-Mbaise');
INSERT INTO `tbl_lga` VALUES ('289', 'IM', 'Ahiazu-Mbaise');
INSERT INTO `tbl_lga` VALUES ('290', 'IM', 'Ehime-Mbano');
INSERT INTO `tbl_lga` VALUES ('291', 'IM', 'Ezinihitte');
INSERT INTO `tbl_lga` VALUES ('292', 'IM', 'Ideato North');
INSERT INTO `tbl_lga` VALUES ('293', 'IM', 'Ideato South');
INSERT INTO `tbl_lga` VALUES ('294', 'IM', 'Ihitte/Uboma');
INSERT INTO `tbl_lga` VALUES ('295', 'IM', 'Ikeduru');
INSERT INTO `tbl_lga` VALUES ('296', 'IM', 'Isiala Mbano');
INSERT INTO `tbl_lga` VALUES ('297', 'IM', 'Isu');
INSERT INTO `tbl_lga` VALUES ('298', 'IM', 'Mbaitoli');
INSERT INTO `tbl_lga` VALUES ('299', 'IM', 'Mbaitoli');
INSERT INTO `tbl_lga` VALUES ('300', 'IM', 'Ngor-Okpala');
INSERT INTO `tbl_lga` VALUES ('301', 'IM', 'Njaba');
INSERT INTO `tbl_lga` VALUES ('302', 'IM', 'Nwangele');
INSERT INTO `tbl_lga` VALUES ('303', 'IM', 'Nkwerre');
INSERT INTO `tbl_lga` VALUES ('304', 'IM', 'Obowo');
INSERT INTO `tbl_lga` VALUES ('305', 'IM', 'Oguta');
INSERT INTO `tbl_lga` VALUES ('306', 'IM', 'Ohaji/Egbema');
INSERT INTO `tbl_lga` VALUES ('307', 'IM', 'Okigwe');
INSERT INTO `tbl_lga` VALUES ('308', 'IM', 'Orlu');
INSERT INTO `tbl_lga` VALUES ('309', 'IM', 'Orsu');
INSERT INTO `tbl_lga` VALUES ('310', 'IM', 'Oru East');
INSERT INTO `tbl_lga` VALUES ('311', 'IM', 'Oru West');
INSERT INTO `tbl_lga` VALUES ('312', 'IM', 'Owerri-Municipal');
INSERT INTO `tbl_lga` VALUES ('313', 'IM', 'Owerri North');
INSERT INTO `tbl_lga` VALUES ('314', 'IM', 'Owerri West ');
INSERT INTO `tbl_lga` VALUES ('315', 'JG', 'Auyo');
INSERT INTO `tbl_lga` VALUES ('316', 'JG', 'Babura');
INSERT INTO `tbl_lga` VALUES ('317', 'JG', 'Birni Kudu');
INSERT INTO `tbl_lga` VALUES ('318', 'JG', 'Biriniwa');
INSERT INTO `tbl_lga` VALUES ('319', 'JG', 'Buji');
INSERT INTO `tbl_lga` VALUES ('320', 'JG', 'Dutse');
INSERT INTO `tbl_lga` VALUES ('321', 'JG', 'Gagarawa');
INSERT INTO `tbl_lga` VALUES ('322', 'JG', 'Garki');
INSERT INTO `tbl_lga` VALUES ('323', 'JG', 'Gumel');
INSERT INTO `tbl_lga` VALUES ('324', 'JG', 'Guri');
INSERT INTO `tbl_lga` VALUES ('325', 'JG', 'Gwaram');
INSERT INTO `tbl_lga` VALUES ('326', 'JG', 'Gwiwa');
INSERT INTO `tbl_lga` VALUES ('327', 'JG', 'Hadejia');
INSERT INTO `tbl_lga` VALUES ('328', 'JG', 'Jahun');
INSERT INTO `tbl_lga` VALUES ('329', 'JG', 'Kafin Hausa');
INSERT INTO `tbl_lga` VALUES ('330', 'JG', 'Kaugama Kazaure');
INSERT INTO `tbl_lga` VALUES ('331', 'JG', 'Kiri Kasamma');
INSERT INTO `tbl_lga` VALUES ('332', 'JG', 'Kiyawa');
INSERT INTO `tbl_lga` VALUES ('333', 'JG', 'Maigatari');
INSERT INTO `tbl_lga` VALUES ('334', 'JG', 'Malam Madori');
INSERT INTO `tbl_lga` VALUES ('335', 'JG', 'Miga');
INSERT INTO `tbl_lga` VALUES ('336', 'JG', 'Ringim');
INSERT INTO `tbl_lga` VALUES ('337', 'JG', 'Roni');
INSERT INTO `tbl_lga` VALUES ('338', 'JG', 'Sule-Tankarkar');
INSERT INTO `tbl_lga` VALUES ('339', 'JG', 'Taura ');
INSERT INTO `tbl_lga` VALUES ('340', 'JG', 'Yankwashi ');
INSERT INTO `tbl_lga` VALUES ('341', 'KD', 'Birni-Gwari');
INSERT INTO `tbl_lga` VALUES ('342', 'KD', 'Chikun');
INSERT INTO `tbl_lga` VALUES ('343', 'KD', 'Giwa');
INSERT INTO `tbl_lga` VALUES ('344', 'KD', 'Igabi');
INSERT INTO `tbl_lga` VALUES ('345', 'KD', 'Ikara');
INSERT INTO `tbl_lga` VALUES ('346', 'KD', 'jaba');
INSERT INTO `tbl_lga` VALUES ('347', 'KD', 'Jema\'a');
INSERT INTO `tbl_lga` VALUES ('348', 'KD', 'Kachia');
INSERT INTO `tbl_lga` VALUES ('349', 'KD', 'Kaduna North');
INSERT INTO `tbl_lga` VALUES ('350', 'KD', 'Kaduna South');
INSERT INTO `tbl_lga` VALUES ('351', 'KD', 'Kagarko');
INSERT INTO `tbl_lga` VALUES ('352', 'KD', 'Kajuru');
INSERT INTO `tbl_lga` VALUES ('353', 'KD', 'Kaura');
INSERT INTO `tbl_lga` VALUES ('354', 'KD', 'Kauru');
INSERT INTO `tbl_lga` VALUES ('355', 'KD', 'Kubau');
INSERT INTO `tbl_lga` VALUES ('356', 'KD', 'Kudan');
INSERT INTO `tbl_lga` VALUES ('357', 'KD', 'Lere');
INSERT INTO `tbl_lga` VALUES ('358', 'KD', 'Makarfi');
INSERT INTO `tbl_lga` VALUES ('359', 'KD', 'Sabon-Gari');
INSERT INTO `tbl_lga` VALUES ('360', 'KD', 'Sanga');
INSERT INTO `tbl_lga` VALUES ('361', 'KD', 'Soba');
INSERT INTO `tbl_lga` VALUES ('362', 'KD', 'Zango-Kataf');
INSERT INTO `tbl_lga` VALUES ('363', 'KD', 'Zaria ');
INSERT INTO `tbl_lga` VALUES ('364', 'KN', 'Ajingi');
INSERT INTO `tbl_lga` VALUES ('365', 'KN', 'Albasu');
INSERT INTO `tbl_lga` VALUES ('1', 'FC', 'Kuje');
INSERT INTO `tbl_lga` VALUES ('2', 'FC', ' Abaji');
INSERT INTO `tbl_lga` VALUES ('3', 'FC', ' Abuja Municipal');
INSERT INTO `tbl_lga` VALUES ('4', 'FC', ' Bwari');
INSERT INTO `tbl_lga` VALUES ('5', 'FC', ' Kwali');
INSERT INTO `tbl_lga` VALUES ('6', 'FC', 'Gwagwalada');
INSERT INTO `tbl_lga` VALUES ('7', 'AB', 'Aba South');
INSERT INTO `tbl_lga` VALUES ('8', 'AB', ' Arochukwu');
INSERT INTO `tbl_lga` VALUES ('9', 'AB', ' Bende');
INSERT INTO `tbl_lga` VALUES ('10', 'AB', ' Ikwuano');
INSERT INTO `tbl_lga` VALUES ('11', 'AB', ' Isiala-Ngwa North');
INSERT INTO `tbl_lga` VALUES ('12', 'AB', ' Isiala-Ngwa South');
INSERT INTO `tbl_lga` VALUES ('13', 'AB', ' Isuikwato');
INSERT INTO `tbl_lga` VALUES ('14', 'AB', ' Obi Nwa');
INSERT INTO `tbl_lga` VALUES ('15', 'AB', ' Ohafia');
INSERT INTO `tbl_lga` VALUES ('16', 'AB', ' Osisioma');
INSERT INTO `tbl_lga` VALUES ('17', 'AB', ' Ngwa');
INSERT INTO `tbl_lga` VALUES ('18', 'AB', ' Ugwunagbo');
INSERT INTO `tbl_lga` VALUES ('19', 'AB', ' Ukwa East');
INSERT INTO `tbl_lga` VALUES ('20', 'AB', ' Ukwa West');
INSERT INTO `tbl_lga` VALUES ('21', 'AB', ' Umuahia North');
INSERT INTO `tbl_lga` VALUES ('22', 'AB', ' Umuahia South');
INSERT INTO `tbl_lga` VALUES ('23', 'AB', ' Umu-Neochi');
INSERT INTO `tbl_lga` VALUES ('24', 'AB', 'Aba North');
INSERT INTO `tbl_lga` VALUES ('25', 'AD', 'Demsa');
INSERT INTO `tbl_lga` VALUES ('26', 'AD', 'Fufore');
INSERT INTO `tbl_lga` VALUES ('27', 'AD', 'Ganaye');
INSERT INTO `tbl_lga` VALUES ('28', 'AD', 'Gireri');
INSERT INTO `tbl_lga` VALUES ('29', 'AD', 'Gombi');
INSERT INTO `tbl_lga` VALUES ('30', 'AD', 'Guyuk');
INSERT INTO `tbl_lga` VALUES ('31', 'AD', 'Hong');
INSERT INTO `tbl_lga` VALUES ('32', 'AD', 'Jada');
INSERT INTO `tbl_lga` VALUES ('33', 'AD', 'Lamurde');
INSERT INTO `tbl_lga` VALUES ('34', 'AD', 'Madagali');
INSERT INTO `tbl_lga` VALUES ('35', 'AD', 'Maiha ');
INSERT INTO `tbl_lga` VALUES ('36', 'AD', 'Mayo-Belwa');
INSERT INTO `tbl_lga` VALUES ('37', 'AD', 'Michika');
INSERT INTO `tbl_lga` VALUES ('38', 'AD', 'Mubi North');
INSERT INTO `tbl_lga` VALUES ('39', 'AD', 'Mubi South');
INSERT INTO `tbl_lga` VALUES ('40', 'AD', 'Numan');
INSERT INTO `tbl_lga` VALUES ('41', 'AD', 'Shelleng');
INSERT INTO `tbl_lga` VALUES ('42', 'AD', 'Song');
INSERT INTO `tbl_lga` VALUES ('43', 'AD', 'Toungo');
INSERT INTO `tbl_lga` VALUES ('44', 'AD', 'Yola North');
INSERT INTO `tbl_lga` VALUES ('45', 'AD', 'Yola South');
INSERT INTO `tbl_lga` VALUES ('46', 'AK', 'Abak');
INSERT INTO `tbl_lga` VALUES ('47', 'AK', 'Eastern Obolo');
INSERT INTO `tbl_lga` VALUES ('48', 'AK', 'Eket');
INSERT INTO `tbl_lga` VALUES ('49', 'AK', 'Esit Eket');
INSERT INTO `tbl_lga` VALUES ('50', 'AK', 'Essien Udim');
INSERT INTO `tbl_lga` VALUES ('51', 'AK', 'Etim Ekpo');
INSERT INTO `tbl_lga` VALUES ('52', 'AK', 'Etinan');
INSERT INTO `tbl_lga` VALUES ('53', 'AK', 'Ibeno');
INSERT INTO `tbl_lga` VALUES ('54', 'AK', 'Ibesikpo Asutan');
INSERT INTO `tbl_lga` VALUES ('55', 'AK', 'Ibiono Ibom');
INSERT INTO `tbl_lga` VALUES ('56', 'AK', 'Ika');
INSERT INTO `tbl_lga` VALUES ('57', 'AK', 'Ikono');
INSERT INTO `tbl_lga` VALUES ('58', 'AK', 'Ikot Abasi');
INSERT INTO `tbl_lga` VALUES ('59', 'AK', 'Ikot Ekpene');
INSERT INTO `tbl_lga` VALUES ('60', 'AK', 'Ini');
INSERT INTO `tbl_lga` VALUES ('61', 'AK', 'Itu');
INSERT INTO `tbl_lga` VALUES ('62', 'AK', 'Mbo');
INSERT INTO `tbl_lga` VALUES ('63', 'AK', 'Mkpat Enin');
INSERT INTO `tbl_lga` VALUES ('64', 'AK', 'Nsit Atai');
INSERT INTO `tbl_lga` VALUES ('65', 'AK', 'Nsit Ibom');
INSERT INTO `tbl_lga` VALUES ('66', 'AK', 'Nsit Ubium');
INSERT INTO `tbl_lga` VALUES ('67', 'AK', 'Obot Akara');
INSERT INTO `tbl_lga` VALUES ('68', 'AK', 'Okobo');
INSERT INTO `tbl_lga` VALUES ('69', 'AK', 'Onna');
INSERT INTO `tbl_lga` VALUES ('70', 'AK', 'Oron ');
INSERT INTO `tbl_lga` VALUES ('71', 'AK', 'Oruk Anam');
INSERT INTO `tbl_lga` VALUES ('72', 'AK', 'Udung Uko');
INSERT INTO `tbl_lga` VALUES ('73', 'AK', 'Ukanafun');
INSERT INTO `tbl_lga` VALUES ('74', 'AK', 'Uruan');
INSERT INTO `tbl_lga` VALUES ('75', 'AK', 'Urue-Offong/Oruko');
INSERT INTO `tbl_lga` VALUES ('76', 'AK', 'Uyo');
INSERT INTO `tbl_lga` VALUES ('77', 'AN', 'Aguata');
INSERT INTO `tbl_lga` VALUES ('78', 'AN', 'Anambra East');
INSERT INTO `tbl_lga` VALUES ('79', 'AN', 'Anambra West');
INSERT INTO `tbl_lga` VALUES ('80', 'AN', 'Anaocha');
INSERT INTO `tbl_lga` VALUES ('81', 'AN', 'Awka North');
INSERT INTO `tbl_lga` VALUES ('82', 'AN', 'Awka South');
INSERT INTO `tbl_lga` VALUES ('83', 'AN', 'Ayamelum');
INSERT INTO `tbl_lga` VALUES ('84', 'AN', 'Dunukofia');
INSERT INTO `tbl_lga` VALUES ('85', 'AN', 'Ekwusigo');
INSERT INTO `tbl_lga` VALUES ('86', 'AN', 'Idemili North');
INSERT INTO `tbl_lga` VALUES ('87', 'AN', 'Idemili south');
INSERT INTO `tbl_lga` VALUES ('88', 'AN', 'Ihiala');
INSERT INTO `tbl_lga` VALUES ('89', 'AN', 'Njikoka');
INSERT INTO `tbl_lga` VALUES ('90', 'AN', 'Nnewi North');
INSERT INTO `tbl_lga` VALUES ('91', 'AN', 'Nnewi South');
INSERT INTO `tbl_lga` VALUES ('92', 'AN', 'Ogbaru');
INSERT INTO `tbl_lga` VALUES ('93', 'AN', 'Onitsha North');
INSERT INTO `tbl_lga` VALUES ('94', 'AN', 'Onitsha South');
INSERT INTO `tbl_lga` VALUES ('95', 'AN', 'Orumba North');
INSERT INTO `tbl_lga` VALUES ('96', 'AN', 'Orumba South');
INSERT INTO `tbl_lga` VALUES ('97', 'AN', 'Oyi ');
INSERT INTO `tbl_lga` VALUES ('98', 'BA', 'Alkaleri');
INSERT INTO `tbl_lga` VALUES ('99', 'BA', 'Bauchi');
INSERT INTO `tbl_lga` VALUES ('100', 'BA', 'Bogoro');
INSERT INTO `tbl_lga` VALUES ('101', 'BA', 'Damban');
INSERT INTO `tbl_lga` VALUES ('102', 'BA', 'Darazo');
INSERT INTO `tbl_lga` VALUES ('103', 'BA', 'Dass');
INSERT INTO `tbl_lga` VALUES ('104', 'BA', 'Ganjuwa');
INSERT INTO `tbl_lga` VALUES ('105', 'BA', 'Giade');
INSERT INTO `tbl_lga` VALUES ('106', 'BA', 'Itas/Gadau');
INSERT INTO `tbl_lga` VALUES ('107', 'BA', 'Jama\'are');
INSERT INTO `tbl_lga` VALUES ('108', 'BA', 'Katagum');
INSERT INTO `tbl_lga` VALUES ('109', 'BA', 'Kirfi');
INSERT INTO `tbl_lga` VALUES ('110', 'BA', 'Misau');
INSERT INTO `tbl_lga` VALUES ('111', 'BA', 'Ningi');
INSERT INTO `tbl_lga` VALUES ('112', 'BA', 'Shira');
INSERT INTO `tbl_lga` VALUES ('113', 'BA', 'Tafawa-Balewa');
INSERT INTO `tbl_lga` VALUES ('114', 'BA', 'Toro');
INSERT INTO `tbl_lga` VALUES ('115', 'BA', 'Warji');
INSERT INTO `tbl_lga` VALUES ('116', 'BA', 'Zaki ');
INSERT INTO `tbl_lga` VALUES ('117', 'BY', 'Brass');
INSERT INTO `tbl_lga` VALUES ('118', 'BY', 'Ekeremor');
INSERT INTO `tbl_lga` VALUES ('119', 'BY', 'Kolokuma/Opokuma');
INSERT INTO `tbl_lga` VALUES ('120', 'BY', 'Nembe');
INSERT INTO `tbl_lga` VALUES ('121', 'BY', 'Ogbia');
INSERT INTO `tbl_lga` VALUES ('122', 'BY', 'Sagbama');
INSERT INTO `tbl_lga` VALUES ('123', 'BY', 'Southern Jaw');
INSERT INTO `tbl_lga` VALUES ('124', 'BY', 'Yenegoa ');
INSERT INTO `tbl_lga` VALUES ('125', 'BN', 'Ado');
INSERT INTO `tbl_lga` VALUES ('126', 'BN', 'Agatu');
INSERT INTO `tbl_lga` VALUES ('127', 'BN', 'Apa');
INSERT INTO `tbl_lga` VALUES ('128', 'BN', 'Buruku');
INSERT INTO `tbl_lga` VALUES ('129', 'BN', 'Gboko');
INSERT INTO `tbl_lga` VALUES ('130', 'BN', 'Guma');
INSERT INTO `tbl_lga` VALUES ('131', 'BN', 'Gwer East');
INSERT INTO `tbl_lga` VALUES ('132', 'BN', 'Gwer West');
INSERT INTO `tbl_lga` VALUES ('133', 'BN', 'Katsina-Ala');
INSERT INTO `tbl_lga` VALUES ('134', 'BN', 'Konshisha');
INSERT INTO `tbl_lga` VALUES ('135', 'BN', 'Kwande');
INSERT INTO `tbl_lga` VALUES ('136', 'BN', 'Logo');
INSERT INTO `tbl_lga` VALUES ('137', 'BN', 'Makurdi');
INSERT INTO `tbl_lga` VALUES ('138', 'BN', 'Obi');
INSERT INTO `tbl_lga` VALUES ('139', 'BN', 'Ogbadibo');
INSERT INTO `tbl_lga` VALUES ('140', 'BN', 'Oju');
INSERT INTO `tbl_lga` VALUES ('141', 'BN', 'Okpokwu');
INSERT INTO `tbl_lga` VALUES ('142', 'BN', 'Ohimini');
INSERT INTO `tbl_lga` VALUES ('143', 'BN', 'Oturkpo');
INSERT INTO `tbl_lga` VALUES ('144', 'BN', 'Tarka');
INSERT INTO `tbl_lga` VALUES ('145', 'BN', 'Ukum');
INSERT INTO `tbl_lga` VALUES ('146', 'BN', 'Ushongo');
INSERT INTO `tbl_lga` VALUES ('147', 'BN', 'Vandeikya ');
INSERT INTO `tbl_lga` VALUES ('148', 'BO', ' Abadam');
INSERT INTO `tbl_lga` VALUES ('149', 'BO', 'Askira/Uba');
INSERT INTO `tbl_lga` VALUES ('150', 'BO', 'Bama');
INSERT INTO `tbl_lga` VALUES ('151', 'BO', 'Bayo');
INSERT INTO `tbl_lga` VALUES ('152', 'BO', 'Biu');
INSERT INTO `tbl_lga` VALUES ('153', 'BO', 'Chibok');
INSERT INTO `tbl_lga` VALUES ('154', 'BO', 'Damboa');
INSERT INTO `tbl_lga` VALUES ('155', 'BO', 'Dikwa');
INSERT INTO `tbl_lga` VALUES ('156', 'BO', 'Gubio');
INSERT INTO `tbl_lga` VALUES ('157', 'BO', 'Guzamala');
INSERT INTO `tbl_lga` VALUES ('158', 'BO', 'Gwoza');
INSERT INTO `tbl_lga` VALUES ('159', 'BO', 'Hawul');
INSERT INTO `tbl_lga` VALUES ('160', 'BO', 'Jere');
INSERT INTO `tbl_lga` VALUES ('161', 'BO', 'Kaga');
INSERT INTO `tbl_lga` VALUES ('162', 'BO', 'Kala/Balge');
INSERT INTO `tbl_lga` VALUES ('163', 'BO', 'Konduga');
INSERT INTO `tbl_lga` VALUES ('164', 'BO', 'Kukawa');
INSERT INTO `tbl_lga` VALUES ('165', 'BO', 'Kwaya Kusar');
INSERT INTO `tbl_lga` VALUES ('166', 'BO', 'Mafa');
INSERT INTO `tbl_lga` VALUES ('167', 'BO', 'Magumeri');
INSERT INTO `tbl_lga` VALUES ('168', 'BO', 'Maiduguri');
INSERT INTO `tbl_lga` VALUES ('169', 'BO', 'Marte');
INSERT INTO `tbl_lga` VALUES ('170', 'BO', 'Mobbar');
INSERT INTO `tbl_lga` VALUES ('171', 'BO', 'Monguno');
INSERT INTO `tbl_lga` VALUES ('172', 'BO', 'Ngala');
INSERT INTO `tbl_lga` VALUES ('173', 'BO', 'Nganzai');
INSERT INTO `tbl_lga` VALUES ('174', 'BO', 'Shani ');
INSERT INTO `tbl_lga` VALUES ('175', 'CR', 'Akpabuyo');
INSERT INTO `tbl_lga` VALUES ('176', 'CR', 'Odukpani');
INSERT INTO `tbl_lga` VALUES ('177', 'CR', 'Akamkpa');
INSERT INTO `tbl_lga` VALUES ('178', 'CR', 'Biase');
INSERT INTO `tbl_lga` VALUES ('179', 'CR', 'Abi');
INSERT INTO `tbl_lga` VALUES ('180', 'CR', 'Ikom');
INSERT INTO `tbl_lga` VALUES ('181', 'CR', 'Yarkur');
INSERT INTO `tbl_lga` VALUES ('182', 'CR', 'Odubra');
INSERT INTO `tbl_lga` VALUES ('183', 'CR', 'Boki');
INSERT INTO `tbl_lga` VALUES ('184', 'CR', 'Ogoja');
INSERT INTO `tbl_lga` VALUES ('185', 'CR', 'Yala');
INSERT INTO `tbl_lga` VALUES ('186', 'CR', 'Obanliku');
INSERT INTO `tbl_lga` VALUES ('187', 'CR', 'Obudu');
INSERT INTO `tbl_lga` VALUES ('188', 'CR', 'Calabar South');
INSERT INTO `tbl_lga` VALUES ('366', 'KN', 'Bagwai');
INSERT INTO `tbl_lga` VALUES ('367', 'KN', 'Bebeji');
INSERT INTO `tbl_lga` VALUES ('368', 'KN', 'Bichi');
INSERT INTO `tbl_lga` VALUES ('369', 'KN', 'Bunkure');
INSERT INTO `tbl_lga` VALUES ('370', 'KN', 'Dala');
INSERT INTO `tbl_lga` VALUES ('371', 'KN', 'Dambatta');
INSERT INTO `tbl_lga` VALUES ('372', 'KN', 'Dawakin Kudu');
INSERT INTO `tbl_lga` VALUES ('373', 'KN', 'Dawakin Tofa');
INSERT INTO `tbl_lga` VALUES ('374', 'KN', 'Doguwa');
INSERT INTO `tbl_lga` VALUES ('375', 'KN', 'Fagge');
INSERT INTO `tbl_lga` VALUES ('376', 'KN', 'Gabasawa');
INSERT INTO `tbl_lga` VALUES ('377', 'KN', 'Garko');
INSERT INTO `tbl_lga` VALUES ('378', 'KN', 'Garum');
INSERT INTO `tbl_lga` VALUES ('379', 'KN', 'Mallam');
INSERT INTO `tbl_lga` VALUES ('380', 'KN', 'Gaya');
INSERT INTO `tbl_lga` VALUES ('381', 'KN', 'Gezawa');
INSERT INTO `tbl_lga` VALUES ('382', 'KN', 'Gwale');
INSERT INTO `tbl_lga` VALUES ('383', 'KN', 'Gwarzo');
INSERT INTO `tbl_lga` VALUES ('384', 'KN', 'Kabo');
INSERT INTO `tbl_lga` VALUES ('385', 'KN', 'Kano Municipal');
INSERT INTO `tbl_lga` VALUES ('386', 'KN', 'Karaye');
INSERT INTO `tbl_lga` VALUES ('387', 'KN', 'Kibiya');
INSERT INTO `tbl_lga` VALUES ('388', 'KN', 'Kiru');
INSERT INTO `tbl_lga` VALUES ('389', 'KN', 'kumbotso');
INSERT INTO `tbl_lga` VALUES ('390', 'KN', 'Kunchi');
INSERT INTO `tbl_lga` VALUES ('391', 'KN', 'Kura');
INSERT INTO `tbl_lga` VALUES ('392', 'KN', 'Madobi');
INSERT INTO `tbl_lga` VALUES ('393', 'KN', 'Makoda');
INSERT INTO `tbl_lga` VALUES ('394', 'KN', 'Minjibir');
INSERT INTO `tbl_lga` VALUES ('395', 'KN', 'Nasarawa');
INSERT INTO `tbl_lga` VALUES ('396', 'KN', 'Rano');
INSERT INTO `tbl_lga` VALUES ('397', 'KN', 'Rimin Gado');
INSERT INTO `tbl_lga` VALUES ('398', 'KN', 'Rogo');
INSERT INTO `tbl_lga` VALUES ('399', 'KN', 'Shanono');
INSERT INTO `tbl_lga` VALUES ('400', 'KN', 'Sumaila');
INSERT INTO `tbl_lga` VALUES ('401', 'KN', 'Takali');
INSERT INTO `tbl_lga` VALUES ('402', 'KN', 'Tarauni');
INSERT INTO `tbl_lga` VALUES ('403', 'KN', 'Tofa');
INSERT INTO `tbl_lga` VALUES ('404', 'KN', 'Tsanyawa');
INSERT INTO `tbl_lga` VALUES ('405', 'KN', 'Tudun Wada');
INSERT INTO `tbl_lga` VALUES ('406', 'KN', 'Ungogo');
INSERT INTO `tbl_lga` VALUES ('407', 'KN', 'Warawa');
INSERT INTO `tbl_lga` VALUES ('408', 'KN', 'Wudil');
INSERT INTO `tbl_lga` VALUES ('409', 'KT', 'Bakori');
INSERT INTO `tbl_lga` VALUES ('410', 'KT', 'Batagarawa');
INSERT INTO `tbl_lga` VALUES ('411', 'KT', 'Batsari');
INSERT INTO `tbl_lga` VALUES ('412', 'KT', 'Baure');
INSERT INTO `tbl_lga` VALUES ('413', 'KT', 'Bindawa');
INSERT INTO `tbl_lga` VALUES ('414', 'KT', 'Charanchi');
INSERT INTO `tbl_lga` VALUES ('415', 'KT', 'Dandume');
INSERT INTO `tbl_lga` VALUES ('416', 'KT', 'Danja');
INSERT INTO `tbl_lga` VALUES ('417', 'KT', 'Dan Musa');
INSERT INTO `tbl_lga` VALUES ('418', 'KT', 'Daura');
INSERT INTO `tbl_lga` VALUES ('419', 'KT', 'Dutsi');
INSERT INTO `tbl_lga` VALUES ('420', 'KT', 'Dutsin-Ma');
INSERT INTO `tbl_lga` VALUES ('421', 'KT', 'Faskari');
INSERT INTO `tbl_lga` VALUES ('422', 'KT', 'Funtua');
INSERT INTO `tbl_lga` VALUES ('423', 'KT', 'Ingawa');
INSERT INTO `tbl_lga` VALUES ('424', 'KT', 'Jibia');
INSERT INTO `tbl_lga` VALUES ('425', 'KT', 'Kafur');
INSERT INTO `tbl_lga` VALUES ('426', 'KT', 'Kaita');
INSERT INTO `tbl_lga` VALUES ('427', 'KT', 'Kankara');
INSERT INTO `tbl_lga` VALUES ('428', 'KT', 'Kankia');
INSERT INTO `tbl_lga` VALUES ('429', 'KT', 'Katsina');
INSERT INTO `tbl_lga` VALUES ('430', 'KT', 'Kurfi');
INSERT INTO `tbl_lga` VALUES ('431', 'KT', 'Kusada');
INSERT INTO `tbl_lga` VALUES ('432', 'KT', 'Mai\'Adua');
INSERT INTO `tbl_lga` VALUES ('433', 'KT', 'Malumfashi');
INSERT INTO `tbl_lga` VALUES ('434', 'KT', 'Mani');
INSERT INTO `tbl_lga` VALUES ('435', 'KT', 'Mashi');
INSERT INTO `tbl_lga` VALUES ('436', 'KT', 'Matazuu');
INSERT INTO `tbl_lga` VALUES ('437', 'KT', 'Musawa');
INSERT INTO `tbl_lga` VALUES ('438', 'KT', 'Rimi');
INSERT INTO `tbl_lga` VALUES ('439', 'KT', 'Sabuwa');
INSERT INTO `tbl_lga` VALUES ('440', 'KT', 'Safana');
INSERT INTO `tbl_lga` VALUES ('441', 'KT', 'Sandamu');
INSERT INTO `tbl_lga` VALUES ('442', 'KT', 'Zango ');
INSERT INTO `tbl_lga` VALUES ('443', 'KB', 'Aleiro');
INSERT INTO `tbl_lga` VALUES ('444', 'KB', 'Arewa-Dandi');
INSERT INTO `tbl_lga` VALUES ('445', 'KB', 'Argungu');
INSERT INTO `tbl_lga` VALUES ('446', 'KB', 'Augie');
INSERT INTO `tbl_lga` VALUES ('447', 'KB', 'Bagudo');
INSERT INTO `tbl_lga` VALUES ('448', 'KB', 'Birnin Kebbi');
INSERT INTO `tbl_lga` VALUES ('449', 'KB', 'Bunza');
INSERT INTO `tbl_lga` VALUES ('450', 'KB', 'Dandi ');
INSERT INTO `tbl_lga` VALUES ('451', 'KB', 'Fakai');
INSERT INTO `tbl_lga` VALUES ('452', 'KB', 'Gwandu');
INSERT INTO `tbl_lga` VALUES ('453', 'KB', 'Jega');
INSERT INTO `tbl_lga` VALUES ('454', 'KB', 'Kalgo ');
INSERT INTO `tbl_lga` VALUES ('455', 'KB', 'Koko/Besse');
INSERT INTO `tbl_lga` VALUES ('456', 'KB', 'Maiyama');
INSERT INTO `tbl_lga` VALUES ('457', 'KB', 'Ngaski');
INSERT INTO `tbl_lga` VALUES ('458', 'KB', 'Sakaba');
INSERT INTO `tbl_lga` VALUES ('459', 'KB', 'Shanga');
INSERT INTO `tbl_lga` VALUES ('460', 'KB', 'Suru');
INSERT INTO `tbl_lga` VALUES ('461', 'KB', 'Wasagu/Danko');
INSERT INTO `tbl_lga` VALUES ('462', 'KB', 'Yauri');
INSERT INTO `tbl_lga` VALUES ('463', 'KB', 'Zuru ');
INSERT INTO `tbl_lga` VALUES ('467', 'KG', 'Adavi');
INSERT INTO `tbl_lga` VALUES ('468', 'KG', 'Ajaokuta');
INSERT INTO `tbl_lga` VALUES ('469', 'KG', 'Ankpa');
INSERT INTO `tbl_lga` VALUES ('470', 'KG', 'Bassa');
INSERT INTO `tbl_lga` VALUES ('471', 'KG', 'Dekina');
INSERT INTO `tbl_lga` VALUES ('472', 'KG', 'Ibaji');
INSERT INTO `tbl_lga` VALUES ('473', 'KG', 'Idah');
INSERT INTO `tbl_lga` VALUES ('474', 'KG', 'Igalamela-Odolu');
INSERT INTO `tbl_lga` VALUES ('475', 'KG', 'Ijumu');
INSERT INTO `tbl_lga` VALUES ('476', 'KG', 'Kabba/Bunu');
INSERT INTO `tbl_lga` VALUES ('477', 'KG', 'Kogi');
INSERT INTO `tbl_lga` VALUES ('478', 'KG', 'Lokoja');
INSERT INTO `tbl_lga` VALUES ('479', 'KG', 'Mopa-Muro');
INSERT INTO `tbl_lga` VALUES ('480', 'KG', 'Ofu');
INSERT INTO `tbl_lga` VALUES ('481', 'KG', 'Ogori/Mangongo');
INSERT INTO `tbl_lga` VALUES ('482', 'KG', 'Okehi');
INSERT INTO `tbl_lga` VALUES ('483', 'KG', 'Okene');
INSERT INTO `tbl_lga` VALUES ('484', 'KG', 'Olamabolo');
INSERT INTO `tbl_lga` VALUES ('485', 'KG', 'Omala');
INSERT INTO `tbl_lga` VALUES ('486', 'KG', 'Yagba East ');
INSERT INTO `tbl_lga` VALUES ('487', 'KG', 'Yagba West');
INSERT INTO `tbl_lga` VALUES ('488', 'KW', 'Asa');
INSERT INTO `tbl_lga` VALUES ('489', 'KW', 'Baruten');
INSERT INTO `tbl_lga` VALUES ('490', 'KW', 'Edu');
INSERT INTO `tbl_lga` VALUES ('491', 'KW', 'Ekiti');
INSERT INTO `tbl_lga` VALUES ('492', 'KW', 'Ifelodun');
INSERT INTO `tbl_lga` VALUES ('493', 'KW', 'Ilorin East');
INSERT INTO `tbl_lga` VALUES ('494', 'KW', 'Ilorin West');
INSERT INTO `tbl_lga` VALUES ('495', 'KW', 'Irepodun');
INSERT INTO `tbl_lga` VALUES ('496', 'KW', 'Isin');
INSERT INTO `tbl_lga` VALUES ('497', 'KW', 'Kaiama');
INSERT INTO `tbl_lga` VALUES ('498', 'KW', 'Moro');
INSERT INTO `tbl_lga` VALUES ('499', 'KW', 'Offa');
INSERT INTO `tbl_lga` VALUES ('500', 'KW', 'Oke-Ero');
INSERT INTO `tbl_lga` VALUES ('501', 'KW', 'Oyun');
INSERT INTO `tbl_lga` VALUES ('502', 'KW', 'Pategi ');
INSERT INTO `tbl_lga` VALUES ('503', 'LA', 'Agege');
INSERT INTO `tbl_lga` VALUES ('504', 'LA', 'Ajeromi-Ifelodun');
INSERT INTO `tbl_lga` VALUES ('505', 'LA', 'Alimosho');
INSERT INTO `tbl_lga` VALUES ('506', 'LA', 'Amuwo-Odofin');
INSERT INTO `tbl_lga` VALUES ('507', 'LA', 'Apapa');
INSERT INTO `tbl_lga` VALUES ('508', 'LA', 'Badagry');
INSERT INTO `tbl_lga` VALUES ('509', 'LA', 'Epe');
INSERT INTO `tbl_lga` VALUES ('510', 'LA', 'Eti-Osa');
INSERT INTO `tbl_lga` VALUES ('511', 'LA', 'Ibeju/Lekki');
INSERT INTO `tbl_lga` VALUES ('512', 'LA', 'Ifako-Ijaye ');
INSERT INTO `tbl_lga` VALUES ('513', 'LA', 'Ikeja');
INSERT INTO `tbl_lga` VALUES ('514', 'LA', 'Ikorodu');
INSERT INTO `tbl_lga` VALUES ('515', 'LA', 'Kosofe');
INSERT INTO `tbl_lga` VALUES ('516', 'LA', 'Lagos Island');
INSERT INTO `tbl_lga` VALUES ('517', 'LA', 'Lagos Mainland');
INSERT INTO `tbl_lga` VALUES ('518', 'LA', 'Mushin');
INSERT INTO `tbl_lga` VALUES ('519', 'LA', 'Ojo');
INSERT INTO `tbl_lga` VALUES ('520', 'LA', 'Oshodi-Isolo');
INSERT INTO `tbl_lga` VALUES ('521', 'LA', 'Shomolu');
INSERT INTO `tbl_lga` VALUES ('522', 'LA', 'Surulere');
INSERT INTO `tbl_lga` VALUES ('523', 'NS', 'Akwanga');
INSERT INTO `tbl_lga` VALUES ('524', 'NS', 'Awe');
INSERT INTO `tbl_lga` VALUES ('525', 'NS', 'Doma');
INSERT INTO `tbl_lga` VALUES ('526', 'NS', 'Karu');
INSERT INTO `tbl_lga` VALUES ('527', 'NS', 'Keana');
INSERT INTO `tbl_lga` VALUES ('528', 'NS', 'Keffi');
INSERT INTO `tbl_lga` VALUES ('529', 'NS', 'Kokona');
INSERT INTO `tbl_lga` VALUES ('530', 'NS', 'Lafia');
INSERT INTO `tbl_lga` VALUES ('531', 'NS', 'Nasarawa');
INSERT INTO `tbl_lga` VALUES ('532', 'NS', 'Nasarawa-Eggon');
INSERT INTO `tbl_lga` VALUES ('533', 'NS', 'Obi');
INSERT INTO `tbl_lga` VALUES ('534', 'NS', 'Toto');
INSERT INTO `tbl_lga` VALUES ('535', 'NS', 'Wamba ');
INSERT INTO `tbl_lga` VALUES ('536', 'NG', 'Agaie');
INSERT INTO `tbl_lga` VALUES ('537', 'NG', 'Agwara');
INSERT INTO `tbl_lga` VALUES ('538', 'NG', 'Bida');
INSERT INTO `tbl_lga` VALUES ('539', 'NG', 'Borgu');
INSERT INTO `tbl_lga` VALUES ('540', 'NG', 'Bosso');
INSERT INTO `tbl_lga` VALUES ('541', 'NG', 'Chanchaga');
INSERT INTO `tbl_lga` VALUES ('542', 'NG', 'Edati');
INSERT INTO `tbl_lga` VALUES ('543', 'NG', 'Gbako');
INSERT INTO `tbl_lga` VALUES ('544', 'NG', 'Gurara');
INSERT INTO `tbl_lga` VALUES ('545', 'NG', 'Katcha');
INSERT INTO `tbl_lga` VALUES ('546', 'NG', 'Kontagora ');
INSERT INTO `tbl_lga` VALUES ('547', 'NG', 'Lapai');
INSERT INTO `tbl_lga` VALUES ('548', 'NG', 'Lavun');
INSERT INTO `tbl_lga` VALUES ('549', 'NG', 'Magama');
INSERT INTO `tbl_lga` VALUES ('550', 'NG', 'Mariga');
INSERT INTO `tbl_lga` VALUES ('551', 'NG', 'Mashegu');
INSERT INTO `tbl_lga` VALUES ('552', 'NG', 'Mokwa');
INSERT INTO `tbl_lga` VALUES ('553', 'NG', 'Muya');
INSERT INTO `tbl_lga` VALUES ('554', 'NG', 'Pailoro');
INSERT INTO `tbl_lga` VALUES ('555', 'NG', 'Rafi');
INSERT INTO `tbl_lga` VALUES ('556', 'NG', 'Rijau');
INSERT INTO `tbl_lga` VALUES ('557', 'NG', 'Shiroro');
INSERT INTO `tbl_lga` VALUES ('558', 'NG', 'Suleja');
INSERT INTO `tbl_lga` VALUES ('559', 'NG', 'Tafa');
INSERT INTO `tbl_lga` VALUES ('560', 'NG', 'Wushishi');
INSERT INTO `tbl_lga` VALUES ('561', 'OG', 'Abeokuta North');
INSERT INTO `tbl_lga` VALUES ('562', 'OG', 'Abeokuta South');
INSERT INTO `tbl_lga` VALUES ('563', 'OG', 'Ado-Odo/Ota');
INSERT INTO `tbl_lga` VALUES ('564', 'OG', 'Egbado North');
INSERT INTO `tbl_lga` VALUES ('565', 'OG', 'Egbado South');
INSERT INTO `tbl_lga` VALUES ('566', 'OG', 'Ewekoro');
INSERT INTO `tbl_lga` VALUES ('567', 'OG', 'Ifo');
INSERT INTO `tbl_lga` VALUES ('568', 'OG', 'Ijebu East');
INSERT INTO `tbl_lga` VALUES ('569', 'OG', 'Ijebu North');
INSERT INTO `tbl_lga` VALUES ('570', 'OG', 'Ijebu North East');
INSERT INTO `tbl_lga` VALUES ('571', 'OG', 'Ijebu Ode');
INSERT INTO `tbl_lga` VALUES ('572', 'OG', 'Ikenne');
INSERT INTO `tbl_lga` VALUES ('573', 'OG', 'Imeko-Afon');
INSERT INTO `tbl_lga` VALUES ('574', 'OG', 'Ipokia');
INSERT INTO `tbl_lga` VALUES ('575', 'OG', 'Obafemi-Owode');
INSERT INTO `tbl_lga` VALUES ('576', 'OG', 'Ogun Waterside');
INSERT INTO `tbl_lga` VALUES ('577', 'OG', 'Odeda');
INSERT INTO `tbl_lga` VALUES ('578', 'OG', 'Odogbolu');
INSERT INTO `tbl_lga` VALUES ('579', 'OG', 'Remo North');
INSERT INTO `tbl_lga` VALUES ('580', 'OG', 'Shagamu');
INSERT INTO `tbl_lga` VALUES ('581', 'OD', 'Akoko North East');
INSERT INTO `tbl_lga` VALUES ('582', 'OD', 'Akoko North West');
INSERT INTO `tbl_lga` VALUES ('583', 'OD', 'Akoko South Akure East');
INSERT INTO `tbl_lga` VALUES ('584', 'OD', 'Akoko South West');
INSERT INTO `tbl_lga` VALUES ('585', 'OD', 'Akure North');
INSERT INTO `tbl_lga` VALUES ('586', 'OD', 'Akure South');
INSERT INTO `tbl_lga` VALUES ('587', 'OD', 'Ese-Odo');
INSERT INTO `tbl_lga` VALUES ('588', 'OD', 'Idanre');
INSERT INTO `tbl_lga` VALUES ('589', 'OD', 'Ifedore');
INSERT INTO `tbl_lga` VALUES ('590', 'OD', 'Ilaje');
INSERT INTO `tbl_lga` VALUES ('591', 'OD', 'Ile-Oluji');
INSERT INTO `tbl_lga` VALUES ('592', 'OD', 'Okeigbo');
INSERT INTO `tbl_lga` VALUES ('593', 'OD', 'Irele');
INSERT INTO `tbl_lga` VALUES ('594', 'OD', 'Odigbo');
INSERT INTO `tbl_lga` VALUES ('595', 'OD', 'Okitipupa');
INSERT INTO `tbl_lga` VALUES ('596', 'OD', 'Ondo East');
INSERT INTO `tbl_lga` VALUES ('597', 'OD', 'Ondo West');
INSERT INTO `tbl_lga` VALUES ('598', 'OD', 'Ose');
INSERT INTO `tbl_lga` VALUES ('599', 'OD', 'Owo ');
INSERT INTO `tbl_lga` VALUES ('600', 'OS', 'Aiyedade');
INSERT INTO `tbl_lga` VALUES ('601', 'OS', 'Aiyedire');
INSERT INTO `tbl_lga` VALUES ('602', 'OS', 'Atakumosa East');
INSERT INTO `tbl_lga` VALUES ('603', 'OS', 'Atakumosa West');
INSERT INTO `tbl_lga` VALUES ('604', 'OS', 'Boluwaduro');
INSERT INTO `tbl_lga` VALUES ('605', 'OS', 'Boripe');
INSERT INTO `tbl_lga` VALUES ('606', 'OS', 'Ede North');
INSERT INTO `tbl_lga` VALUES ('607', 'OS', 'Ede South');
INSERT INTO `tbl_lga` VALUES ('608', 'OS', 'Egbedore');
INSERT INTO `tbl_lga` VALUES ('609', 'OS', 'Ejigbo');
INSERT INTO `tbl_lga` VALUES ('610', 'OS', 'Ife Central');
INSERT INTO `tbl_lga` VALUES ('611', 'OS', 'Ife East');
INSERT INTO `tbl_lga` VALUES ('612', 'OS', 'Ife North');
INSERT INTO `tbl_lga` VALUES ('613', 'OS', 'Ife South');
INSERT INTO `tbl_lga` VALUES ('614', 'OS', 'Ifedayo');
INSERT INTO `tbl_lga` VALUES ('615', 'OS', 'Ifelodun');
INSERT INTO `tbl_lga` VALUES ('616', 'OS', 'Ila');
INSERT INTO `tbl_lga` VALUES ('617', 'OS', 'Ilesha East');
INSERT INTO `tbl_lga` VALUES ('618', 'OS', 'Ilesha West');
INSERT INTO `tbl_lga` VALUES ('619', 'OS', 'Irepodun');
INSERT INTO `tbl_lga` VALUES ('620', 'OS', 'Irewole');
INSERT INTO `tbl_lga` VALUES ('621', 'OS', 'Isokan');
INSERT INTO `tbl_lga` VALUES ('622', 'OS', 'Iwo');
INSERT INTO `tbl_lga` VALUES ('623', 'OS', 'Obokun');
INSERT INTO `tbl_lga` VALUES ('624', 'OS', 'Odo-Otin');
INSERT INTO `tbl_lga` VALUES ('625', 'OS', 'Ola-Oluwa');
INSERT INTO `tbl_lga` VALUES ('626', 'OS', 'Olorunda');
INSERT INTO `tbl_lga` VALUES ('627', 'OS', 'Oriade');
INSERT INTO `tbl_lga` VALUES ('628', 'OS', 'Orolu');
INSERT INTO `tbl_lga` VALUES ('629', 'OS', 'Osogbo');
INSERT INTO `tbl_lga` VALUES ('630', 'OY', 'Afijio');
INSERT INTO `tbl_lga` VALUES ('631', 'OY', 'Akinyele');
INSERT INTO `tbl_lga` VALUES ('632', 'OY', 'Atiba');
INSERT INTO `tbl_lga` VALUES ('633', 'OY', 'Atigbo');
INSERT INTO `tbl_lga` VALUES ('634', 'OY', 'Egbeda');
INSERT INTO `tbl_lga` VALUES ('635', 'OY', 'IbadanCentral');
INSERT INTO `tbl_lga` VALUES ('636', 'OY', 'Ibadan North');
INSERT INTO `tbl_lga` VALUES ('637', 'OY', 'Ibadan North West');
INSERT INTO `tbl_lga` VALUES ('638', 'OY', 'Ibadan South East');
INSERT INTO `tbl_lga` VALUES ('639', 'OY', 'Ibadan South West');
INSERT INTO `tbl_lga` VALUES ('640', 'OY', 'Ibarapa Central');
INSERT INTO `tbl_lga` VALUES ('641', 'OY', 'Ibarapa East');
INSERT INTO `tbl_lga` VALUES ('642', 'OY', 'Ibarapa North');
INSERT INTO `tbl_lga` VALUES ('643', 'OY', 'Ido');
INSERT INTO `tbl_lga` VALUES ('644', 'OY', 'Irepo');
INSERT INTO `tbl_lga` VALUES ('645', 'OY', 'Iseyin');
INSERT INTO `tbl_lga` VALUES ('646', 'OY', 'Itesiwaju');
INSERT INTO `tbl_lga` VALUES ('647', 'OY', 'Iwajowa');
INSERT INTO `tbl_lga` VALUES ('648', 'OY', 'Kajola');
INSERT INTO `tbl_lga` VALUES ('649', 'OY', 'Lagelu Ogbomosho North');
INSERT INTO `tbl_lga` VALUES ('650', 'OY', 'Ogbmosho South');
INSERT INTO `tbl_lga` VALUES ('651', 'OY', 'Ogo Oluwa');
INSERT INTO `tbl_lga` VALUES ('652', 'OY', 'Olorunsogo');
INSERT INTO `tbl_lga` VALUES ('653', 'OY', 'Oluyole');
INSERT INTO `tbl_lga` VALUES ('654', 'OY', 'Ona-Ara');
INSERT INTO `tbl_lga` VALUES ('655', 'OY', 'Orelope');
INSERT INTO `tbl_lga` VALUES ('656', 'OY', 'Ori Ire');
INSERT INTO `tbl_lga` VALUES ('657', 'OY', 'Oyo East');
INSERT INTO `tbl_lga` VALUES ('658', 'OY', 'Oyo West');
INSERT INTO `tbl_lga` VALUES ('659', 'OY', 'Saki East');
INSERT INTO `tbl_lga` VALUES ('660', 'OY', 'Saki West');
INSERT INTO `tbl_lga` VALUES ('661', 'OY', 'Surulere');
INSERT INTO `tbl_lga` VALUES ('662', 'PL', 'Barkin Ladi');
INSERT INTO `tbl_lga` VALUES ('663', 'PL', 'Bassa');
INSERT INTO `tbl_lga` VALUES ('664', 'PL', 'Bokkos');
INSERT INTO `tbl_lga` VALUES ('665', 'PL', 'Jos East');
INSERT INTO `tbl_lga` VALUES ('666', 'PL', 'Jos North');
INSERT INTO `tbl_lga` VALUES ('667', 'PL', 'Jos South');
INSERT INTO `tbl_lga` VALUES ('668', 'PL', 'Kanam');
INSERT INTO `tbl_lga` VALUES ('669', 'PL', 'Kanke');
INSERT INTO `tbl_lga` VALUES ('670', 'PL', 'Langtang North');
INSERT INTO `tbl_lga` VALUES ('671', 'PL', 'Langtang South');
INSERT INTO `tbl_lga` VALUES ('672', 'PL', 'Mangu');
INSERT INTO `tbl_lga` VALUES ('673', 'PL', 'Mikang');
INSERT INTO `tbl_lga` VALUES ('674', 'PL', 'Pankshin');
INSERT INTO `tbl_lga` VALUES ('675', 'PL', 'Qua\'an Pan');
INSERT INTO `tbl_lga` VALUES ('676', 'PL', 'Riyom');
INSERT INTO `tbl_lga` VALUES ('677', 'PL', 'Shendam');
INSERT INTO `tbl_lga` VALUES ('678', 'PL', 'Wase');
INSERT INTO `tbl_lga` VALUES ('679', 'RV', 'Abua/Odual');
INSERT INTO `tbl_lga` VALUES ('680', 'RV', 'Ahoada East');
INSERT INTO `tbl_lga` VALUES ('681', 'RV', 'Ahoada West');
INSERT INTO `tbl_lga` VALUES ('682', 'RV', 'Akuku Toru');
INSERT INTO `tbl_lga` VALUES ('683', 'RV', 'Andoni');
INSERT INTO `tbl_lga` VALUES ('684', 'RV', 'Asari-Toru');
INSERT INTO `tbl_lga` VALUES ('685', 'RV', 'Bonny');
INSERT INTO `tbl_lga` VALUES ('686', 'RV', 'Degema');
INSERT INTO `tbl_lga` VALUES ('687', 'RV', 'Emohua');
INSERT INTO `tbl_lga` VALUES ('688', 'RV', 'Eleme');
INSERT INTO `tbl_lga` VALUES ('689', 'RV', 'Etche');
INSERT INTO `tbl_lga` VALUES ('690', 'RV', 'Gokana');
INSERT INTO `tbl_lga` VALUES ('691', 'RV', 'Ikwerre');
INSERT INTO `tbl_lga` VALUES ('692', 'RV', 'Khana');
INSERT INTO `tbl_lga` VALUES ('693', 'RV', 'Obia/Akpor');
INSERT INTO `tbl_lga` VALUES ('694', 'RV', 'Ogba/Egbema/Ndoni');
INSERT INTO `tbl_lga` VALUES ('695', 'RV', 'Ogu/Bolo');
INSERT INTO `tbl_lga` VALUES ('696', 'RV', 'Okrika');
INSERT INTO `tbl_lga` VALUES ('697', 'RV', 'Omumma');
INSERT INTO `tbl_lga` VALUES ('698', 'RV', 'Opobo/Nkoro');
INSERT INTO `tbl_lga` VALUES ('699', 'RV', 'Oyigbo');
INSERT INTO `tbl_lga` VALUES ('700', 'RV', 'Port-Harcourt');
INSERT INTO `tbl_lga` VALUES ('701', 'RV', 'Tai ');
INSERT INTO `tbl_lga` VALUES ('702', 'SO', 'Binji');
INSERT INTO `tbl_lga` VALUES ('703', 'SO', 'Bodinga');
INSERT INTO `tbl_lga` VALUES ('704', 'SO', 'Dange-shnsi');
INSERT INTO `tbl_lga` VALUES ('705', 'SO', 'Gada');
INSERT INTO `tbl_lga` VALUES ('706', 'SO', 'Goronyo');
INSERT INTO `tbl_lga` VALUES ('707', 'SO', 'Gudu');
INSERT INTO `tbl_lga` VALUES ('708', 'SO', 'Gawabawa');
INSERT INTO `tbl_lga` VALUES ('709', 'SO', 'Illela');
INSERT INTO `tbl_lga` VALUES ('710', 'SO', 'Isa');
INSERT INTO `tbl_lga` VALUES ('711', 'SO', 'Kware');
INSERT INTO `tbl_lga` VALUES ('712', 'SO', 'kebbe');
INSERT INTO `tbl_lga` VALUES ('713', 'SO', 'Rabah');
INSERT INTO `tbl_lga` VALUES ('714', 'SO', 'Sabon birni');
INSERT INTO `tbl_lga` VALUES ('715', 'SO', 'Shagari');
INSERT INTO `tbl_lga` VALUES ('716', 'SO', 'Silame');
INSERT INTO `tbl_lga` VALUES ('717', 'SO', 'Sokoto North');
INSERT INTO `tbl_lga` VALUES ('718', 'SO', 'Sokoto South');
INSERT INTO `tbl_lga` VALUES ('719', 'SO', 'Tambuwal');
INSERT INTO `tbl_lga` VALUES ('720', 'SO', 'Tqngaza');
INSERT INTO `tbl_lga` VALUES ('721', 'SO', 'Tureta');
INSERT INTO `tbl_lga` VALUES ('722', 'SO', 'Wamako');
INSERT INTO `tbl_lga` VALUES ('723', 'SO', 'Wurno');
INSERT INTO `tbl_lga` VALUES ('724', 'SO', 'Yabo');
INSERT INTO `tbl_lga` VALUES ('725', 'TR', 'Ardo-Kola');
INSERT INTO `tbl_lga` VALUES ('726', 'TR', 'Bali');
INSERT INTO `tbl_lga` VALUES ('727', 'TR', 'Donga');
INSERT INTO `tbl_lga` VALUES ('728', 'TR', 'Gashaka');
INSERT INTO `tbl_lga` VALUES ('729', 'TR', 'Cassol');
INSERT INTO `tbl_lga` VALUES ('730', 'TR', 'Ibi');
INSERT INTO `tbl_lga` VALUES ('731', 'TR', 'Jalingo');
INSERT INTO `tbl_lga` VALUES ('732', 'TR', 'Karin-Lamido');
INSERT INTO `tbl_lga` VALUES ('733', 'TR', 'Kurmi');
INSERT INTO `tbl_lga` VALUES ('734', 'TR', 'Lau');
INSERT INTO `tbl_lga` VALUES ('735', 'TR', 'Sardauna');
INSERT INTO `tbl_lga` VALUES ('736', 'TR', 'Takum');
INSERT INTO `tbl_lga` VALUES ('737', 'TR', 'Ussa');
INSERT INTO `tbl_lga` VALUES ('738', 'TR', 'Wukari');
INSERT INTO `tbl_lga` VALUES ('739', 'TR', 'Yorro');
INSERT INTO `tbl_lga` VALUES ('740', 'TR', 'Zing');
INSERT INTO `tbl_lga` VALUES ('741', 'YB', 'Bade');
INSERT INTO `tbl_lga` VALUES ('742', 'YB', 'Bursari');
INSERT INTO `tbl_lga` VALUES ('743', 'YB', 'Damaturu');
INSERT INTO `tbl_lga` VALUES ('744', 'YB', 'Fika');
INSERT INTO `tbl_lga` VALUES ('745', 'YB', 'Fune');
INSERT INTO `tbl_lga` VALUES ('746', 'YB', 'Geidam');
INSERT INTO `tbl_lga` VALUES ('747', 'YB', 'Gujba');
INSERT INTO `tbl_lga` VALUES ('748', 'YB', 'Gulani');
INSERT INTO `tbl_lga` VALUES ('749', 'YB', 'Jakusko');
INSERT INTO `tbl_lga` VALUES ('750', 'YB', 'Karasuwa');
INSERT INTO `tbl_lga` VALUES ('751', 'YB', 'Karawa');
INSERT INTO `tbl_lga` VALUES ('752', 'YB', 'Machina');
INSERT INTO `tbl_lga` VALUES ('753', 'YB', 'Nangere');
INSERT INTO `tbl_lga` VALUES ('754', 'YB', 'Nguru Potiskum');
INSERT INTO `tbl_lga` VALUES ('755', 'YB', 'Tarmua');
INSERT INTO `tbl_lga` VALUES ('756', 'YB', 'Yunusari');
INSERT INTO `tbl_lga` VALUES ('757', 'YB', 'Yusufari');
INSERT INTO `tbl_lga` VALUES ('758', 'ZM', 'Anka');
INSERT INTO `tbl_lga` VALUES ('759', 'ZM', 'Bakura');
INSERT INTO `tbl_lga` VALUES ('760', 'ZM', 'Birnin Magaji');
INSERT INTO `tbl_lga` VALUES ('761', 'ZM', 'Bukkuyum');
INSERT INTO `tbl_lga` VALUES ('762', 'ZM', 'Bungudu');
INSERT INTO `tbl_lga` VALUES ('763', 'ZM', 'Gummi');
INSERT INTO `tbl_lga` VALUES ('764', 'ZM', 'Gusau');
INSERT INTO `tbl_lga` VALUES ('765', 'ZM', 'Kaura');
INSERT INTO `tbl_lga` VALUES ('766', 'ZM', 'Namoda');
INSERT INTO `tbl_lga` VALUES ('767', 'ZM', 'Maradun');
INSERT INTO `tbl_lga` VALUES ('768', 'ZM', 'Maru');
INSERT INTO `tbl_lga` VALUES ('769', 'ZM', 'Shinkafi');
INSERT INTO `tbl_lga` VALUES ('770', 'ZM', 'Talata Mafara');
INSERT INTO `tbl_lga` VALUES ('771', 'ZM', 'Tsafe');
INSERT INTO `tbl_lga` VALUES ('772', 'ZM', 'Zurmi ');

-- ----------------------------
-- Table structure for `tbl_log`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE `tbl_log` (
  `log_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(25) unsigned NOT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `machine_name` varchar(150) DEFAULT NULL,
  `operation` varchar(200) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `fname` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `FK_tbl_log_1` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_log
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_marital_status`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_marital_status`;
CREATE TABLE `tbl_marital_status` (
  `marital_status_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `marital_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`marital_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_marital_status
-- ----------------------------
INSERT INTO `tbl_marital_status` VALUES ('1', 'Single');
INSERT INTO `tbl_marital_status` VALUES ('2', 'Married');
INSERT INTO `tbl_marital_status` VALUES ('3', 'Divorced');
INSERT INTO `tbl_marital_status` VALUES ('4', 'Separated');

-- ----------------------------
-- Table structure for `tbl_payment`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_payment`;
CREATE TABLE `tbl_payment` (
  `payment_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(25) unsigned DEFAULT NULL,
  `term_id` int(25) unsigned DEFAULT NULL,
  `class_id` int(25) unsigned DEFAULT NULL,
  `admission_id` int(25) unsigned DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `datetime` varchar(45) DEFAULT NULL,
  `purpose` varchar(150) DEFAULT NULL,
  `amount2` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `FK_tbl_payment_1` (`session_id`),
  KEY `FK_tbl_payment_2` (`term_id`),
  KEY `FK_tbl_payment_3` (`class_id`),
  KEY `FK_tbl_payment_4` (`admission_id`),
  CONSTRAINT `FK_tbl_payment_1` FOREIGN KEY (`session_id`) REFERENCES `tbl_session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_payment_2` FOREIGN KEY (`term_id`) REFERENCES `tbl_term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_payment_3` FOREIGN KEY (`class_id`) REFERENCES `tbl_class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_payment_4` FOREIGN KEY (`admission_id`) REFERENCES `tbl_admission` (`admission_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_payment
-- ----------------------------
INSERT INTO `tbl_payment` VALUES ('1', '1', '1', '1', '1', '3500', '2013-10-13', 'Part payment for First term school fee', 'Three thousand five hundred naira only');
INSERT INTO `tbl_payment` VALUES ('2', '1', '1', '1', '1', '7500', '2013-10-13', 'Part payment of school fee for 2013/2014 session', 'Seven thousand five hundred naira only');
INSERT INTO `tbl_payment` VALUES ('3', '1', '2', '2', '2', '3000', '2014-03-09', ' school fee', 'three thousand naira only');
INSERT INTO `tbl_payment` VALUES ('4', '1', '1', '1', '3', '4500', '2014-03-29', ' school fee', 'Four Thousand five hundred naira only');
INSERT INTO `tbl_payment` VALUES ('5', '2', '1', '1', '3', '4000', '2014-12-27', ' part payment of school fee', '');

-- ----------------------------
-- Table structure for `tbl_psychomotor`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_psychomotor`;
CREATE TABLE `tbl_psychomotor` (
  `psychomotor_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `attentiveness` int(25) unsigned DEFAULT NULL,
  `punctuality` int(25) unsigned DEFAULT NULL,
  `honest` int(25) unsigned DEFAULT NULL,
  `dedication` int(25) unsigned DEFAULT NULL,
  `neatness` int(25) unsigned DEFAULT NULL,
  `relationship` int(25) unsigned DEFAULT NULL,
  `commitment` int(25) unsigned DEFAULT NULL,
  `politeness` int(25) unsigned DEFAULT NULL,
  `responsibility` int(25) unsigned DEFAULT NULL,
  `obedience` int(25) unsigned DEFAULT NULL,
  `admission_id` int(25) unsigned DEFAULT NULL,
  `teacher_comment` varchar(250) DEFAULT NULL,
  `principal_comment` varchar(250) DEFAULT NULL,
  `session_id` int(25) unsigned DEFAULT NULL,
  `term_id` int(25) unsigned DEFAULT NULL,
  `no_in_classs` int(25) unsigned DEFAULT NULL,
  `times_present` int(25) unsigned DEFAULT NULL,
  `times_absent` int(25) unsigned DEFAULT NULL,
  `next_term_begins` varchar(45) DEFAULT NULL,
  `next_term_ends` varchar(45) DEFAULT NULL,
  `sch_id` int(25) unsigned DEFAULT NULL,
  `class_id` int(25) NOT NULL,
  PRIMARY KEY (`psychomotor_id`),
  KEY `FK_tbl_psychomotor_1` (`admission_id`),
  KEY `FK_tbl_psychomotor_2` (`session_id`),
  KEY `FK_tbl_psychomotor_3` (`term_id`),
  CONSTRAINT `FK_tbl_psychomotor_1` FOREIGN KEY (`admission_id`) REFERENCES `tbl_admission` (`admission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_psychomotor_2` FOREIGN KEY (`session_id`) REFERENCES `tbl_session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_psychomotor_3` FOREIGN KEY (`term_id`) REFERENCES `tbl_term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_psychomotor
-- ----------------------------
INSERT INTO `tbl_psychomotor` VALUES ('2', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '1', 'HJKL', 'FGHJK', '1', '2', '50', '24', '24', '10/02/2013', '10/12/2013', '100', '1');
INSERT INTO `tbl_psychomotor` VALUES ('3', '2', '2', '3', '4', '5', '3', '3', '4', '3', '3', '4', 'nice', 'good', '1', '1', '31', '5', '33', null, null, '0', '1');
INSERT INTO `tbl_psychomotor` VALUES ('4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '1', 'He is responsible', 'Very good result', '1', '1', '0', '108', '4', null, null, '100', '1');

-- ----------------------------
-- Table structure for `tbl_reg`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_reg`;
CREATE TABLE `tbl_reg` (
  `reg_id` int(25) NOT NULL AUTO_INCREMENT,
  `surname` varchar(150) DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `othername` varchar(150) DEFAULT NULL,
  `dob` varchar(150) DEFAULT NULL,
  `pob` varchar(150) DEFAULT NULL,
  `gender_id` int(25) DEFAULT NULL,
  `religion_id` int(25) DEFAULT NULL,
  `nationalty` varchar(150) DEFAULT NULL,
  `states_id` int(10) DEFAULT NULL,
  `lga_id` int(11) DEFAULT NULL,
  `parent_name` varchar(100) DEFAULT NULL,
  `parent_add` varchar(250) DEFAULT NULL,
  `parent_phone` varchar(50) DEFAULT NULL,
  `emer_name` varchar(100) DEFAULT NULL,
  `emer_add` varchar(250) DEFAULT NULL,
  `emer_phone` varchar(50) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `sch_id` int(11) NOT NULL,
  PRIMARY KEY (`reg_id`),
  KEY `FK_tbl_reg_gender` (`gender_id`),
  KEY `FK_tbl_reg_religion` (`religion_id`),
  KEY `FK_tbl_reg_state` (`states_id`),
  KEY `FK_tbl_reg_lga` (`lga_id`),
  CONSTRAINT `FK_tbl_reg_gender` FOREIGN KEY (`gender_id`) REFERENCES `tbl_gender` (`gender_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_reg_lga` FOREIGN KEY (`lga_id`) REFERENCES `tbl_lga` (`lga_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_reg_religion` FOREIGN KEY (`religion_id`) REFERENCES `tbl_religion` (`religion_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_reg_state` FOREIGN KEY (`states_id`) REFERENCES `tbl_states` (`states_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_reg
-- ----------------------------
INSERT INTO `tbl_reg` VALUES ('100', 'HASSAN', 'TUNDE', 'GSDF', '03/12/2013', 'Island', '2', '2', '0', '9', '187', 'GDS', 'dg', '54', 'GDSG', 'gs', '876', '5f02f0889301fd7be1ac972c11bf3e7d', '100');
INSERT INTO `tbl_reg` VALUES ('101', 'OMOLOYE', 'TOHIR', 'O', '03/12/1996', 'lagos', '1', '1', '1', '30', '615', 'MR. OMOLOYE', 'otta', '080878', 'MR OMOLOYE', 'Otta', '0803781', '900150983cd24fb0d6963f7d28e17f72', '100');
INSERT INTO `tbl_reg` VALUES ('102', 'FE', 'E', '', '05/01/2013', '', '2', '2', '1', '3', '47', 'TE', 'RE', '34', 'GFG', 'FF', '6545', 'a57b8491d1d8fc1014dd54bcf83b130a', '100');
INSERT INTO `tbl_reg` VALUES ('103', 'ADENIYI', 'SEUN', 'O', '05/17/2013', 'Lagos', '1', '1', '1', '30', '615', 'MR/MRS OMOLOYE', 'Sango Otta', '08023457865', 'MR OMOLOYE', 'Sango Otta', '08022059045', '1a1dc91c907325c69271ddf0c944bc72', '200');
INSERT INTO `tbl_reg` VALUES ('104', 'AJISAFE', 'ADEOLA', '', '08/09/1995', 'Lagos', '2', '1', '1', '4', '81', 'MRS AJISAFE', '', '08037816587', 'MR AJISAFE', '4,osun state', '080786764455', '8977c4746fef0149df6eb19886b83a11', '100');

-- ----------------------------
-- Table structure for `tbl_religion`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_religion`;
CREATE TABLE `tbl_religion` (
  `religion_id` int(25) NOT NULL AUTO_INCREMENT,
  `religion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`religion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_religion
-- ----------------------------
INSERT INTO `tbl_religion` VALUES ('1', 'Islam');
INSERT INTO `tbl_religion` VALUES ('2', 'Christianity');

-- ----------------------------
-- Table structure for `tbl_role`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_role`;
CREATE TABLE `tbl_role` (
  `role_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `Firstname` varchar(100) DEFAULT NULL,
  `Lastname` varchar(100) DEFAULT NULL,
  `user_type_id` int(25) unsigned DEFAULT NULL,
  `islocked` varchar(5) DEFAULT 'no',
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  KEY `FK_tbl_role_1` (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_role
-- ----------------------------
INSERT INTO `tbl_role` VALUES ('1', 'cleansoft', 'admin', 'tohir', 'oye', '2', 'no', '2029-07-11 01:51:57');
INSERT INTO `tbl_role` VALUES ('3', 'walex', 'walex', 'adewale', 'ismail', '1', 'no', '2029-07-11 08:08:53');
INSERT INTO `tbl_role` VALUES ('4', 'sa', 'admin', 'sysadmin', 'administrator', '2', 'yes', '2014-08-11 12:23:38');
INSERT INTO `tbl_role` VALUES ('5', 'segun', 'segun', 'segun', 'olasegun', '5', 'no', '2014-04-18 08:54:39');

-- ----------------------------
-- Table structure for `tbl_scorebank`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_scorebank`;
CREATE TABLE `tbl_scorebank` (
  `scoreBank_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(25) unsigned DEFAULT NULL,
  `subject_id` int(25) unsigned DEFAULT NULL,
  `student_code` varchar(150) DEFAULT NULL,
  `ca` double DEFAULT NULL,
  `exam` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `remark` varchar(150) DEFAULT NULL,
  `term_id` int(25) unsigned DEFAULT NULL,
  `session_id` int(25) unsigned DEFAULT NULL,
  `sch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`scoreBank_id`),
  KEY `FK_tbl_scorebank_class` (`class_id`),
  KEY `FK_tbl_scorebank_subject` (`subject_id`),
  KEY `FK_tbl_scorebank_term` (`term_id`),
  KEY `FK_tbl_scorebank_session` (`session_id`),
  CONSTRAINT `FK_tbl_scorebank_classID` FOREIGN KEY (`class_id`) REFERENCES `tbl_class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_scorebank_session` FOREIGN KEY (`session_id`) REFERENCES `tbl_session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_scorebank_subjectID` FOREIGN KEY (`subject_id`) REFERENCES `tbl_subject` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_scorebank_term` FOREIGN KEY (`term_id`) REFERENCES `tbl_term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_scorebank
-- ----------------------------
INSERT INTO `tbl_scorebank` VALUES ('2', '1', '3', '1', '17', '46', '63', 'PASS', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('4', '1', '1', '1', '5', '19', '24', 'FAIL', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('5', '1', '2', '1', '22', '50', '72', 'PASS', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('6', '1', '3', '1', '15', '25', '40', 'PASS', '2', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('7', '1', '2', '1', '20', '30', '50', 'PASS', '2', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('8', '1', '1', '1', '21', '29', '50', 'PASS', '2', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('9', '1', '4', '1', '18', '37', '55', 'PASS', '2', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('10', '2', '3', '2', '27', '61', '88', 'PASS', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('11', '1', '3', '4', '21', '45', '66', 'PASS', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('12', '1', '2', '4', '18', '35', '53', 'PASS', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('13', '1', '1', '4', '22', '28', '50', 'PASS', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('14', '1', '4', '4', '15', '45', '60', 'PASS', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('15', '1', '6', '1', '15', '35', '50', 'PASS', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('16', '1', '5', '1', '18', '41', '59', 'PASS', '1', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('17', '1', '1', 'StudentCode', '0', '0', '0', 'FAIL', '3', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('18', '1', '1', '1', '16', '45', '61', 'PASS', '3', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('19', '1', '1', '3', '22', '32', '54', 'PASS', '3', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('20', '1', '1', '4', '23', '18', '41', 'PASS', '3', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('21', '1', '3', '1', '22', '17', '39', 'FAIL', '3', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('22', '1', '3', '3', '18', '31', '49', 'PASS', '3', '1', '100');
INSERT INTO `tbl_scorebank` VALUES ('23', '1', '3', '4', '11', '42', '53', 'PASS', '3', '1', '100');

-- ----------------------------
-- Table structure for `tbl_session`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_session`;
CREATE TABLE `tbl_session` (
  `session_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `session_name` varchar(150) DEFAULT NULL,
  `session_desc` varchar(150) DEFAULT NULL,
  `sch_id` int(11) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_session
-- ----------------------------
INSERT INTO `tbl_session` VALUES ('1', '2013/2014', null, '100');
INSERT INTO `tbl_session` VALUES ('2', '2014/2015', null, '100');
INSERT INTO `tbl_session` VALUES ('3', '2015/2016', null, '100');

-- ----------------------------
-- Table structure for `tbl_staff`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_staff`;
CREATE TABLE `tbl_staff` (
  `staff_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(150) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `initial` varchar(45) DEFAULT NULL,
  `category_id` int(25) unsigned DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `title` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`staff_id`),
  KEY `FK_tbl_staff_1` (`category_id`),
  CONSTRAINT `FK_tbl_staff_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_staff
-- ----------------------------
INSERT INTO `tbl_staff` VALUES ('1', ' sule', 'lamido', 'M', '2', '08099888', 'it@sbtelecoms.com', 'Mr.');

-- ----------------------------
-- Table structure for `tbl_states`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_states`;
CREATE TABLE `tbl_states` (
  `states_id` int(10) NOT NULL,
  `StateName` varchar(200) NOT NULL,
  `StateCapital` varchar(50) DEFAULT NULL,
  `StateCode` char(10) DEFAULT NULL,
  `Deleted` varchar(50) DEFAULT NULL,
  `DeletedBy` varchar(30) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`states_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_states
-- ----------------------------
INSERT INTO `tbl_states` VALUES ('1', 'Abia', '', 'AB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('2', 'Adamawa', '', 'AD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('3', 'Akwa Ibom', '', 'AK', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('4', 'Anambra', '', 'AN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('5', 'Bauchi', '', 'BA', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('6', 'Bayelsa', '', 'BY', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('7', 'Benue', '', 'BN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('8', 'Borno', '', 'BO', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('9', 'Cross Rivers', '', 'CR', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('10', 'Delta', '', 'DT', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('11', 'Ebonyi', '', 'EB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('12', 'Edo', '', 'ED', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('13', 'Ekiti', '', 'EK', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('14', 'Enugu', '', 'EN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('15', 'FCT', '', 'FC', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('16', 'Gombe', '', 'GM', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('17', 'Imo', '', 'IM', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('18', 'Jigawa', '', 'JG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('19', 'Kaduna', '', 'KD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('20', 'Kano', '', 'KN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('21', 'Katsina', '', 'KT', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('22', 'Kebbi', '', 'KB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('23', 'Kogi', '', 'KG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('24', 'Kwara', '', 'KW', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('25', 'Lagos', '', 'LA', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('26', 'Nassarawa', '', 'NS', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('27', 'Niger', '', 'NG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('28', 'Ogun', '', 'OG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('29', 'Ondo', '', 'OD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('30', 'Osun', '', 'OS', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('31', 'Oyo', '', 'OY', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('32', 'Plateau', '', 'PL', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('33', 'Rivers', '', 'RV', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('34', 'Sokoto', '', 'SO', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('35', 'Taraba', '', 'TR', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('36', 'Yobe', '', 'YB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('37', 'Zamfara', '', 'ZM', '', '', '2008-08-02 18:55:22');

-- ----------------------------
-- Table structure for `tbl_subject`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_subject`;
CREATE TABLE `tbl_subject` (
  `subject_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(150) DEFAULT NULL,
  `subject_desc` varchar(150) DEFAULT NULL,
  `sch_id` int(11) NOT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_subject
-- ----------------------------
INSERT INTO `tbl_subject` VALUES ('1', 'MATH', '', '100');
INSERT INTO `tbl_subject` VALUES ('2', 'ENGLISH', '', '100');
INSERT INTO `tbl_subject` VALUES ('3', 'AGRIC', '', '100');
INSERT INTO `tbl_subject` VALUES ('4', 'PHE', '', '100');
INSERT INTO `tbl_subject` VALUES ('5', 'SOCIAL STUDIES', 'Social Studies', '100');
INSERT INTO `tbl_subject` VALUES ('6', 'INT SCIENCE', 'Integrated Sci', '100');

-- ----------------------------
-- Table structure for `tbl_term`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_term`;
CREATE TABLE `tbl_term` (
  `term_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(150) DEFAULT NULL,
  `term_desc` varchar(150) DEFAULT NULL,
  `sch_id` int(11) NOT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_term
-- ----------------------------
INSERT INTO `tbl_term` VALUES ('1', 'FIRST', 'term', '100');
INSERT INTO `tbl_term` VALUES ('2', 'SECOND', 'second term', '100');
INSERT INTO `tbl_term` VALUES ('3', 'THIRD', '4th', '100');

-- ----------------------------
-- Table structure for `tbl_title`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_title`;
CREATE TABLE `tbl_title` (
  `title_id` int(25) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_title
-- ----------------------------
INSERT INTO `tbl_title` VALUES ('1', 'Mr.');
INSERT INTO `tbl_title` VALUES ('2', 'Mrs.');
INSERT INTO `tbl_title` VALUES ('3', 'Miss.');

-- ----------------------------
-- Table structure for `tbl_user_type`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_type`;
CREATE TABLE `tbl_user_type` (
  `user_type_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(50) NOT NULL,
  `user_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user_type
-- ----------------------------
INSERT INTO `tbl_user_type` VALUES ('1', 'Admin', 'Administrative Priviledge');
INSERT INTO `tbl_user_type` VALUES ('2', 'HOD', 'Head of Department');
INSERT INTO `tbl_user_type` VALUES ('3', 'Dean', 'Dean of College');
INSERT INTO `tbl_user_type` VALUES ('4', 'Teacher', 'Class Teacher');
INSERT INTO `tbl_user_type` VALUES ('5', 'Busary', 'Busary Department');
