CREATE TABLE IF NOT EXISTS `tbl_user_type` (
  `user_type_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(50) NOT NULL,
  `user_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;