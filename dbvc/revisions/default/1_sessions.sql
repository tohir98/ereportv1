CREATE TABLE IF NOT EXISTS `tbl_session` (
  `session_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `session_name` varchar(150) DEFAULT NULL,
  `session_desc` varchar(150) DEFAULT NULL,
  `sch_id` int(11) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;