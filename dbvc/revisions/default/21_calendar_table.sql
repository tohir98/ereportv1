CREATE TABLE IF NOT EXISTS `tbl_calender` (
  `calender_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(25) unsigned DEFAULT NULL,
  `term_id` int(25) unsigned DEFAULT NULL,
  `begin` varchar(45) DEFAULT NULL,
  `end` varchar(45) DEFAULT NULL,
  `midterm_start` varchar(45) DEFAULT NULL,
  `midterm_end` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`calender_id`),
  KEY `FK_tbl_calender_1` (`session_id`),
  KEY `FK_tbl_calender_2` (`term_id`),
  CONSTRAINT `FK_tbl_calender_1` FOREIGN KEY (`session_id`) REFERENCES `tbl_session` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_calender_2` FOREIGN KEY (`term_id`) REFERENCES `tbl_term` (`term_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;