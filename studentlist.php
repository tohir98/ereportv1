<?php
session_start();
set_time_limit(0);
error_reporting(0);
include("connect.php");
include("fns.php");
?>
<?php
if ($_GET['action'] == "delete") :
    ?>
    <script type="text/javascript">
        var a = confirm("Are You Sure ?");
        self.location = 'studentlist.php?id=<?= $_GET['id'] ?>&a=' + a;
    </script>  
    <?php
endif;

if ($a == "true"):
    $id = $_GET['id'];
    $result = mysql_query("DELETE FROM tbl_admission WHERE admission_id='$id'");

endif;
?>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Typography | BlueWhale Admin</title>
    <link rel="stylesheet" type="text/css" href="css_main/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_main/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css_main/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <script type="text/javascript">

    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();


    });
    </script>
</head>
<?php
$select = "SELECT ad.firstname, ad.lastname, cl.class_name, gd.gender, rg.religion, ad.email, ad.phone, ad.admission_id
				FROM tbl_admission ad
				INNER JOIN tbl_class cl ON ad.class_id=cl.class_id
				INNER JOIN tbl_gender gd ON ad.gender_id=gd.gender_id
				INNER JOIN tbl_religion rg ON ad.religion_id=rg.religion_id WHERE ad.sch_id='" . $_SESSION['school_id'] . "' ";
$result = mysql_query($select);
?>

<div class="box round first grid">
    <h2>
        Our Students &nbsp; | <font style="font-size:15px; color:red">You have (<?php echo mysql_num_rows($result); ?>) Registered Students </font></h2>
    <div class="block">



        <table class="data display datatable" id="example">
            <thead>
                <tr>
                    <th></th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Class</th>
                    <th>Gender</th>
                    <th>Religion</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Edit/Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                while ($row = mysql_fetch_array($result)) {
                    ?>
                    <tr class="odd gradeX">
                        <?php
                        $sn = $row[7];
                        echo"<td><input type=checkbox name=sn[] value=$sn></td>";
                        ?>

                        <td><?php echo $row[0] ?></td>
                        <td><?php echo $row[1] ?></td>
                        <td><?php echo $row[2] ?></td>
                        <td><?php echo $row[3] ?></td>
                        <td><?php echo $row[4] ?></td>
                        <td><?php echo $row[5] ?></td>
                        <td><?php echo $row[6] ?></td>
                        <?php
                        echo "<td><a href='addstudent.php?id=$sn'><img src='images/edit.png' alt='Edit' title='Click here to Edit'></a>&nbsp;
						   		<a href='studentlist.php?action=delete&id=$sn'><img src='images/erase.png' alt='Delete' title='Click here to Delete'></a></td>";
                        ?>
                    </tr>
                    <?php
                }
                ?>


            </tbody>
        </table>



    </div>
</div>
