<?php include '_header_script.php'; ?>

<?php
include_once '..\bin\constant.php';
include_once '..\bin\methods.php';

if ($_POST['Submit'] === "Save") {

    $msg = updateSettings($_POST, $_FILES['logo']);
    ?>
    <script>
        alert('<?=$msg?>');
    </script>
<?php } ?>
    <script type="text/javascript">
        $(document).ready(function () {
            setupTinyMCE();
            setupProgressbar('progress-bar');
            setDatePicker('date-picker');
            setupDialogBox('dialog', 'opener');
            $('input[type="checkbox"]').fancybutton();
            $('input[type="radio"]').fancybutton();
        });
    </script>
    <!-- /TinyMCE -->
    <style type="text/css">
        #progress-bar
        {
            width: 400px;
        }
    </style>
    </head>

    <div class="box round first grid">
        <h2>Settings</h2>
        <div class="block ">
            <form method="post" enctype="multipart/form-data"  name="frmreg" id="frmreg" >
                <table class="form">
                    <tbody>
                        <tr>
                            <td colspan="2" valign="top">
                                <table width="98%" border="0" cellpadding="5" cellspacing="5">
                                    <tbody>
                                        <tr>
                                            <td colspan="4" align="center" style="font-size:16px; font-family:arial; font-weight:bold; margin-left:40px; color: #EE0000"><?php echo isset($error) ? $error : ''; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4" align="right"><hr color="#FF0000"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <span class="label">
                                                <label for="school_name">School Name:</label>
                                            </span>
                                        </td>
                                        <td align="left">
                                            <span class="text-input-bg">
                                                <input id="school_name" name="school_name" size="30" type="text" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="label">
                                                <label for="address">Address:</label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input type="text" id="address" name="address" size="30"  />
                                            </span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="label">
                                                <label for="phone">Phone </label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input type="text" id="phone" name="phone" size="30"  />
                                            </span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="label">
                                                <label for="email">Email</label>
                                            </span>
                                        </td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="email" name="email" size="30" type="text" />
                                            </span></td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="label">
                                                <label for="logo">Logo</label>
                                            </span></td>
                                        <td align="left"><span class="text-input-bg">
                                                <input id="logo" name="logo" size="30" type="file" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">&nbsp;</td>
                                        <td align="right"><span class="style5"><div id="dstat" align="center"></div></span></td>
                                        <td align="left"><label>
                                                <input name="Submit" type="submit" class="btn btn-red" value="Save" />
                                            </label></td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>