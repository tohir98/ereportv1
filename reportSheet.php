<?php
session_start();
include "bin/make_safe.php";
include ("bin/conit.php");
if (!get_cfg_var('safe_mode')) {
      set_time_limit(0);
    }
	 
$student_id = trim($_POST['student_id']);
$class_id = trim($_POST['class_id']);
$term_id = trim($_POST['term_id']);
$session_id = trim($_POST['session_id']);
$sch_id = $_SESSION['school_id'];
$name=getName($student_id);
$sex=getSex($student_id);
$class=getClass($student_id);
list($begin,$end_date)=explode("%",getCalender());
	
	$Qchker = "SELECT * FROM tbl_psychomotor WHERE admission_id= '$student_id' AND term_id='$term_id' AND session_id='$session_id'";
		$Rchker = mysql_query($Qchker);
		if (mysql_num_rows($Rchker) > 0){
			$RowRchker = mysql_fetch_array($Rchker);
			$times_present = $RowRchker['times_present'];
			$times_absent = $RowRchker['times_absent'];
			$teacher_comment = $RowRchker['teacher_comment'];
			$principal_comment = $RowRchker['principal_comment'];
			$no_in_class = $RowRchker['no_in_classs'];
			
			$attentiveness = $RowRchker['attentiveness'];
			$punctuality = $RowRchker['punctuality'];
			$honest = $RowRchker['honest'];
			$dedication = $RowRchker['dedication'];
			$neatness = $RowRchker['neatness'];
			$relationship = $RowRchker['relationship'];
			$commitment = $RowRchker['commitment'];
			$politeness = $RowRchker['politeness'];
			$responsibility = $RowRchker['responsibility'];
			$obedience = $RowRchker['obedience'];
			$next_term_begins = $RowRchker['next_term_begins'];
			$next_term_ends = $RowRchker['next_term_ends'];
		}


?>

<html>
<head>
<style type="text/css">
<!--
.style101 {font-size: 12px}
.style106 {font-family: "Trebuchet MS"; font-weight: bold; color: #000000; text-decoration:underline; }
.style107 {font-family: "Trebuchet MS"}
.CAP {	font-size: 12px;
	font-weight: bold;
	font-family: "verdana";
}
.thin {	font-size: 12px;
	font-weight: bold;
	font-family: "arial Narrow";
}
.style108 {font-size: 12px;
	font-weight: bold; font-family:Verdana;
}

-->
</style>
</head>

<body>
<div align="center" style="width:900px; margin-left:300px; border:solid 1px #999999">
	<h2 align="center" class="style107"><img src="images/reportSheet.png" alt="Report Sheet" width="300" height="50"></h2>
	<br>
	<table width="500" border="0" align="center">
	  <tr>
		<td colspan="5" class="CAP" align="center" style="font: 'Arial Black'; font-size:14px; color:#990000;"><?php echo getField('tbl_session','session_id', 'session_name ', $session_id); ?> ACADEMIC SESSION</td>
		<td class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td colspan="5" class="CAP" align="center" style="font: 'Arial Black'; font-size:14px; color:#990000;"><?php echo getField('tbl_term','term_id', 'term ', $term_id); ?> TERM RESULT</td>
		<td class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td colspan="5" class="CAP" align="center">&nbsp;</td>
		<td class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td colspan="5" class="CAP" align="center" style="font-family:'Arial Black'; font-size:14px; color:#003300;"><u><?php echo strtoupper($name); ?></u></td>
		<td class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td class="CAP">&nbsp;</td>
		<td class="CAP">&nbsp;</td>
		<td align="center" class="CAP">&nbsp;</td>
		<td class="CAP">&nbsp;</td>
		<td class="CAP">&nbsp;</td>
		<td class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td width="269" class="CAP">ADMISSION NUMBER : </td>
		<td width="74" class="CAP">&nbsp;<?=$student_id?></td>
		<td width="15" align="center" class="CAP">|</td>
		<td width="37" class="CAP">&nbsp;</td>
		<td width="240" class="CAP">TIMES PRESENT </td>
		<td width="39" class="CAP"><?=$times_present?></td>
	  </tr>
	  <tr>
		<td class="CAP">SEX</td>
		<td class="CAP"><?=$sex?></td>
		<td align="center" class="CAP">|</td>
		<td class="CAP">&nbsp;</td>
		<td class="CAP">TIMES ABSENT </td>
		<td class="CAP"><?=$times_absent?></td>
	  </tr>
	  <tr>
		<td class="CAP">CLASS</td>
		<td class="CAP"><?php echo getField('tbl_class','class_id', 'class_name', $class_id); ?></td>
		<td align="center" class="CAP">|</td>
		<td class="CAP">&nbsp;</td>
		<td class="CAP">NEXT TERM BEGINS </td>
		<td class="CAP"><?=$next_term_begins?></td>
	  </tr>
	  <tr>
		<td class="CAP">NUMBER IN CLASS </td>
		<td class="CAP"><?=$no_in_class?></td>
		<td align="center" class="CAP">|</td>
		<td class="CAP">&nbsp;</td>
		<td class="CAP">NEXT TERM ENDS </td>
		<td class="CAP"><?=$next_term_ends?></td>
	  </tr>
	</table>
	<p align="center" class="style107">&nbsp;</p>
	<table width="880" border="0" align="center">
	  <tr>
		<td colspan="13" align="center" valign="middle" class="style106">STUDENT'S PROGRESSIVE PERFORMANCE</td>
		<td colspan="3" class="style106">CLASS PERF. </td>
	  </tr>
	  
	  <tr bgcolor="#3399FF">
		<td width="5%" align="center" valign="middle" class="style106" >S/N </td>
		<td width="11%" valign="middle" class="style106" >&nbsp;<span class="style106" >SUBJECT</span> </td>
		<td width="5%" valign="middle"  class="style106" >&nbsp;CA</td>
		<td width="9%" valign="middle" class="style106" >&nbsp;Exam. 70% </td>
		<td width="9%" valign="middle" class="style106" >&nbsp;TOTAL </td>
		<?php if($term_id == 2){ ?>
			<td width="12%" valign="middle" class="style106" >&nbsp;TERM1 </td>
		<?php } ?>
		<?php if($term_id == 3){ ?>
			<td width="12%" valign="middle" class="style106" >&nbsp;TERM1 </td>
			<td width="12%" valign="middle" class="style106" >&nbsp;TERM2 </td>
		<?php } ?>
		<td width="12%" valign="middle" class="style106" >&nbsp;AVERAGE </td>
		<td width="9%" valign="middle" class="style106" >&nbsp;GRADE </td>
		<td width="10%" valign="middle" class="style106" >&nbsp;REMARK</td>
		<td width="4%" valign="middle" class="style106" >&nbsp;</td>
		<td width="11%" valign="middle" class="style106" >&nbsp;</td>
		<td width="4%" valign="middle" class="style106" >HI</td>
		<td width="5%" valign="middle" class="style106" >LW</td>
		<td width="6%" valign="middle" class="style106" >AV</td>
	  </tr>
	  <?php
	
					
		$qry = "SELECT * FROM tbl_scorebank WHERE student_code='$student_id' AND class_id='$class_id' AND term_id='$term_id' AND session_id='$session_id' and sch_id='$sch_id'  ";
		$Rqry = mysql_query($qry);
		if (mysql_num_rows($Rqry)>0){
		$counter = 1;
		$bgcolor = '#D2FFA6';
		while ($rowRqry = mysql_fetch_array($Rqry))
		{
			if ($bgcolor =='#D2FFA6')
			{
				$bgcolor ='#F2FFE6';
			}else{
				$bgcolor ='#D2FFA6';
			}
	
	 
			$subject_id = $rowRqry["subject_id"];
			$ca = $rowRqry["ca"];
			$exam = $rowRqry["exam"];
			$ttotal =  $ca + $exam;
			list($grade,$remark)=explode("-",GradeIt($ttotal));
	  
			$subject = getField('tbl_subject','subject_id', 'subject_name ', $subject_id);
		
	
			echo "  
			<tr bgcolor=\"$bgcolor\" height='25' class='CAP'>
			<td align='center'>&nbsp;$counter</td>
			<td>&nbsp;$subject</td>
			<td>&nbsp;$ca</td>
			<td>&nbsp;$exam</td>
			<td>&nbsp;$ttotal</td>";
			if($term_id == 2){
				$term1_score=getTermScore($student_id,$class_id,$session_id,$term_id-1,$subject_id);
				list($term1_ca,$term1_exam,$term1_total)=explode("-",$term1_score);
				echo "<td>&nbsp;$term1_total</td>";
				$average=number_format(($term1_total+$exam)/2,0);
			}elseif($term_id == 3){
				$term1_score=getTermScore($student_id,$class_id,$session_id,$term_id-2,$subject_id);
				list($term1_ca,$term1_exam,$term1_total)=explode("-",$term1_score);
				echo "<td>&nbsp;$term1_total</td>";
				$term2_score=getTermScore($student_id,$class_id,$session_id,$term_id-1,$subject_id);
				list($term2_ca,$term2_exam,$term2_total)=explode("-",$term2_score);
				echo "<td>&nbsp;$term2_total</td>";
				$average=number_format(($term2_total+$term1_total+$exam)/3,0);
			}
			echo "<td>&nbsp;$average</td>
			<td>&nbsp;$grade</td>
			<td>&nbsp;$remark</td>
			</tr>";
			++$counter; 
		}
	  
	  
	  
	  }
	  
	  
	  ?>
  </table>
	<p>&nbsp;</p>
	<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
		<td width="245">&nbsp;</td>
		<td width="205" class="CAP">NUMBER OF SUBJECTS </td>
		<td width="207" class="CAP"><?=$counter-1?></td>
		<td width="43" class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td class="CAP">EXPECTED MARK </td>
		<td class="CAP"><?=($counter-1)*100?></td>
		<td class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td class="CAP">TOTAL MARK OBTAINED </td>
		<td class="CAP"><span class="style108"><?php echo $overall=getOvrall($sch_id, $class_id, $term_id, $session_id, $student_id); ?></span></td>
		<td class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td class="CAP">AVERAGE SCORE </td>
		<td class="CAP"><?php echo number_format(($overall/($counter-1)),2); ?>%</td>
		<td class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td class="CAP">&nbsp;</td>
		<td class="CAP">&nbsp;</td>
		<td class="CAP">&nbsp;</td>
	  </tr>
	  <tr>
		<td colspan="4" class="CAP"><u>AFFECTIVE &amp; PSYCHOMOTOR ASSESSMENT - (Very poor), 2(poor), 3(Fair), 4(Good), 5(Excellent) </u></td>
	  </tr>
	  <tr>
		<td colspan="4" class="CAP"><table width="100%" border="0">
		  <tr>
			<td class="CAP">ATTENTIVENESS</td>
			<td class="CAP"><?=$attentiveness?></td>
			<td class="CAP">|</td>
			<td class="CAP">NEATNESS</td>
			<td class="CAP"><?=$neatness?></td>
			<td class="CAP">|</td>
			<td class="CAP">POLITENESS</td>
			<td class="CAP"><?=$politeness?></td>
		  </tr>
		  <tr>
			<td class="CAP">PUNCTUALITY</td>
			<td class="CAP"><?=$punctuality?></td>
			<td class="CAP">|</td>
			<td class="CAP">RELATIONSHIP WITH OTHERS </td>
			<td class="CAP"><?=$relationship?></td>
			<td class="CAP">|</td>
			<td class="CAP">RESPONSIBILITY</td>
			<td class="CAP"><?=$responsibility?></td>
		  </tr>
		  <tr>
			<td class="CAP">HONESTY</td>
			<td class="CAP"><?=$honest?></td>
			<td class="CAP">|</td>
			<td class="CAP">COMMITTMENT</td>
			<td class="CAP"><?=$commitment?></td>
			<td class="CAP">|</td>
			<td class="CAP">OBEDIENCE</td>
			<td class="CAP"><?=$obedience?></td>
		  </tr>
		  <tr>
			<td class="CAP">DEDICATION</td>
			<td class="CAP"><?=$dedication?></td>
			<td class="CAP">|</td>
			<td>&nbsp;</td>
			<td class="CAP">&nbsp;</td>
			<td class="CAP">&nbsp;</td>
			<td>&nbsp;</td>
			<td class="CAP">&nbsp;</td>
		  </tr>
		  <tr>
			<td class="CAP">&nbsp;</td>
			<td class="CAP">&nbsp;</td>
			<td class="CAP">&nbsp;</td>
			<td>&nbsp;</td>
			<td class="CAP">&nbsp;</td>
			<td class="CAP">&nbsp;</td>
			<td>&nbsp;</td>
			<td class="CAP">&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan="8" class="thin">Class Teacher's Comment: 
			  <?=$teacher_comment?></td>
		  </tr>
		  <tr>
			<td colspan="8" class="thin">Principal's Comment: 
			  <?=$principal_comment?></td>
		  </tr>
		  <tr>
			<td colspan="8" class="CAP"><hr></td>
		  </tr>
		</table></td>
	  </tr>
	</table>
	<p>&nbsp;</p>
	<div style="font-family:Verdana; font-weight:bold; font-size:10px">
		Result Approved By:<br>
		The Principal &amp; Academic Committee of<br>
		Adventist High School<br><br>
		Printed Today: <?php echo date('M d,Y h:i:a'); ?>
	</div>
</div>
</body>
</html>